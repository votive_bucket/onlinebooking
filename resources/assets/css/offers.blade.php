@extends('layouts.app')

@section('title', 'Combo')

@section('content')
<section class="home_main budstation_bg">
  <div class="container home_pos content_text">
      <div class="logo wow fadeInDown animated">      
          <a href="javascript:void(0)"><img src="{{url('resources/assets')}}/images/logo.png"></a>
      </div>
      <div class="main_hd wow fadeInDown animated">
        <h1>QUÀ ĐẶC BIỆT KHI ĐẶT TIỆC</h1>   
        <p class="sub_hd">Phần quà này có thể làm thành phiếu bốc thăm may mắn cho các nhân viên tham dự tiệc hoạc dành tặng cho nhân viên xuất sắc của công ty bạn.  </p>   
      </div>
      <section class="box_spcOffer">
      <div class="row">
        @foreach($alloffer as $key => $value)
        <div class="col-md-4 col-sm-4 wow fadeInLeft animated">
          <div class="bx_price">
            <h4>{{$value->offer_name}}</h4>
            <div class="bx_poImg"><img src="{{url('/public/uploads/offer/')}}/{{$value->offer_image}}"></div>
            
          </div>
        </div>
        @endforeach          
      </div>
    </section>
      <div class="btn_bud wow fadeInDown animated">
        <a href="{{url('choose/combo')}}" class="offerbtn">
          
        </a>    
      </div>            
  </div>  
</section>
@endsection