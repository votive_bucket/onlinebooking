@if (!$venues_list->isEmpty())
<div class="abd">
    <table class="table bx_tbl" border="1">
        <tbody>
            <?php $i=0;
            foreach ($venues_list as $key => $venue) {
            $i++;?>
            <tr role="row" class="list_venue">
                <td>
                    <div class="squaredTwo">
                        <input type="checkbox" name="venue_check" value="{{$venue->venue_id}}" id="squaredwo{{$i}}" data-imgsrc="{{$venue->venue_logo}}"/>
                        <label for="squaredwo{{$i}}"></label>
                    </div>
                </td>
                <td>
                    <div class="bx_htl">
                        <div class="img_htl">
                            <a href="javascript:void(0)" class="venue-info" venueid="{{$venue->venue_id}}">
                                @if(!empty($venue->venue_logo))
                                <img src="{{url('/public/uploads/venue')}}/{{!empty($venue->venue_logo)? $venue->venue_logo:'' }}">
                                @else
                                <img src="{{url('/resources/assets/images/')}}/logo.png">
                                @endif
                            </a>
                        </div>
                        <div class="bx_htlText">
                            <div class="bx_mainText">
                                <h4>{{$venue->venue_name}}</h4>
                                <h5>Sức Chứa: {{$venue->venue_hall_capacity}} khách</h5>
                            </div>
                            <div class="bx_subText">
                                {{$venue->venue_address}}
                            </div>
                            <ul class="list_icon">
                                @if ($venue->venue_has_dj==1)
                                <li>
                                    <img src="{{url('/resources/assets/images/')}}/venue-dj-icon.svg">
                                    <span>DJ</span>
                                </li>
                                @endif
                                @if ($venue->venue_has_livemusic==1)
                                <li>
                                    <img src="{{url('/resources/assets/images/')}}/venue-live-music-icon.svg">
                                    <span>Dàn Âm Thanh</span>
                                </li>
                                @endif
                                @if (!empty($venue->venue_has) && $venue->venue_has!='')
                                <li>
                                    <img src="{{url('/resources/assets/images/')}}/venue-as-extra-icon.svg">
                                    <span>{{$venue->venue_has}}</span>
                                </li>
                                @endif
                                @if ($venue->venue_has_projector==1)
                                <li>
                                    <img src="{{url('/resources/assets/images/')}}/venue-projector-icon.svg">
                                    <span>Máy Chiếu</span>
                                </li>
                                @endif
                                @if ($venue->venue_has_stage==1)
                                <li>
                                    <img src="{{url('/resources/assets/images/')}}/venue-stage-icon.svg">
                                    <span>Sân Khấu</span>
                                </li>
                                @endif
                                @if ($venue->venue_has_viproom==1)
                                <li>
                                    <img src="{{url('/resources/assets/images/')}}/venue-vip-room-icon.svg">
                                    <span>Phòng VIP</span>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
@else
    <table class="table bx_tbl" border="1">
        <tbody>
            <tr role="row"> Khôngtìmthấykếtquảphùhợp… </tr>
        </tbody>
    </table>
@endif