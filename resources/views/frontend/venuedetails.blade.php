<style type="text/css">
	.carousel {
    margin-top: 20px;
}
.item .thumb {
	width: 25%;
	cursor: pointer;
	float: left;
}
.item .thumb img {
	width: 100%;
	margin: 2px;
}
.item img {
	width: 100%;	
}

</style>
<div class="modal-header">
  <div class="bx_htlPop">
    <div class="img_htlPop">
      @if(!empty($venuemenudata[0]['venue']->venue_logo))
        <img src="{{url('/public/uploads/venue')}}/{{!empty($venuemenudata[0]['venue']->venue_logo)? $venuemenudata[0]['venue']->venue_logo:'' }}">
      @else
        <img src="{{url('/resources/assets/images/')}}/logo.png">
      @endif
    </div>
    <div class="bx_mainTextPop">
      <h4>{{$venuemenudata[0]['venue']->venue_name}}</h4>
      <p> {{$venuemenudata[0]['venue']->venue_address}}</p>
    </div>
  </div>
  <button type="button" class="select-venu" venue_id="{{$venuemenudata[0]['venue']->venue_id}}" venue_img="{{$venuemenudata[0]['venue']->venue_logo}}" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">CHỌN QUÁN</span>
  </button>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="bx_popVenue">
    <div class="row">
      <div class="col-md-7">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <?php foreach ($venuemenudata[0]['venue_images'] as $key => $vimage) { ?>
              <div class="item {{($key==0)?'active':''}}">
                <img src="{{url('/public/uploads/venue/images')}}/{{!empty($vimage->image_name)? $vimage->image_name:'' }}">
              </div>
            <?php } ?>
          </div>
        </div> 
    <div class="clearfix">
        <div id="thumbcarousel" class="carousel slide" data-interval="false">
            <div class="carousel-inner">
                <div class="item active">
                  <?php foreach ($venuemenudata[0]['venue_images'] as $key => $vimage) { ?>
                    <div data-target="#carousel" data-slide-to="{{$key}}" class="thumb">
                      <img src="{{url('/public/uploads/venue/images')}}/{{!empty($vimage->image_name)? $vimage->image_name:'' }}">
                    </div>
                  <?php } ?>
                </div><!-- /item -->
            </div><!-- /carousel-inner -->
        </div> <!-- /thumbcarousel -->
    </div><!-- /clearfix -->
      </div>
      <div class="col-md-5">
        <div class="bx_popVright">
          <h4>Sức chứa: <span>{{$venuemenudata[0]['venue']->venue_hall_capacity}} khách</span></h4>
          <ul class="list_icon">
            @if ($venuemenudata[0]['venue']->venue_has_dj==1)
            <li>
              <img src="{{url('/resources/assets/images/')}}/venue-dj-icon.svg">
              <span>DJ</span>
            </li>
            @endif
            @if ($venuemenudata[0]['venue']->venue_has_livemusic==1)
            <li>
              <img src="{{url('/resources/assets/images/')}}/venue-live-music-icon.svg">
              <span>Dàn Âm Thanh</span>
            </li>
            @endif
            @if (!empty($venuemenudata[0]['venue']->venue_has) && $venuemenudata[0]['venue']->venue_has!='')
            <li>
              <img src="{{url('/resources/assets/images/')}}/venue-as-extra-icon.svg">
              <span>{{$venuemenudata[0]['venue']->venue_has}}</span>
            </li>
            @endif
            @if ($venuemenudata[0]['venue']->venue_has_projector==1)
            <li>
              <img src="{{url('/resources/assets/images/')}}/venue-projector-icon.svg">
              <span>Máy Chiếu</span>
            </li>
            @endif
            @if ($venuemenudata[0]['venue']->venue_has_stage==1)
            <li>
              <img src="{{url('/resources/assets/images/')}}/venue-stage-icon.svg">
              <span>Sân Khấu</span>
            </li>
            @endif
            @if ($venuemenudata[0]['venue']->venue_has_viproom==1)
            <li>
              <img src="{{url('/resources/assets/images/')}}/venue-vip-room-icon.svg">
              <span>Phòng VIP</span>
            </li>
            @endif
          </ul>
          <div class="bx_tabMenu">
            <ul class="nav nav-tabs" role="tablist">
              <?php foreach ($venuemenudata[0]['menus'] as $key => $menu) { ?>
                <li role="presentation" class="{{($key==0)?'active':''}}"><a href="#menu{{$key}}" aria-controls="menu{{$key}}" role="tab" data-toggle="tab">{{$menu->menu_name}}</a></li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php
              //echo "<pre>";print_r($venuemenudata[0]['menus']);die();
              foreach ($venuemenudata[0]['menus'] as $key => $menu) { ?>
                <div role="tabpanel" class="tab-pane {{($key==0)?'active':''}}" id="menu{{$key}}">
                  <div class="sub-items">
                    {!! $menu->submenu_ids !!}
                  </div>
                  <div class="menu-price-total">
                    <p>Giá cho 1 người:
                      <span class="price-tlt">
                        {{str_replace(",",".",number_format($menu->menu_price))}} VNĐ
                      </span>
                    </p>
                    <p>
                      Giá tổng cộng: 
                      <span class="price-tlt">
                        <?php echo (str_replace(",",".",number_format($menu->menu_price*$venuemenudata[0]['countPeople']))) .' VNĐ'; ?>
                        </span>
                      </p>
                  </div>
                </div>
            <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>