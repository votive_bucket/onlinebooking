@extends('layouts.app')

@section('title', 'Combo')

@section('content')

<div class="box_main_thank">

<section class="budstation_bg">
	 <div class="logo wow fadeInDown animated">      
	    <a href="javascript:void(0)"><img src="{{url('resources/assets')}}/images/logo.png"></a>
	</div>




<div class="container home_pos">
    <div class="thankyou-middle">
        <div class="thankyou-middle-section">
            <div class="bx_hdTh">
                <h2>CẢM ƠN</h2>
                <p>BẠN ĐÃ ĐẶT TIỆC CÙNG BUD STATION!</p>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="bx_thnkLeft">
                        <div class="bx_leftCont">
                            <h4>Ưu đãi lớn khi mua trên 300 thùng bia! </h4>
                            <p>Tiết kiệm lên đến 12% và kèm theo ban nhạc cho buổi tiệc của bạn</p>
                        </div>
                        <a href="tel:18006100" class="bx_htLine">
                            <img src="{{url('resources/assets/images')}}/img_mob.png">
                            <div class="htLine">
                                Gọi ngay hotline
                                <span>1800-6100-1-2</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="bx_thnkLeft bx_thnkRight">
                        <div class="bx_leftCont">
                            <h4>Giới thiệu bạn bè đặt tiệc trên 100 người</h4>
                            <p>Nhận ngay 300.000 VNĐ!</p>                           
                        </div>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{url('/')}}/thankyou" target="_blank" class="bx_htLine">
                            <img src="{{url('resources/assets/images')}}/icon_fb.png">
                            <div class="htLine">
                                CHIA SẺ NGAY
                                <span>Facebook, Messenger</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="btn_bud">
	<a href="{{url('/')}}"><img src="{{url('resources/assets/images')}}/img_btn.png"> Trở về trang chủ <i class="fa fa-angle-right" aria-hidden="true"></i> <i class="fa fa-angle-right" aria-hidden="true"></i> <i class="fa fa-angle-right" aria-hidden="true"></i> </a>    
</div>




</div>
	
</section>


</div>
@endsection