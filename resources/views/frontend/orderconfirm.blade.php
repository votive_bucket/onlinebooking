@extends('layouts.app')

@section('title', 'Order Confirm')

@section('content')
<?php //echo "<pre>";print_r($formdata);die;?>
    <section class="budstation_bgOne order-confirm">
        <div class="container order_conf">
            <div class="logo wow fadeInDown animated">      
                <a href="#"><img src="{{url('resources/assets')}}/images/logo.png"></a>
            </div>
            <div class="order-confirm-main">
              <div class="order-confirm-section">
                <div class="row">
                    <form action="{{url('/thankyou')}}"name="userform" id="userform" method="post">
                    <div class="col-md-5">

                        <div class="box_ordLeft">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="f_name">NGÀY TỔ CHỨC (*)</div>
                            </div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                    <input class="form-control" id="date" name="date" placeholder="DD/MM/YYYY" type="text"/>
                                    <!-- <input type='text' class="form-control" data-provide="datepicker"> -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="f_name">THỜI GIAN TỔ CHỨC (*)</div>
                            </div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                   <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                                        <input type="text" class="form-control time" name="time" id="time" value="19:00">@csrf
                                        <input type="hidden" name="order" id="order" value="{{$dataid}}">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-time"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-5">
                                <div class="f_name">Họ và tên (*)</div>
                            </div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                    <input class="form-control" id="name" name="name" placeholder="NGUYỄN VĂN A" type="text"/>                               
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="f_name">số điện thoại (*)</div>
                            </div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                    <input class="form-control" id="phone" name="phone" placeholder="0123456789" type="text"/>
                                   
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="f_name">địa chỉ email (*)</div>
                            </div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                    <input class="form-control" id="email" name="email" placeholder="nguyenvana@gmail.com" type="text"/>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="f_name">người giới thiệu</div>
                            </div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                    <input class="form-control" id="ref_phone" name="ref_phone" placeholder="(số điện thoại)" type="text"/>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="f_name">Ghi chú</div>
                            </div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                    <textarea name="note" id="note" placeholder="..."></textarea>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-7">
                                <div class="f_nameOne">
                                    (*) Thông tin bắt buộc
                                </div>
                            </div>
                        </div>

                    </div>
                    </div>
                    <div class="col-md-7">
                       <div class="main_bxOrder wow fadeInDown animated">
                           <h4>xác nhận đơn hàng</h4>
                           <div class="sb_order">
                               <div class="row">
                                    <div class="col-md-8">
                                        <div class="logo_order">      
                                            <a href="#"><img src="{{url('resources/assets')}}/images/logo_black.png"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="ord_date">
                                            <h4>ĐƠN HÀNG</h4>
                                            <h5>Ngày <?php echo date('d/m/Y'); ?></h5>
                                        </div>
                                    </div>
                               </div>

                               <div class="ord_name">
                                   <div class="row">
                                        <div class="col-md-5"><h5><strong>Mã đơn hàng:</strong></h5></div>
                                        <div class="col-md-7"><h5></h5> #{{$dataorder}}</div>
                                   </div>
                                   <div class="row">
                                        <div class="col-md-5"><h5><strong>Số lượng người tham dự:</strong></h5></div>
                                        <div class="col-md-7"><h5>{{$formdata['people']}}</h5></div>
                                   </div>
                                   
                               </div>
                               <div class="ord_menu">
                                   <h3>Những quán ưu tiên theo thứ tự</h3>

                                   <div class="row">
                                    @foreach($venuemenudata as $key => $value)
                                       <div class="col-md-4">
                                            <h4 class="hd_ord">{{$key+1}}. {{$value['venue']->venue_name}}</h4>
                                              <?php 
                                                  $total = '';
                                                  $percentage = '';
                                                  $offer = '';
                                                  if($key==0){
                                                    $total = $formdata['total1'];
                                                    $percentage = ($total*50)/100;

                                                    $offer = $offer1;
                                                  }
                                                  if($key==1){
                                                    $total = $formdata['total2'];
                                                    $percentage = ($total*50)/100;
                                                    $offer = $offer2;
                                                  }
                                                  if($key==2){
                                                    $total = $formdata['total3'];
                                                    $percentage = ($total*50)/100;
                                                    $offer = $offer3;

                                                  }
                                              ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5><strong>Combo {{$formdata['comboid']}} - {{$value['menus']->menu_name}}</strong></h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12"><h5><strong>{{str_replace(",",".",number_format($value['menus']->menu_price))}} VNĐ/1 người.</strong></h5></div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5"><h5><strong>Quà Đặc Biệt:</strong></h5></div>
                                                <div class="col-md-7"><h5>{{ $offer }}</h5></div>
                                            </div>
                                            <ul class="list_ordMenu">
                                                <li>  
                                                    {!!$value['menus']->submenu_ids !!}  
                                                </li>
                                            </ul>
                                            <div class="box_ordCont">
                                              <div class="ord_cart">
                                              <img src="http://votivelaravel.in/budstation/resources/assets/images/icon_cartBlack.png"> 
                                              </div>
                                              <div class="ord_cartText">
                                                  <h5>Tổng số tiền thanh toán</h5>
                                                  
                                                  <span class="totalprice">{{str_replace(",",".",number_format($total))}} VNĐ</span>
                                                  <span class="percentage"> Số tiền đặt cọc {{str_replace(",",".",number_format($percentage))}} VNĐ</span>
                                              </div>                                       
                                                
                                            </div>

                                       </div>
                                       @endforeach
                                       
                                   </div>
                               </div>

                               <div class="ord_tot">
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div>
                                               <p>Để nhận được phần quà đặc biệt, bạn vui lòng hoàn tất tiền cọc (50% giá trị đơn hàng) trước ngày <span class="disdate"> DD/MM/YYYY <span>  </p>
                                                <p>BUD STATION sẽ liên hệ với bạn để hổ trợ chi tiết. Xin cảm ơn!</p>
                                                <input type="hidden" name="discount_date"  id="discount_date" value="">
                                           </div>
                                       </div>
                                       
                                   </div>
                               </div>
                           </div>


                          <div class="del_bx">  
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal">Huỷ đơn hàng  <i class="fa fa-trash-o"></i></a>
                          </div>

                       </div>
                    </div>
                    <!-- <div class="btn_bud wow fadeInDown animated">
                        <a href="{{url('choose/combo')}}">
                          <img src="{{url('resources/assets')}}/images/img_btn.png"> ĐẶT TIỆC NGAY <i class="fa fa-angle-right" aria-hidden="true"></i> 
                          <i class="fa fa-angle-right" aria-hidden="true"></i> 
                          <i class="fa fa-angle-right" aria-hidden="true"></i> 
                        </a>    
                      </div> -->
                      <div class="submitbtn">
                        <input type="submit" name="submit" class="next action-button" id="submit_order" value="" />
                    </div>
                    </form>
                </div>
              </div>
            </div>
        </div>  
    </section>
    <div class="modal fade deleteBox" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <h3>Bạn có muốn huỷ đơn hàng và trở về trang chủ không?</h3>
            </div>
            <div class="modal-footer">
              <!-- <button type="button" class="btn btn-primary" id="ok" > --><a href="javascript:void(0);" onclick="return orderdelete({{$dataid}});"> Có </a><!-- </button> -->
              <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Không</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade confirmBox deleteBox" id="confirmModal" role="dialog">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body successresult">
              <!-- <h3>Order Deleted Successfully</h3> -->
            </div>
            <div class="modal-footer">
              <a href="{{url('/')}}"> Được </a>
              <!-- <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button> -->
            </div>
          </div>
        </div>
      </div>
@endsection

