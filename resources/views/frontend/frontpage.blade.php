@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <section class="home_main">
        <div class="container home_pos">
            <div class="logo wow fadeInDown animated">      
                <a href="{{url('/')}}"><img src="{{url('resources/assets')}}/images/logo.png"></a>
            </div>
            <div class="main_hd wow fadeInDown animated">
                <h1><span><img src="{{url('resources/assets')}}/images/img_hedText.png"></span></h1>         
            </div>
            <div class="bx_cap">                      
                <img src="{{url('resources/assets')}}/images/caps.png">                
            </div>
            <div class="btn_bud wow fadeInDown animated">
                <a href="{{url('offers')}}">
                    <!-- <img src="{{url('resources/assets')}}/images/budstation-btn.png">
                    <img src="{{url('resources/assets')}}/images/budstation-btn-hover.png" class="btnhover"> -->
                </a>    
            </div>
            <div class="box_cust wow fadeInLeft animated">
                 <h4>Gọi điện hotline <span> 1800 6100 - 1 - 2</span></h4><a href="tel:18006100" class="mo_ser"><img src="{{url('resources/assets')}}/images/img_mob.png"></a> 
            </div>
        </div>  
    </section>
@endsection