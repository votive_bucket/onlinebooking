@extends('layouts.app')

@section('title', 'Combo')

@section('content')
<section class="budstation_bg stemps-all-section">
  <div class="container home_pos">
    <div class="row section-for-steps">
      <div class="col-md-12">
        <div class="box_step">
          <form id="msform" action="{{url('/saveorder')}}" method="post" onsubmit="return dateCheck(this)">
            <div class="main-step-head">
              <div class="logo wow fadeInDown animated">    
                <a href="javascript:void(0)"><img src="{{url('/resources/assets/images/')}}/logo.png"></a>
              </div>
              <ul id="progressbar">
                <li class="active">Chọn Gói</li>
                <li>Chọn quán</li>
                <li>Chọn món</li>
              </ul>
            </div>           
            <fieldset id="field1">  
              <div class="box_gift">
                <div class="bx_select">      
                  <h4>SỐ NGƯỜI THAM DỰ</h4>
                  <div class="input-group number-spinner">
                    <input type="text" class="form-control text-center" name="people" id="people" value="5" readonly="true" ondblclick="this.readOnly='';">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default" id="increamentPeople" onclick="incrementValue()" data-dir="dwn"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
                    </span>               
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default" onclick="decrementValue()" id="decreamentPeople" data-dir="up"><i class="fa fa-chevron-down" aria-hidden="true"></i></button>
                    </span>
                    @csrf                  
                  </div>
                </div>
                <input type="hidden" name="minofferamount" id="minofferamount" value="{{$minofferamount}}">
              </div> 

              <div class="middle-main-section">
                <div class="middle-combo-section">
                  <div class="bx_select_range">         
                    <div class="range">
                      <h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN DAO ĐỘNG TỪ {{str_replace(",",".",number_format((($allcombo[1]->combo_price-99000)*5)))}} VNĐ - {{str_replace(",",".",number_format(($allcombo[1]->combo_price+99000)*5))}} VNĐ<br> BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>
                    </div>                
                  </div>
                  <div class="row cmb">
                    @foreach($allcombo as $key => $value)
                    <div class="col-md-4 col-sm-4 wow fadeInLeft animated selectcombo @if($key == 1)comboselected @endif" data-dataprice="{{$value->combo_price}}" data-dataid="{{$value->combo_id}}">
                      
                      <div class="box_comb">
                        <a href="javascript:void(0);" >
                          <div class="img_comb">
                            <img src="{{ url('/public/uploads/combo/')}}/{{($key == 1)?$value->combo_selected_image:$value->combo_image}}" id="comboimg{{$key}}"/>
                          </div>
                          <div class="combo_price">{{str_replace(",",".",number_format($value->combo_price))}} VNĐ/<span>{{strip_tags($value->combo_content)}}</span></div>
                        </a>
                      </div>
                     
                    </div>
                    @endforeach
                    <input type="hidden" name="comboprice" id="comboprice" value="@if(!empty($allcombo[1]->combo_price)) {{$allcombo[1]->combo_price}} @endif">
                    <input type="hidden" name="comboid" id="comboid" value="@if(!empty($allcombo[1]->combo_id)) {{$allcombo[1]->combo_id}} @endif">   
                    <input type="hidden" name="price" id="price" value="">       
                  </div>
                </div>
              </div>

              <a href="{{url('/')}}" class="backhomepage"></a>
              <input type="button" name="next" class="next action-button" value="" id="firstcombo">

            </fieldset>
            <!-- choose combo End -->               
                                    
           <!-- choose venue start-->      
            <fieldset id="field1">
              <div class="box_gift_second">
                <div class="bx_select">   
                  <div class="input-group number-spinner countPeople">
                                     
                  </div>
                </div>                
              </div>
              <div class="middle-venue-main">
                <div class="middle-venue-section">
                  <div class="row ">
                    <div class="col-md-5 col-sm-5">
                      <div class="bx_lftScnd">
                        <div id="dd" class="wrapper-dropdown-3" tabindex="1">
                          <span>{{$defaultcity->city_name}}</span> 
                          <ul id="cityid" class="dropdown">
                            @foreach ($allcity as $key => $city)
                            <li city-id="{{$city->city_id}}">
                              <a href="#">
                                <i class="icon-envelope icon-large"></i>
                                {{$city->city_name}}
                              </a>
                            </li>
                            @endforeach
                          </ul>
                          <input type="hidden" name="hidden_city_id" id="hidden_city_id" value="1">
                        </div>
                      <ul id="list_checkBox" class="list_checkBox">
                        <?php
                        $i = 0;
                        foreach ($defaultdistricts as $key => $district) {
                        $i++; ?>
                          <li>
                            <div class="squaredTwo">
                              <input type="checkbox" value="{{$district->id}}" id="squared{{$i}}" name="dist_check"/>
                              <label for="squared{{$i}}"><span>{{$district->district_name}}</span></label>
                            </div>
                          </li>
                        <?php } ?>
                      </ul>
                      <input type="hidden" name="dist_ids" id="dist_ids">
                    </div>
                    <div class="box_mainThmb">
                      <h4>Lựa chọn thứ tự quán  bạn ưu thích</h4>
                      <input type="hidden" name="venus_ids" id="venus_ids">
                      <input type="hidden" name="venus_imgs" id="venus_imgs">
                      <ul class="list_img_thumb">
                        <li id="item1">
                          <h5>1</h5>
                        </li>
                        <li id="item2">                  
                          <h5>2</h5>                        
                        </li>
                        <li id="item3">                      
                          <h5>3</h5>                        
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-7">
                    <div class="bx_rigtScnd venues-lists ">
                    </div>
                    <div class="errormsg"></div>
                  </div>
                </div>
                </div>
              </div>
            <input type="button" name="previous" class="previous action-button" value=""/>
            <input type="button" name="next" class="next_sec next action-button" value="" id="secondvenues" />
            </fieldset>
            <!-- choose venue end-->

            <fieldset id="field1">
              <div class="box_gift_second">
                <div class="bx_select">   
                  <div class="input-group number-spinner countPeople">
                                     
                  </div>
                </div>                
              </div>
              <div class="main_bxTrd_main">
                <div class="main_bxTrd">
                <div class="menuerror"></div> 
                  <div class="row" id="menulist">
                  </div>
                </div>
              </div>               
              <input type="button" name="previous" class="previous action-button" value="" />
              <input type="submit" name="submit" class="next_sec next action-button" value="" id="thirdmenu" />
            </fieldset>
            @foreach($alloffer as $key => $value)
              <input type="hidden" name="offers_hidden_id_{{$key}}" value="{{$value->offer_id}}">
              <input type="hidden" name="offers_hidden_price_{{$key}}" value="{{$value->offer_amount}}">
              <input type="hidden" name="offers_hidden_name_{{$key}}" value="{{$value->offer_name}}">
            @endforeach
          </form>
        </div>      
      </div>
    </div>
  </div>
</section>


<div class="modal fade mod_one" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">QUÀ ĐẶC BIỆT KHI ĐẶT TIỆC</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="sub_hd">Phần quà này có thể làm thành phiếu bốc thăm may mắn cho các nhân viên tham dự tiệc hoặc dành tặng cho nhân viên xuất sắc của công ty bạn.  </p>
        <div class="row">
          @foreach($alloffer as $key => $value)
          <div class="col-md-4 col-sm-4 wow fadeInLeft animated">
            <div class="bx_price">
              <h4>{{$value->offer_name}}</h4>
              <div class="bx_poImg"><img src="{{url('/public/uploads/offer/')}}/{{$value->offer_image}}"></div>
              <h5>{{$value->offer_content}}<span>{{str_replace(",",".",number_format($value->offer_amount))}} VNĐ</span></h5>
            </div>
          </div>
          @endforeach
          
        </div>         
      </div>  
    </div>
  </div>
</div>

<div class="modal fade mod_one mod_menu" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="bx_imgPop"><img src="{{url('/resources/assets/images/')}}/img_ht1.jpg"></div>
        <div class="bx_hdPop">
          <h5 class="modal-title" id="exampleModalLabel">The Gangs</h5>
          <span>32 Mạc Đĩnh Chi, Phường Đa Kao, Quận 1, TP. HCM</span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="sub_hd">Phần quà này có thể làm thành phiếu bốc thăm may mắn cho các nhân viên tham dự tiệc hoạc dành tặng cho nhân viên xuất sắc của công ty bạn.  </p>
        <div class="row">
          <div class="col-md-5 col-sm-5 wow fadeInLeft animated">
            <div class="bx_price">
              <h4>SAMSUNG SOUNDBAR</h4>
              <div class="bx_poImg"><img src="{{url('/resources/assets/images/')}}/img_popPro.png"></div>
              <h5>Dành cho hoá đơn đặt tiệc từ<span>9.950.000 VNĐ</span></h5>
            </div>
          </div>
          <div class="col-md-7 col-sm-7 wow fadeInLeft animated">
            <div class="bx_price">
              <h4>PHIẾU MUA HÀNG TRỊ GIÁ 2 TRIỆU</h4>
              <div class="bx_poImg"><img src="{{url('/resources/assets/images/')}}/img_popPro2.png"></div>
              <h5>Dành cho hoá đơn đặt tiệc từ<span>19.900.000 VNĐ</span></h5>
            </div>
          </div>  
        </div>         
      </div>  
    </div>
  </div>
</div>

<div class="modal fade mod_one" id="venueModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content venue_details"> 
    </div>
  </div>
</div>
@endsection