<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>@yield('title') - Bud Station</title>

        <link rel="shortcut icon" href="{{url('/')}}/favicon.ico" type="image/x-icon">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/bootstrap.min.css') }}">        
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/font-awesome.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/master.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/master_style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/owl.carousel.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/owl.theme.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/animate.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/bootstrap-multiselect.css') }}">
        <!-- <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/jquery.mCustomScrollbar.css') }}"> -->
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">        
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

        <meta property="og:title" content="" />
        <meta property="og:type" content="article" />
        <meta property="og:image" content="{{url('resources/assets/images')}}/fb-share.png" />
        <meta property="og:url" content="{{url('/')}}/thankyou" />
        <meta property="og:description" content="" />
        <meta property="og:image:width"  />
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="300" />

    </head>
    <?php
    $custom_class = '';
    if (Request::is('/')) {
        $custom_class = 'home_bg';
    } elseif (Request::is('thankyou')) {
       $custom_class = 'thankyou_bg';
    } else {
        $custom_class = 'box_comn';
    }      
    ?>
    <body class="{{$custom_class}}">

        @yield('content')



        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../js/minified/jquery-1.11.0.min.js"><\/script>')</script>
        <!-- <script src="{{ url('resources/assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script> --> 
        <!-- <script>
        (function($){
            $(window).on("load",function(){
                
                $("a[rel='load-content']").click(function(e){
                    e.preventDefault();
                    var url=$(this).attr("href");
                    $.get(url,function(data){
                        $(".content .mCSB_container").append(data); //load new content inside .mCSB_container
                        //scroll-to appended content 
                        $(".content").mCustomScrollbar("scrollTo","h2:last");
                    });
                });
                
                $(".content").delegate("a[href='top']","click",function(e){
                    e.preventDefault();
                    $(".content").mCustomScrollbar("scrollTo",$(this).attr("href"));
                });
                
            });
        })(jQuery);
    </script> -->

        <!-- jQuery -->
        <!-- <script type="text/javascript" src="{{ url('resources/assets/js/jquery-3.2.1.min.js') }}"></script> -->
        <script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <script type="text/javascript" src="{{ url('resources/assets/js/wow.min.js') }}"></script> 
        <script type="text/javascript" src="{{ url('resources/assets/js/bootstrap.min.js') }}"></script> 
        <script type="text/javascript" src="{{ url('resources/assets/js/owl.carousel.min.js') }}"></script>
     
        <script src="{{ url('resources/assets/js/bootstrap-multiselect.min.js') }}"></script>
        <script src="{{ url('resources/assets/js/jquery.validate.min.js') }}"></script>


        <script type="text/javascript">
          function orderdelete(id){         

            $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });    
                $.ajax({
                  url: "{{url('delete')}}/"+id,
                  type:'get',
                  success: function(data){                    
                    var res = JSON.parse(data);
                    if(res.status == 1){
                      $("#myModal").modal('hide');
                      $(".successresult").text(res.msg);
                      $("#confirmModal").modal('show');
                      setTimeout( function(){ 
                        window.location="{{url('/')}}";
                      }  , 4000 );

                    }else{
                      $("#myModal").modal('hide');
                      $(".successresult").text(res.msg);
                      $("#confirmModal").modal('show');
                    }
                  },
                  error: function(e){
                    console.log(e);
                  },
                });
          }

        </script>
        <script type="text/javascript">
          var baseurl = "{{url('/')}}";
        </script> 
        <script>
            wow = new WOW(
            {
               animateClass: 'animated',
               offset: 100
            }
            );
            wow.init();
            
        </script>
        <script>
            $(document).ready(function(){
                var date_input=$('input[name="date"]'); //our date input has the name "date"
                var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
                var date = new Date();
                var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                            
                var discountdate = '';
                $(function() {
                  date_input.datepicker({
                    startDate: today,
                    //format: 'dd/mm/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true,
                    onSelect: function(_date, _datepicker){
                       var date = new Date();
                      var myDate = new Date(_date);
                      myDate.setDate(myDate.getDate()-7);
                      if(myDate <= date){
                         date.setDate(date.getDate()+1);
                         var d = date.getDate();
                          var m =  date.getMonth();
                          m += 1;  // JavaScript months are 0-11
                          var y = date.getFullYear();

                         $("span.disdate").text(d + "/" + m + "/" + y);
                         $("#discount_date").val(d + "/" + m + "/" + y);
                      }else{
                         var d = myDate.getDate();
                          var m =  myDate.getMonth();
                          m += 1;  // JavaScript months are 0-11
                          var y = myDate.getFullYear();
                        $("span.disdate").text(d + "/" + m + "/" + y);
                        $("#discount_date").val(d + "/" + m + "/" + y);
                      }
                      
                    }
                  })
                  var date ="";
                });
                
            })
        </script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

        <script type="text/javascript">
           var list_indexing = 0;
            var test = {};
            var testimg = {};
            var globleindex = 0;

            var new_conc = {};
            new_conc[0] = null;
            new_conc[1] = null;
            new_conc[2] = null;
        </script>

        <script type="text/javascript">
            function venueIds(){
               //$("ul.list_img_thumb")
               var data_vid = '';
               $.each($("ul.list_img_thumb li"), function(index, value){
                  var datavid = $(this).attr('data-vid');
                  if(datavid !== undefined && datavid !== null && datavid !== 'undefined' && datavid !== '')
                  {
                     data_vid = data_vid+datavid+",";
                  }
               });
               $("#venus_ids").val(data_vid);
            }
        </script>

        <script type="text/javascript">
            $(document).on('click', '.venue-info', function() {
                $("#venueModal").modal('show');
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });    
                $.ajax({
                  url: baseurl+"/venue/details",
                  type:'get',
                  data: {
                    venueid : jQuery(this).attr('venueid'),
                    countPeople : jQuery('#countPeople').val(),
                    comboid : jQuery('#comboid').val(),
                  },
                  success: function(data){
                    var dataSearch = JSON.parse(data);
                    $(".venue_details").empty();
                    $(".venue_details").html('');
                    $(".venue_details").html(dataSearch.venueview);
                  },
                  error: function(e){
                    console.log(e);
                  },
                });
            });

            function isCheck(venue_id)
            {
              //logic
              if ($("input[name=venue_check][value="+venue_id+"]").is(':checked')) {
                return false;
              } else {
                return true;
              }
            }

            $(document).on('click', '.select-venu', function() {
               var venue_id = jQuery(this).attr('venue_id');
               var venue_img = jQuery(this).attr('venue_img');

                var res = isCheck(venue_id);
                
                if(res)
                {
                    $("input[name=venue_check][value="+venue_id+"]").prop("checked",true);

                    var $checkboxes = $("input[name='venue_check']:checked");
                    var cnt = $checkboxes.filter(':checked').length;
                    
                    if(cnt >= 1 && cnt <= 3)
                    {
                        $("input[name=venue_check][value="+venue_id+"]").prop("checked",true);
                        if(new_conc[0] === null || new_conc[0] === undefined || new_conc[0] === 'undefined' || new_conc[0] === '')
                       {
                          new_conc[0] = venue_id;

                          /*$(".list_img_thumb li#item1").addClass('active img_thumb');
                          $(".list_img_thumb li#item1").attr('data-vid',venue_id); 
                          $(".list_img_thumb li#item1").html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');*/

                       } else if(new_conc[1] === null || new_conc[1] === undefined || new_conc[1] === 'undefined' || new_conc[1] === '') {
                          new_conc[1] = venue_id;

                          /*$(".list_img_thumb li#item2").addClass('active img_thumb');
                          $(".list_img_thumb li#item2").attr('data-vid',venue_id); 
                          $(".list_img_thumb li#item2").html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');*/

                       } else if(new_conc[2] === null || new_conc[2] === undefined || new_conc[2] === 'undefined' || new_conc[2] === '') {
                          new_conc[2] = venue_id;

                          /*$(".list_img_thumb li#item3").addClass('active img_thumb');
                          $(".list_img_thumb li#item3").attr('data-vid',venue_id); 
                          $(".list_img_thumb li#item3").html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');*/

                       }

                        $.each($(".list_img_thumb li"), function(index, value){
                          var $list = $(this).hasClass('active');
                          if(!$list)
                          {
                            var id = $(this).attr('id');
                            
                            $(".list_img_thumb li#"+id).addClass('active img_thumb');
                            $(".list_img_thumb li#"+id).attr('data-vid',venue_id); 
                            $(".list_img_thumb li#"+id).html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');

                            return false;
                          }
                        });

                       venueIds();
                    }
                    else
                    {
                      /*$(this).prop('checked', false);*/
                      $("input[name=venue_check][value="+venue_id+"]").prop("checked",false);
                      alert('You can select only three items.');
                    }
                }                
            });
        </script>

        <script>
        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $("#secondvenues").click(function(){ 
            var venue = $("#venus_ids").val(); 
            var venuearr = venue.split(',');
            if(venue != '' && venuearr.length == 4){
                if(animating) return false;
                animating = true;
                //if(formNext1.valid() == true){ 
                var venusid2 = [];
                $('.list_img_thumb li').each(function(i){
                    venusid2.push($(this).data('vid'));
                });
                $('#venus_ids').val(venusid2);
                if($("#venus_ids").val()!=''){
                    $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });    
                    $.ajax({
                      url: baseurl+"/get/menus",
                      type:'get',
                      data: {
                        venus_ids : jQuery('#venus_ids').val(),
                        cid: jQuery('#comboid').val()
                      },
                      beforeSend: function() {
                        $("#loading_image").show();
                      },
                      success: function(data){
                        console.log(data);
                        var i = 0;
                        var dataSearch = JSON.parse(data);

                        $("#menulist").html('');
                        $("#menulist").html(dataSearch.menuview);
                      },
                      error: function(e){
                        console.log(e);
                      },
                      complete: function(){
                        $('#loading_image').hide();
                      }
                    }); 
                }
                
                current_fs = $(this).parent();
                next_fs = $(this).parent().next();            
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                
                
                //show the next fieldset
                next_fs.show(); 
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now, mx) {
                        
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50)+"%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({'transform': 'scale('+scale+')'});
                        next_fs.css({'left': left, 'opacity': opacity});
                    }, 
                    duration: 800, 
                    complete: function(){
                        current_fs.hide();
                        animating = false;
                    }, 
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
             //}
            }else{
                $('div .errormsg').text('Xin vui lòng chọn 3 địa điễm');
            }
        });
        
        $("#firstcombo").click(function(){
            if(animating) return false;
            animating = true;

            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });    
            $.ajax({
              url: baseurl+"/default/venues",
              type:'get',
              data: {
                cid : jQuery('#comboid').val(),
                capacity : jQuery('#people').val()
              },
              success: function(data){
                var i = 0;
                var dataSearch = JSON.parse(data);
                $(".venues-lists").empty();
                $(".venues-lists").html('');
                //$(".list_img_thumb").empty();
                $(".venues-lists").html(dataSearch.venueview);
              },
              error: function(e){
                console.log(e);
              },
            });

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();            
            //activate next step on progressbar using the index of next_fs
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            var people = $("#people").val();

            $(".countPeople").html(people+' NGƯỜI THAM DỰ </br> <input type="hidden" id="countPeople" value="'+people+'"/>');
            //show the next fieldset
            next_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale('+scale+')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });
        $(document).on('change', '.menuchecked', function() { 
            for(var i= 1; i<=3; i++){ 
                
                var price = $('input[name="menucheck'+i+'"]:checked').data("menuprice"+i);             
                var noOfPeople = parseFloat($("#countPeople").val(), 10);
                var total = 0;
                var hiddentotal =0;
                var box_gift_a = "#box_gift"+i+" a";
                var box_giftCont = '#box_giftCont'+i+' .totalprice'+i;


                if(!isNaN(price)){
                    if(noOfPeople != '' && (!isNaN(noOfPeople))){
                        hiddentotal = noOfPeople * price;
                        total = addCommas(noOfPeople *  price);
                        $(box_giftCont).text(total.replace(/,/g, "."));                   
                    }
                    var min = parseFloat(document.getElementById('minofferamount').value);
                    if(hiddentotal >= min){
                        $(box_gift_a).addClass('applied');
                    }else{
                        $(box_gift_a).removeClass('applied');
                    }
                    $("#total"+i).val(hiddentotal);
                }
            }
            
        });

        function dateCheck(){ 
           if($("input[name='menucheck1']").is(":checked") && $("input[name='menucheck2']").is(":checked") && $("input[name='menucheck3']").is(":checked")){

                var menu1 = $("input[name='menucheck1']:checked").val();
                var menu2 = $("input[name='menucheck2']:checked").val();
                var menu3 = $("input[name='menucheck3']:checked").val();
                
           }else{
                $('.menuerror').html('Xin vui lòng chọn 1 menu từ mỗi địa điểm.');
                return false;
           }
        }

        $(".previous").click(function(){
            if(animating) return false;
            animating = true;
            
            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();
            
            //de-activate current step on progressbar
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
            $("#progressbar li").eq($("fieldset").index(previous_fs)).addClass("active");

            //show the previous fieldset
            previous_fs.show(); 
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1-now) * 50)+"%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                }, 
                duration: 800, 
                complete: function(){
                    current_fs.hide();
                    animating = false;
                }, 
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        </script>

        <!-- Get district and venues on behalf on city -->
        <script type="text/javascript">
            $('#cityid li').click(function(){
                var cityID = $(this).attr('city-id');
                $('#hidden_city_id').val(cityID);
                var i = 0;
                if(cityID){
                    $.ajax({
                       type:"GET",
                       url:baseurl+"/venue/get-district-list",
                       data: {
                        city_id : cityID,
                        cid : jQuery('#comboid').val(),
                        capacity : jQuery('#people').val()
                      },
                       success:function(res){
                       var dataSearch = JSON.parse(res);               
                        if(res){
                            $("#list_checkBox").empty();
                            $(".venues-lists").empty();
                            $(".venues-lists").html('');
                            $(".venues-lists").html(dataSearch.venueview);
                            $.each(dataSearch.districts,function(key,value){
                                i++;
                                $("#list_checkBox").append('<li><div class="squaredTwo"><input type="checkbox" value="'+value.id+'" id="squared'+i+'" name="dist_check" /><label for="squared'+i+'"><span>'+value.district_name+'</span></label></div></li>');
                            });        
                        }else{
                           $("#list_checkBox").empty();
                        }
                       }
                    });
                } else {
                    $("#list_checkBox").empty();
                }                
            });

            $(document).on('change', '.list_checkBox li', function() { 
                var distircts = [];
                $.each($("input[name='dist_check']:checked"), function(){            
                    distircts.push($(this).val());
                });
                $('#dist_ids').val(distircts);
                FilterVenues();
            });

            function FilterVenues(){
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });    
                $.ajax({
                  url: baseurl+"/filter/venues",
                  type:'get',
                  data: {
                    cityID : jQuery('#hidden_city_id').val(),
                    dist_ids : jQuery('#dist_ids').val(),
                    cid : jQuery('#comboid').val(),
                    capacity : jQuery('#people').val(),
                  },
                  beforeSend: function() {
                    $("#loading_image").show();
                  },
                  success: function(data){
                    console.log(data);
                    var i = 0;
                    var dataSearch = JSON.parse(data);
                    $(".venues-lists").empty();
                    $(".venues-lists").html('');
                    //$(".list_img_thumb").empty();
                    $(".venues-lists").html(dataSearch.venueview);
                  },
                  error: function(e){
                    console.log(e);
                  },
                  complete: function(){
                    $('#loading_image').hide();
                  }
                });    
            }

            function count(array){

                /*var c = 0;
                for(i in array) // in returns key, not object
                    if(array[i] != undefined)
                        c++;

                return c;*/

                var count = 0;
                var indexing=0;

                for (indexing in array) {
                    if (array.hasOwnProperty(indexing)) {
                        count++;
                    }
                    indexing++;
                }
                return count;
            }
            
            $(document).on('change', "input[name='venue_check']", function() { 
               var venus = [];
               var venusid = [];
               var z = 0 ; 
               var selector = ''; 
               if($(this).is(':checked'))
               {  
                  globleindex++;
                  if(globleindex>3)
                  {
                     globleindex = 1;
                  }
                 
                  if($('#item'+globleindex).has('img').length>0)
                  {
                     globleindex++;
                  }
               
                  var checked = $(this).is(":checked");
                  var venue_id = $(this).val();
                  var venue_img = $(this).data('imgsrc'); 
                  /*$(".list_img_thumb li#item"+globleindex).addClass('active img_thumb');
                  $(".list_img_thumb li#item"+globleindex).attr('data-vid',venue_id); 
                  $(".list_img_thumb li#item"+globleindex).html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');*/


                     var $checkboxes = $("input[name='venue_check']:checked");
                     var cnt = $checkboxes.filter(':checked').length;

                     if(cnt >= 1 && cnt <= 3)
                     {
                            if(new_conc[0] === null || new_conc[0] === undefined || new_conc[0] === 'undefined' || new_conc[0] === '')
                            {
                              new_conc[0] = venue_id;

                              /*$(".list_img_thumb li#item1").addClass('active img_thumb');
                              $(".list_img_thumb li#item1").attr('data-vid',venue_id); 
                              $(".list_img_thumb li#item1").html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');*/

                            } else if(new_conc[1] === null || new_conc[1] === undefined || new_conc[1] === 'undefined' || new_conc[1] === '') {
                              new_conc[1] = venue_id;

                              /*$(".list_img_thumb li#item2").addClass('active img_thumb');
                              $(".list_img_thumb li#item2").attr('data-vid',venue_id); 
                              $(".list_img_thumb li#item2").html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');*/

                            } else if(new_conc[2] === null || new_conc[2] === undefined || new_conc[2] === 'undefined' || new_conc[2] === '') {
                              new_conc[2] = venue_id;

                              /*$(".list_img_thumb li#item3").addClass('active img_thumb');
                              $(".list_img_thumb li#item3").attr('data-vid',venue_id); 
                              $(".list_img_thumb li#item3").html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');*/

                            }

                            $.each($(".list_img_thumb li"), function(index, value){
                              var $list = $(this).hasClass('active');
                              if(!$list)
                              {
                                var id = $(this).attr('id');
                                
                                $(".list_img_thumb li#"+id).addClass('active img_thumb');
                                $(".list_img_thumb li#"+id).attr('data-vid',venue_id); 
                                $(".list_img_thumb li#"+id).html('<img class="myt'+venue_id+'" src="'+baseurl+'/public/uploads/venue/'+venue_img+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');

                                return false;
                              }
                            });
                            /*$.each($(".list_img_thumb li"), function(index, value){
                                var sel = $(this).attr('data-vid');
                                var id = $(this).attr('id');
                                if(sel === venue_id)
                                {
                                  $(".list_img_thumb li").removeClass('active img_thumb');
                                  $(".list_img_thumb li#"+id).attr('data-vid','');
                                  var content = '';
                                  if(id == 'item1')
                                  {
                                     content = '<h5>1</h5>';
                                  } else if(id == 'item2') {
                                     content = '<h5>2</h5>';
                                  } else if(id == 'item3') {
                                     content = '<h5>3</h5>';
                                  }
                                  $(".list_img_thumb li#"+id).html(content);
                                }
                            });*/

                     }
                     else
                     {
                        $(this).prop('checked', false);
                        alert('You can select only three options');
                     }

                  /*$.each($("input[name='venue_check']:checked"), function(index, value){

                     var checked = $(this).is(":checked");
                     var venue_id = $(this).val();
                     var venue_img = $(this).data('imgsrc');
                     //alert(z);
                     
                     if (checked) { 

                        var match = 0;
                        var total = count(test);
                        for (var j = 0; j < total; j++)
                        {
                           if (test[j] === venue_id) 
                           {
                              match = 1;
                           }
                        }
                        if(match === 0)
                        {
                           if(venue_id !== undefined && venue_id !== null && venue_id !== 'undefined' && venue_id !== '')
                           {
                              test[list_indexing] = venue_id;
                              testimg[list_indexing] = venue_img;
                              list_indexing++;
                           }
                           
                        }                     
                        var vid = '';
                        var vimg = '';

                        for (var k = 0; k < count(test); k++) 
                        {
                           vid += test[k]+',';
                           vimg += testimg[k]+','; 
                           //console.log(i);
                           //$(".list_img_thumb li#item"+i).empty();
                           //$(".list_img_thumb li#item"+i).addClass('active img_thumb');
                           //$(".list_img_thumb li#item"+i).attr('data-vid', test[k]); 
                           //$(".list_img_thumb li#item"+i).html('<img class="myt'+test[k]+'" src="'+baseurl+'/public/uploads/venue/'+testimg[k]+'"><span><i class="fa fa-times" aria-hidden="true"></i></span>');
                        }
                        //console.log(test);
                        vids = vid.replace(/,\s*$/, "");
                        vimgs = vimg.replace(/,\s*$/, "");
                        //console.log(test);
                     }
                     z++;  
                  });
                  $('#venus_imgs').val(vimgs);
                  $('#venus_ids').val(vids);
                  console.log(test);*/
               }


               var checked = $(this).is(":checked");
               if(!checked)
               {
                  var venue_id = $(this).val();
                  var venue_img = $(this).data('imgsrc');
                  if(new_conc[0] === venue_id)
                  {
                     /*new_conc[0] = null;*/
                     var first_res = new_conc[1];
                     var second_res = new_conc[2];
                     new_conc[0] = first_res;
                     new_conc[1] = second_res;
                     new_conc[2] = null;



                     /*$(".list_img_thumb li#item1").removeClass('active img_thumb');
                     $(".list_img_thumb li#item1").attr('data-vid',''); 
                     $(".list_img_thumb li#item1").html('<h5>1</h5>');*/

                  } else if(new_conc[1] === venue_id) {
                     /*new_conc[1] = null;*/
                     var second_res = new_conc[2];
                     new_conc[1] = second_res;
                     new_conc[2] = null;

                     /*$(".list_img_thumb li#item2").removeClass('active img_thumb');
                     $(".list_img_thumb li#item2").attr('data-vid',''); 
                     $(".list_img_thumb li#item2").html('<h5>2</h5>');*/

                  } else if(new_conc[2] === venue_id) {
                     new_conc[2] = null;


                     /*$(".list_img_thumb li#item3").removeClass('active img_thumb');
                     $(".list_img_thumb li#item3").attr('data-vid',''); 
                     $(".list_img_thumb li#item3").html('<h5>3</h5>');*/

                  }


                  $.each($(".list_img_thumb li"), function(index, value){
                      var sel = $(this).attr('data-vid');
                      var id = $(this).attr('id');
                      if(sel === venue_id)
                      {
                        $(".list_img_thumb li#"+id).removeClass('active img_thumb');
                        $(".list_img_thumb li#"+id).attr('data-vid','');
                        var content = '';
                        if(id == 'item1')
                        {
                           content = '<h5>1</h5>';
                        } else if(id == 'item2') {
                           content = '<h5>2</h5>';
                        } else if(id == 'item3') {
                           content = '<h5>3</h5>';
                        }
                        $(".list_img_thumb li#"+id).html(content);
                      }
                 });


               }

               var checked_ddd = true;
               if(!checked_ddd)
               {
                  /*var venue_img = $(this).data('imgsrc');
                  var retestimg = {};*/

                  /*var retest = {};
                  var venue_id = $(this).val();
                  var total = count(test);

                  var new_indexing = 0;
                  for (var j = 0; j < total; j++)
                  {
                   if (test[j] !== venue_id) 
                   {
                        if(test[j] !== undefined && test[j] !== null && test[j] !== 'undefined' && test[j] !== '')
                        {
                           retest[new_indexing] = test[j];
                           //retestimg[k] = testimg[j];
                           new_indexing++;
                        }
                     
                   }
                  }

                  test = retest;
                  list_indexing = new_indexing;
                  console.log(test);*/

                  var retest = {};
                  console.log(test);
                  var new_indexing = 0;
                  $.each($("input[name='venue_check']:checked"), function(index, value){

                     var checked = $(this).is(":checked");
                     var venue_id = $(this).val();
                     var venue_img = $(this).data('imgsrc');
                     //alert(z);

                     
                     
                     if (checked) { 

                        var match = 0;
                        var total = count(test);
                        console.log(total);
                        if(total === 0)
                        {
                           retest[new_indexing] = venue_id;
                           new_indexing++;
                        }
                        else
                        {
                           console.log(test);
                           for (var j = 0; j < total; j++)
                           {
                              if (test[j] === venue_id) 
                              {
                                 retest[j] = venue_id;
                                 new_indexing++;
                              }
                           }   
                        }
                        

                        /*var match = 0;
                        var total = count(test);
                        for (var j = 0; j < total; j++)
                        {
                           if (test[j] === venue_id) 
                           {
                              match = 1;
                           }
                        }
                        if(match === 0)
                        {
                           if(venue_id !== undefined && venue_id !== null && venue_id !== 'undefined' && venue_id !== '')
                           {*/
                              //retest[new_indexing] = venue_id;
                              //new_indexing++;
                           /*}
                           
                        }*/                     
                        
                     } 
                  });
                  
                  var newArr = {};
                  var new_arr=0;
                  for (var i in retest) {
                       newArr[new_arr] = retest[i];
                       new_arr++;
                  }
                  test = newArr;
                  list_indexing = new_indexing;

                 
               }
               venueIds();
               //console.log(new_conc);
            });

            $( ".list_img_thumb" ).sortable({
                helper: 'clone',
                opacity: 0.5,
                cursor: "move",
                connectWith: ".list_img_thumb",
                /*receive: function(event, ui) {
                  ui.item.appendTo("#list2");
                }*/
                stop: function(event, ui) {
                    
                    venueIds();
                }
            });
            //$( ".list_img_thumb" ).disableSelection();

            $('.list_img_thumb').on('click', '.fa-times', function() {
               var sel = $(this).parents('.img_thumb').attr('data-vid');
               var id = $(this).parents('.img_thumb').attr('id');
               if(new_conc[0] === sel)
               {
                  /*new_conc[0] = null;*/
                  var first_res = new_conc[1];
                  var second_res = new_conc[2];
                  new_conc[0] = first_res;
                  new_conc[1] = second_res;
                  new_conc[2] = null;


                  /*$(this).parents('.img_thumb').html('<h5>1</h5>');
                  $(".list_img_thumb li#item1").removeClass('active img_thumb');
                  $(".list_img_thumb li#item1").attr('data-vid',''); 
                  $(".list_img_thumb li#item1").html('<h5>1</h5>');*/

                  $(".list_img_thumb li#"+id).removeClass('active img_thumb');
                  $(".list_img_thumb li#"+id).attr('data-vid','');
                  var content = '';
                  if(id == 'item1')
                  {
                     content = '<h5>1</h5>';
                  } else if(id == 'item2') {
                     content = '<h5>2</h5>';
                  } else if(id == 'item3') {
                     content = '<h5>3</h5>';
                  }
                  $(".list_img_thumb li#"+id).html(content);

               } else if(new_conc[1] === sel) {
                  /*new_conc[1] = null;*/
                  var second_res = new_conc[2];
                  new_conc[1] = second_res;
                  new_conc[2] = null;

                  /*$(".list_img_thumb li#item2").removeClass('active img_thumb');
                  $(".list_img_thumb li#item2").attr('data-vid',''); 
                  $(".list_img_thumb li#item2").html('<h5>2</h5>');*/
                  $(".list_img_thumb li#"+id).removeClass('active img_thumb');
                  $(".list_img_thumb li#"+id).attr('data-vid','');
                  var content = '';
                  if(id == 'item1')
                  {
                     content = '<h5>1</h5>';
                  } else if(id == 'item2') {
                     content = '<h5>2</h5>';
                  } else if(id == 'item3') {
                     content = '<h5>3</h5>';
                  }
                  $(".list_img_thumb li#"+id).html(content);
                  
               } else if(new_conc[2] === sel) {
                  new_conc[2] = null;
                  /*$(".list_img_thumb li#item3").removeClass('active img_thumb');
                  $(".list_img_thumb li#item3").attr('data-vid',''); 
                  $(".list_img_thumb li#item3").html('<h5>3</h5>');*/

                  $(".list_img_thumb li#"+id).removeClass('active img_thumb');
                  $(".list_img_thumb li#"+id).attr('data-vid','');
                  var content = '';
                  if(id == 'item1')
                  {
                     content = '<h5>1</h5>';
                  } else if(id == 'item2') {
                     content = '<h5>2</h5>';
                  } else if(id == 'item3') {
                     content = '<h5>3</h5>';
                  }
                  $(".list_img_thumb li#"+id).html(content);
               }

                $.each($("input[name='venue_check']"), function(){
                    var unc = $(this).val();
                    if (unc === sel) {
                        $(this).prop('checked',false);
                    }
                });
                venueIds();
            });

            $(document).on('change', "input[name='venue_check']", function() {
              var checked = $(this).is(":checked");
              ;
              if(!checked){
               test = {};
               testimg = {};
               retestimg = {};
              var vall = $(this).val();
              var vall11 = $(".img_thumb[data-vid='" + vall +"']").index(); 
              var tt = parseInt(vall11);
              globleindex = tt;
              tt++;
              
              $("#item"+tt).html('<h5>'+tt+'</h5>');
                $('.myt'+vall).remove();
              }


             });
            
        </script>   
        <script type="text/javascript">            
            function DropDown(el) {
                this.dd = el;
                this.placeholder = this.dd.children('span');
                this.opts = this.dd.find('ul.dropdown > li');
                this.val = '';
                this.index = -1;
                this.initEvents();
            }
            DropDown.prototype = {
                initEvents : function() {
                    var obj = this;

                    obj.dd.on('click', function(event){
                        $(this).toggleClass('active');
                        return false;
                    });

                    obj.opts.on('click',function(){
                        var opt = $(this);
                        obj.val = opt.text();
                        obj.index = opt.index();
                        obj.placeholder.text(obj.val);
                    });
                },
                getValue : function() {
                    return this.val;
                },
                getIndex : function() {
                    return this.index;
                }
            }

            $(function() {
                var dd = new DropDown( $('#dd') );
                $(document).click(function() {
                    // all dropdowns
                    $('.wrapper-dropdown-3').removeClass('active');
                });
            });
        </script>


        <script type="text/javascript">            
            $(document).ready(function () {
              $('.checkAll').on('click', function () {
                $(this).closest('table').find('tbody :checkbox')
                  .prop('checked', this.checked)
                  .closest('tr').toggleClass('selected', this.checked);
              });

              $('tbody :checkbox').on('click', function () {
                $(this).closest('tr').toggleClass('selected', this.checked); //Classe de seleção na row
              
                $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
              });
            });  
        </script>


        <!-- Combo Package -->
        <script type="text/javascript">
            $(".selectcombo").click(function(){
                $('div .selectcombo').removeClass('comboselected');
                $(this).addClass('comboselected');
                var b ='';
                for(var i=0; i<3; i++){
                    var a = $('#comboimg'+i).attr('src');
                    var b = a.replace('_selected','');
                    $('#comboimg'+i).attr('src', b);
                }
                var imgsource = $(this).find('img').attr('src');
                //var imgname = imgsource.split(".");
                var img = imgsource.split(/\.(?=[^\.]+$)/);                  
                var n = img.includes("_selected");

                if(n==false){                    
                    $(this).find('img').attr('src', img[0]+'_selected.'+img[1]);
                }else{
                    $('div .img_comb img').attr('src', b);
                }
                // if img src contains single dot(.)
                /*var str = imgname[0];
                var n = str.includes("_selected");

                if(n==false){                    
                    $(this).find('img').attr('src', imgname[0]+'_selected.'+imgname[1]);
                }else{
                    $('div .img_comb img').attr('src', b);
                }*/
                //end

                /*var amount = $("#disk").slider("value") * 1.60;
                $("#diskamountUnit").val('$' + amount.toFixed(2));*/
               
                var val5 = parseFloat(document.getElementById('people').value);
                var price = $(this).data('dataprice'); 
                var from = addCommas(price * val5);     
                var p = parseFloat(price-99000);
                var to = addCommas(parseFloat(p)*val5);
                var id = $(this).data('dataid');
                var cid = $("#comboid").val();
                var secondcombo_to = parseFloat(price)+99000;
                var sec_to = addCommas(parseFloat(secondcombo_to)*val5);
                $(".range").html('');
                if(id == 1){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if(id == 2){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN DAO ĐỘNG TỪ '+to.replace(/,/g, ".")+' VNĐ – '+sec_to.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if (id == 3) {
                    $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                } 
                $("#price").val(price);
                $("#comboprice").val(price);
                $("#comboid").val(id);

            });

            function incrementValue()
            {   
                var val5 = parseFloat(document.getElementById('people').value);
                var value = round5(val5);
                if(value < 300){
                    value = isNaN(value) ? 5 : value;
                    value += 5;                    
                }else{
                    if($('#people').is('[readonly]')){
                        value = 300;                        
                    }else{
                        value = isNaN(value) ? 5 : value;
                        value += 5;                        
                    }
                }

                var price = $("#comboprice").val();
                var from = addCommas(price * value);
                var p = parseFloat($("#comboprice").val())-99000;
                var to = addCommas(parseFloat(p)*value);
                var cid = $("#comboid").val();

                var secondcombo_to = parseFloat($("#comboprice").val())+99000;
                var sec_to = addCommas(parseFloat(secondcombo_to)*value);
                
                $(".range").html('');
                if(cid == 1){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if(cid == 2){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN DAO ĐỘNG TỪ '+to.replace(/,/g, ".")+' VNĐ – '+sec_to.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if (cid == 3) {
                    $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }
                
                if(value == 300){
                  value = "300+";
                }
                var min = parseFloat(document.getElementById('minofferamount').value);
                document.getElementById('people').value = value;
            }

            function decrementValue()
            {   
                var val5 = parseFloat(document.getElementById('people').value);
                var value = val5;
                if(value > 5){                      
                    if($('#people').is('[readonly]')){
                        //value = rounddecrease5(val5);
                        value = isNaN(value) ? 5 : value;
                        value -= 5;                        
                    }else{
                        $('#people').prop('readonly', true);
                        var value = rounddecrease5(val5);
                        value = isNaN(value) ? 5 : value;
                        value -= 5; 
                    }               
                }else{
                    value = 5;
                }
                var price = $("#comboprice").val();
                var from = addCommas(price * value);
                var p = parseFloat($("#comboprice").val())-99000;
                var to = addCommas(parseFloat(p)*value);
                var secondcombo_to = parseFloat($("#comboprice").val())+99000;
                var sec_to = addCommas(parseFloat(secondcombo_to)*value);
                var cid = $("#comboid").val();
                $(".range").html('');
                if(cid == 1){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if(cid == 2){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN DAO ĐỘNG TỪ '+to.replace(/,/g, ".")+' VNĐ – '+sec_to.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if (cid == 3) {
                    $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }
                /*
                $(".range").html('');
                $(".range").html('<h4>MUC GIA BUỔI TIỆC CỦA BẠN LÀ TỰ '+from.replace(/,/g, ".")+' VND - '+to.replace(/,/g, ".")+' VND </br>BẠN SẼ ĐƯỢC GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');*/
                var min = parseFloat(document.getElementById('minofferamount').value);
                document.getElementById('people').value = value;
            }

            $('#people').keypress(function (event){
                var keycode = event.which;
                if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))){
                  event.preventDefault();
                }              
            });
            $('#people').keyup(function (event) {
                var value = this.value; 
                var price = parseFloat(document.getElementById('comboprice').value);
                $("#price").val(price);
                document.getElementById('people').value = value;
                
                var price = $("#comboprice").val();
                var from = addCommas(price * value);
                var p = parseFloat(price)-99000;
                var to = addCommas(parseFloat(p)*value);
                var cid = $("#comboid").val();

                var secondcombo_to = parseFloat($("#comboprice").val())+99000;
                var sec_to = addCommas(parseFloat(secondcombo_to)*value);
                $(".range").html('');
                if(cid == 1){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if(cid == 2){
                  $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN DAO ĐỘNG TỪ '+to.replace(/,/g, ".")+' VNĐ – '+sec_to.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }else if (cid == 3) {
                    $(".range").html('<h4>CHI PHÍ CHO BUỔI TIỆC CỦA BẠN TẠM TÍNH LÀ '+from.replace(/,/g, ".")+' VNĐ <br>BẠN SẼ CÓ GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');
                }
                /*$(".range").html('');
                $(".range").html('<h4>MUC GIA BUỔI TIỆC CỦA BẠN LÀ TỰ '+from.replace(/,/g, ".")+' VND - '+to.replace(/,/g, ".")+' VND </br>BẠN SẼ ĐƯỢC GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');*/

               /* var price = $("#comboprice").val();
                $(".range").html('');
                $(".range").html('<h4>MUC GIA BUỔI TIỆC CỦA BẠN LÀ TỰ '+(price * value)+' VND - '+((parseFloat(price)+99000) *value)+' VND </br>BẠN SẼ ĐƯỢC GIÁ CHÍNH XÁC KHI CHỌN MÓN ĂN</h4>');*/ 
           });


            function round5(x)
            {   
                if(x>=5){
                    return (x % 5) >= 2.5 ? parseInt(x / 5) * 5 : parseInt(x / 5) * 5;
                }
            }
            function rounddecrease5(x)
            {   
                if(x>=5){
                    return (x % 5) >= 2.5 ? parseInt(x / 5) * 5+5 : parseInt(x / 5) * 5+5;
                }
            }
            function addCommas(nStr) {
                nStr += '';
                var x = nStr.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
        </script>
        <script type="text/javascript">


            $("#userform").validate({
                rules:{
                    date:{
                        required:true,
                    },
                    time:{
                        required:true,
                    },
                    name:{
                        required:true,
                    },
                    phone:{
                        required:true,
                        number: true,                        
                    },
                    email:{
                        required:true,
                        email: true
                    },
                    /*ref_phone:{
                        required:true,
                        number: true,
                        minlength:10,
                        maxlength:12
                    },
                    note:{
                        required:true,
                    },*/
                },
                submitHandler:function(form){
                    form.submit();
                }
            });

            $('[name="date"]').rules('add', {
              messages: {
                  required: "THÔNG TIN BẮT BUỘC"
              }
            });
            $('[name="time"]').rules('add', {
              messages: {
                  required: "THÔNG TIN BẮT BUỘC"
              }
            });
            $('[name="name"]').rules('add', {
              messages: {
                  required: "THÔNG TIN BẮT BUỘC"
              }
            });
            $('[name="phone"]').rules('add', {
              messages: {
                  required: "THÔNG TIN BẮT BUỘC",
                  number: 'VUI LÒNG NHẬP MỘT SỐ HỢP LỆ',
              }
            });
            $('[name="email"]').rules('add', {
              messages: {
                  required: "THÔNG TIN BẮT BUỘC",
                  email: 'XIN VUI LÒNG NHẬP ĐỊA CHỈ EMAIL HỢP LỆ'
              }
            }); 
        </script>
       <!--  <script type="text/javascript"> 

            $('#multi-select-demo').multiselect({
              includeSelectAllOption: true
            }); 
            $(document).on('change', '.menuchecked', function() { 
                //var menuid = [];
                var venueid = []; 
                $("input:checkbox[name=menucheck]:checked").each(function(){
                    //menuid.push($(this).val());
                    venueid.push($(this).data('venueid'));
                });
                //$('#menu_ids').val(menuid);
                $('#venue_ids').val(venueid);
            });
        </script>
 -->        <!-- Combo Package -->
          
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>   
        <script type="text/javascript">
            $(document).ready(function(){
                $('input.time').timepicker({
                    timeFormat: 'H:mm',
                    interval: 30,                    
                    defaultTime: '19:00',
                    startTime: '00:00',
                });
            });
        </script>    
        <script type="text/javascript">
            var galleryTop = new Swiper('.swiper-container', {
              spaceBetween: 10,
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
            });
            var galleryThumbs = new Swiper('.gallery-thumbs', {
              spaceBetween: 10,
              centeredSlides: true,
              slidesPerView: 'auto',
              touchRatio: 0.2,
              slideToClickedSlide: true,
            });
            galleryTop.controller.control = galleryThumbs;
            galleryThumbs.controller.control = galleryTop;
            
        </script> 
    </body>
</html>