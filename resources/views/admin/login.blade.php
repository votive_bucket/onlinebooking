<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/admin/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/font-awesome.css') }}">
   
    <title>Bud Station Admin - Login</title>
    
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <a href="#">Bud Station</a>
      </div>
      <div class="login-box">
        <form class="login-form" action="{{Route('admin.dologin')}}" method="POST">
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
          <div id="myLoadDiv">
            <img class="loading_image" src="{{ url('resources/assets/images/light_rounded/loader.gif') }}" style="display:none;"/>
          </div>
           @csrf
           @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif
          @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>        
          @endif
          @if(Session::has('error'))
            <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
          @endif
          <div class="form-group">
            <label class="control-label">USERNAME</label>
            <input class="form-control" type="text" name="user_name" id="user_name" placeholder="User Name" autofocus>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="password" id="password" placeholder="Password">
          </div>
          <div class="form-group btn-container">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
          </div>
          <div class="form-group">
            <div class="utility">
              <!-- <div class="animated-checkbox">
                <label class="semibold-text">
                  <input type="checkbox" name="staysignin" id="staysignin" value=""><span class="label-text">Stay Signed in</span>
                </label>
              </div> -->
              <p class="semibold-text mb-0"><a href="{{ url('/forget')}}" data-toggle="flip">Forgot Password ?</a></p>
            </div>
          </div>
        </form>
        <form class="forget-form" action="" id="reset_form">
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
          <div id="myLoadDiv">
            <img class="loading_image" src="{{ url('resources/assets/images/light_rounded/loader.gif') }}" style="display:none;"/>
          </div>
          <span class="emailerror"></span>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="text" name="forgetemail" id="forgetemail"placeholder="Email">
          </div>
          @csrf
          <div class="form-group btn-container">
            <button type="button" class="btn btn-primary btn-block" id="reset"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
          </div>
          <div class="form-group mt-20">
            <p class="semibold-text mb-0"><a data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
          </div>
        </form>
      </div>
    </section>
  </body>

  
  <script src="{{ url('resources/assets/js/admin/jquery-2.1.4.min.js') }}"></script>
  <script src="{{ url('resources/assets/js/admin/bootstrap.min.js') }}"></script>  
  <script src="{{ url('resources/assets/js/admin/main.js') }}"></script>
  <script src="{{ url('resources/assets/js/jquery.validate.min.js') }}"></script>
  <script type="text/javascript">
    var baseurl = "{{url('/')}}";
  </script>
  <script type="text/javascript">
    $("#reset").click(function(){
      var forgetemail = $('#forgetemail').val();
      /*if(forgetemail=="" || forgetemail==null){
        $("span .emailerror").html("Please Enter Email Id");
      }*/
      $.ajax({
        url:"{{url('/admin/forgetlink')}}",
        data: {
                'email':$('#forgetemail').val(), 
                '_token':$('input[name="_token"]').val()
              },
        type:'POST',
        beforeSend: function() {
          $(".loading_image").show();
        },
        success: function(data){
          if(data.errors){
            $("span.emailerror").removeClass("alert alert-success");
            $("span.emailerror").addClass("alert alert-danger");
            $("span.emailerror").html(data.errors.email);
          }else if(data.warning){
            $("span.emailerror").removeClass("alert alert-success");
            $("span.emailerror").addClass("alert alert-danger");
            $("span.emailerror").html(data.warning);
          }else if(data.success){
            $("span.emailerror").removeClass("alert alert-danger");
            $("span.emailerror").addClass("alert alert-success");
            $("span.emailerror").html(data.success);
            setTimeout(function() {
              window.location.replace(baseurl+'/admin/login');
            }, 2000);
          }
        },
        complete: function(){
          $('.loading_image').hide();
        },
        error: function(e){
          console.log(e);
        }
      });
    });
  </script>
</html>