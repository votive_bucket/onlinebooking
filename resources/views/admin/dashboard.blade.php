@extends('admin.layouts.adminlayout')
@section('title', 'Dashboard')


@section('content')
  <div class="content-wrapper">
    <div class="page-title">
      <div>
        <h1><i class="fa fa-dashboard"></i> Dashboard</h1>
      </div>
      <div>
        <ul class="breadcrumb">
          <li><i class="fa fa-home fa-lg"></i></li>
          <li><a href="#">Dashboard</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <h4>WelCome {{ Auth::user()->user_firstname }}</h4>
        </div>
      </div>
    </div>
  </div>  
@endsection