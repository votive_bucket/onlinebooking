@extends('admin.layouts.adminlayout')
@section('title', 'Add Submenu')


@section('content')
<div class="content-wrapper">
        <div class="page-title">
          <div>
            <h1><i class="fa fa-th-list"></i> Submenus</h1>
            <p></p>
          </div>
          <div>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>Submenus</li>
              <li class="active"><a href="#"></a></li>
            </ul>
          </div>          
        </div>
        
        @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="success"></div>
        <div class="error"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="box_addCat">
              <div class="heading"><h4>Submenus List</h4></div> 
              <button type="button" name="addSubmenu" id="addUser" class="addbtn"><a href="{{ url('admin/submenu/add') }}">Add Submenu</a>
              </button>
            </div>
            <div class="card">              
              <div class="card-body">
                <div class="table-responsive no-border">
                  <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                      <tr>
                        <th>S No.</th>
                        <th>Submenu Title</th>
                        <th>Submenu Price (VND)</th>
                        <th>Item</th>
                        <th>Submenu Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; $submenustatus='';
                      ?>
                      @foreach($submenulists as $key =>$value)
                        <?php 
                          if($value->submenu_status == 1){
                            $submenustatus = 'Active';
                          } else{
                            $submenustatus = 'Deactive';
                          }  
                        ?>                      
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$value->sub_menu_name}}</td>
                        <td>{{$value->submenu_price}}</td>
                        <td>
                          <?php 
                           
                          $result = DB::table('items')->selectRaw('GROUP_CONCAT(item_name SEPARATOR ", ") as item_name')->whereIn('item_id',explode(',', $value->items))->first();
                          if(!empty($result)){
                              echo $result->item_name;
                          }
                            ?>
                           
                        </td>
                        <td>
                          {{$submenustatus}}
                        </td>
                        <td>
                          <div class="link-del-view link_one"> 
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Edit"><a href="{{ url('admin/submenu/edit', $value->sub_menu_id) }}"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Delete"><a href="javascript:void(0);" onclick="return deleteStatus({{$value->sub_menu_id}}, 'admin/submenu/delete');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </div>
                            @if($value->submenu_status==1)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Inactive">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$value->sub_menu_id}}, 'admin/submenu/status');">
                                  <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                </a>
                              </div>   
                            @elseif($value->submenu_status==0)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Active">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$value->sub_menu_id}}, 'admin/submenu/status');">
                                  <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                </a>
                              </div>  
                            @endif                            
                          </div>
                        </td>
                        </tr>
                        @endforeach 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection