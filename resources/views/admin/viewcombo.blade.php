@extends('admin.layouts.adminlayout')
@section('title', 'Combo')

@section('content')
<div class="content-wrapper">
  <div class="row">    
    <div class="col-sm-12">
      <div class="ad-user">
        <h4><i class="icon-user mgr-10 profile-icon"></i> COMBO INFORMATION</h4>
      </div> 
      <div class="tab-content">
        <div id="profile-tab" class="tab-pane active">
          <div class="pd-20">              
            <!-- <h4 class="mgbt-xs-15 mgtp-10 font-semibold pad"><i class="icon-user mgr-10 profile-icon"></i> combo INFORMATION</h4> -->
            <div class="row">
                <div class="col-sm-12">
                  <div class="row mgbt-xs-0">
                    <label class="col-xs-5 control-label">Combo Name:</label>
                    <div class="col-xs-7 controls">@if(isset($combo->combo_name)){{ ucfirst(trans($combo->combo_name)) }}@endif</div>
                    <!-- col-sm-10 --> 
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="row mgbt-xs-0">
                    <label class="col-xs-5 control-label">Combo Price:</label>
                    <div class="col-xs-7 controls">@if(isset($combo->combo_price)){{ ucfirst(trans($combo->combo_price)) }}@endif</div>
                    <!-- col-sm-10 --> 
                  </div>
                </div>
                                
                <div class="col-sm-12">
                  <div class="row mgbt-xs-0">
                    <label class="col-xs-5 control-label">Combo Content:</label>
                    <div class="col-xs-7 controls">@if(isset($combo->combo_content)){!! ucfirst(trans($combo->combo_content)) !!}@endif</div>
                    <!-- col-sm-10 --> 
                  </div>
                </div>             
                
                <div class="col-sm-12">
                  <div class="row mgbt-xs-0">
                    <label class="col-xs-5 control-label">Combo Image:</label>
                    <div class="col-xs-7 controls">
                      <img alt="example image" src="{{ url('/public/uploads/combo')}}/{{$combo->combo_image }}" class="img-test">
                     </div>
                    <!-- col-sm-10 --> 
                  </div>
                </div>
              
            </div> 
            <!-- row --> 
            <button class="goback"><a href="{{ url('/admin/allcombo') }}">Go Back</a></button>
          </div>
            <!-- pd-20 --> 
        </div>
      </div>
    </div>
  </div>
  <!-- end of row  -->
</div>
@endsection