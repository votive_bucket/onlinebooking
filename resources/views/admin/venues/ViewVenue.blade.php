@extends('admin.layouts.adminlayout')
@section('title', 'View Menu')

@section('content')
<div class="content-wrapper">
  <div class="row">    
    <div class="col-sm-12">
      <div class="ad-user">
        <h4><i class="icon-user mgr-10 profile-icon"></i> Venue Information</h4>
      </div>  
      <div class="tab-content">
        <div id="profile-tab" class="tab-pane active">
          <div class="pd-20">            
            <div class="row">
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue Title:</label>
                  <div class="col-xs-7 controls">{{$Venues->venue_name}}</div>
                </div>
              </div>
     
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">City:</label>
                  <div class="col-xs-7 controls">{{$Venues->city}}</div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">District:</label>
                  <div class="col-xs-7 controls">{{$Venues->district}}</div>
                </div>
              </div>  
              <!-- <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue ward:</label>
                  <div class="col-xs-7 controls">{{$Venues->venue_ward}}</div>
                </div>
              </div>  -->
              <!-- <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue streat:</label>
                  <div class="col-xs-7 controls">{{$Venues->venue_street}}</div>
                </div>
              </div>  -->
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue address:</label>
                  <div class="col-xs-7 controls">{{$Venues->venue_address}}</div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue hall capacity:</label>
                  <div class="col-xs-7 controls">{{$Venues->venue_hall_capacity}}</div>
                </div>
              </div> 

              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue has Máy Chiếu:</label>
                  <div class="col-xs-7 controls">@if($Venues->venue_has_projector==1) Yes @else No @endif</div>
                </div>
              </div> 

              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue has Djà “DJ”:</label>
                  <div class="col-xs-7 controls">@if($Venues->venue_has_dj==1) Yes @else No @endif</div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue has VIP Room à “Phòng VIP”:</label>
                  <div class="col-xs-7 controls">@if($Venues->venue_has_viproom==1) Yes @else No @endif</div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue has Stage à “Sân Khấu”:</label>
                  <div class="col-xs-7 controls">@if($Venues->venue_has_stage==1) Yes @else No @endif</div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue has Live Music à “Live”:</label>
                  <div class="col-xs-7 controls">@if($Venues->venue_has_livemusic==1) Yes @else No @endif</div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue has:</label>
                  <div class="col-xs-7 controls">@if(!empty($Venues->venue_has)) {{$Venues->venue_has}}  @endif</div>
                </div>
              </div>
            <!-- <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue has Bar Games:</label>
                  <div class="col-xs-7 controls">@if($Venues->venue_status==1) Active @else Inactive @endif</div>
                </div>
              </div> -->
            <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Logo:</label>
                  <div class="col-xs-7 controls">
                    <div class="text-center vd_info-parent" style="float:left;"> 
                       <img alt="example image" src="{{ url('/public/uploads/venue')}}/{{$Venues->venue_logo }}" class="img-test">
                    </div>
                  </div>
                </div>
              </div> 
            <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue images:</label>
                  <div class="col-xs-7 controls"  style="float:left;">
                     <div class="img-prw-box-gallery" > 
                          @if($images)
                            @foreach($images as $img)
                              <div class="imgmul col-md-2">
                                <img src="{{ url('/public/uploads/venue/images')}}/{{$img->image_name }}">
                                
                              </div>
                              @endforeach
                           
                          @endif
                        </div>                      
                  </div>
                </div>
              </div>                                                                                                               
            </div> 
            <button class="goback"><a href="{{ url('admin/menu/list') }}">Go Back</a></button>
            <!-- row --> 
          </div>
          
            <!-- pd-20 --> 
        </div>
      </div>
    </div>
  </div>
  <!-- end of row  -->
</div>
@endsection