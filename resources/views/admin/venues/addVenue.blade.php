@extends('admin.layouts.adminlayout')

@section('title', 'Add Venue')
@section('content')
<?php //echo "<pre>"; print_r($menu->category_id); die;?>
<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>@if(!empty($Venues)) EDIT @else ADD @endif Venue</h4>
        </div>
     </div>
  </div>

<div class="card">
  <div class="row">
    <form action="{{url('admin/venue/venuestore')}}" method="POST" name="addvenueform" id="addvenueform" enctype="multipart/form-data">
      <div class="col-sm-12">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <div class="row">
                <div class="col-sm-12">
                  @csrf
                  @php 
                  $edit = false;
                  @endphp
                  @if(!empty($Venues))
                  <input type="hidden" class="form-control" id="edit" name="edit" value="edit">
                  <input type="hidden" class="form-control" id="venue_id" name="venue_id" value="{{$Venues->venue_id}}">
                  @php 
                  $edit = true;
                  @endphp
                  @endif

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Venue Name:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="venue_name" name="venue_name" value="@if($edit){{$Venues->venue_name}} @endif">
                        </div>
                      </div>
                      </div>
                    </div>
                     
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">City:</label>
                        <div class="col-md-10">
                          <select name="city" class="form-control city">
                              <option value="" >select city</option>
                              @foreach($city as $ci)
                                <option value="{{$ci->city_id}}" @if($edit && $Venues['venue_city']==$ci->city_id) selected @endif >{{$ci->city_name}}</option>
                              @endforeach
                          </select>
                        </div>
                      </div>
                      </div>
                    </div>  
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">District:</label>
                        <div class="col-md-10">
                          <select name="district" class="form-control district">
                              <option value="">select district</option>
                              @if($edit)
                                @foreach($district as $di)
                                <option value="{{$di->id}}" @if($edit && $Venues['venue_district']==$di->id) selected @endif >{{$di->district_name}}</option>
                              @endforeach
                             @endif  
                          </select>
                        </div>
                      </div>
                      </div>
                    </div>                                  
                     <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Venue address:</label>
                        <div class="col-sm-10">
                           <textarea class="form-control" name="venue_address">@if($edit){{$Venues->venue_address}} @endif</textarea>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Venue hall capacity:</label>
                        <div class="col-md-10">
                          <input type="text" class="form-control" id="venue_hall_capacity" name="venue_hall_capacity" value="@if($edit){{$Venues->venue_hall_capacity}} @endif">
                        </div>
                      </div>
                      </div>
                    </div> 
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name"></label>
                        <div class="col-md-10">
                        <div class="bx_chk">                        
                          <input type="checkbox" id="venue_has_projector" name="venue_has_projector" class="form-control" @if($edit && $Venues->venue_has_projector==1) checked @endif>
                          <label class="" for="menu_name">Venue has Máy Chiếu</label>
                        </div>
                        </div>
                      </div>
                      </div>
                    </div> 
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name"></label>
                        <div class="col-md-10">
                       <div class="bx_chk">   
                          <input type="checkbox" id="venue_has_dj" name="venue_has_dj" class="form-control" @if($edit && $Venues->venue_has_dj==1) checked @endif>
                           <label class="" for="menu_name">Venue has Djà “DJ”</label>
                        </div>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name"></label>
                        <div class="col-md-10">
                       <div class="bx_chk">   
                          <input type="checkbox" id="venue_has_viproom" name="venue_has_viproom" class="form-control" @if($edit && $Venues->venue_has_viproom==1) checked @endif>
                           <label class="" for="menu_name">Venue has VIP Room à “Phòng VIP”</label>
                        </div>
                        </div>
                      </div>
                      </div>
                    </div>

                  <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name"></label>
                        <div class="col-md-10">
                       <div class="bx_chk">   
                          <input type="checkbox" id="venue_has_stage" name="venue_has_stage" class="form-control" @if($edit && $Venues->venue_has_stage==1) checked @endif>
                           <label class="" for="menu_name">Venue has Stage à “Sân Khấu”</label>
                        </div>
                        </div>
                      </div>
                      </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="venue_has_livemusic"></label>
                        <div class="col-md-10">
                       <div class="bx_chk">   
                          <input type="checkbox" id="venue_has_livemusic" name="venue_has_livemusic" class="form-control" @if($edit && $Venues->venue_has_livemusic==1) checked @endif>
                           <label class="" for="venue_has_livemusic">Venue has Live Music à “Live”</label>
                        </div>
                        </div>
                      </div>
                      </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="venue_has">Venue has:</label>
                        <div class="col-md-10">
                        <!-- <select name="venue_has" class="form-control">
                              <option value="" >select</option>
                              <option value="2" @if($edit && $Venues->venue_has==1) selected @endif>No</option> 
                              <option value="1" @if($edit && $Venues->venue_has==1) selected @endif>Yes</option> 
                          </select> -->
                          <input type="text" id="venue_has" name="venue_has" class="form-control" value="@if($edit) {{$Venues->venue_has }} @endif">                           
                        </div>
                      </div>
                      </div>
                  </div>                  
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="combo_image">Venue Logo:</label>
                          <div class="col-md-10">
                            <div class="col-md-6 form-control">
                              <input type="file" accept="image/png, image/jpeg" name="venuelogo" id="combo_image" >
                              <input type="hidden" id="upload_img" name="upload_img" value="{{ ($edit) ? $Venues->venue_logo :'' }}" class="img-test" >
                            </div>
                            <div class="col-md-6">
                              <div class="row">
                                <div id="img_preview">
                                  @if($edit &&  $Venues->venue_logo !='')
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('/public/uploads/venue')}}/{{$Venues->venue_logo }}" class="img-test">
                                    </div>
                                  @else
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('resources/assets/images') }}/logo_black.png" class="img-test" >
                                    </div>
                                  @endif 
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>   

                    </div> 

                 <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="combo_image">Venue Images:</label>
                          <div class="col-md-10">
                            <div class="col-md-6 form-control">
                              <input type="file" accept="image/*" name="venueimages[]" id="venueimages" multiple>
                            </div> 



                            <div class="img-prw-box-gallery"> 
                               <div class="row">
                        @if($edit)
                          @if($images)
                            @foreach($images as $img)
                              <div class="imgmul col-md-3">
                                <img src="{{ url('/public/uploads/venue/images')}}/{{$img->image_name }}">
                                <a href="javascript:;" class="deleteimg" data-id="{{$img->image_id}}" ><i class="fa fa-trash"></i></a>
                              </div>
                              @endforeach
                           
                          @endif
                        @endif
                      </div>
                      </div>   
                      <div class="img-prw-box-gallery fortemp"> 
                      </div>  


                          </div>
                        </div>
                      </div> 
                               
                   </div>

                  </div>
                </div>
              </div>
              <div class="row" id="additional_info">
                <div class="prof_info">
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-md-offset-4 col-sm-6">  
                        <button class="btn vd_btn vd_bg-green" type="submit" id="menusubmit"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                      </div>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
@endsection

