@extends('admin.layouts.adminlayout')

@section('title', 'Change Password')

@section('content')
<?php //echo "<pre>"; print_r($user[0]->user_street); die;?>
<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>Change Password</h4>
        </div>
     </div>
  </div>
  <div class="row">
    @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('error'))
          <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
        @endif
    <form  action="{{ url('admin/changepass') }}" method="post" name="changePasswordForm" id="changePasswordForm">
      <div class="col-sm-12">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              
              <div class="row">
                <div class="col-sm-12">                  
                  @csrf             
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label for="old_password">Old Password:</label>
                        <input class="form-control" type="password" name="old_password" id="old_password" value="">
                     </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="new_password">New Password:</label>
                      <input class="form-control" type="password" name="new_password" id="new_password" value="">
                    </div>
                  </div>  
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="confirm_password">Confirm Password</label>
                      <input class="form-control" type="password" name="confirm_password" id="confirm_password" value="">
                    </div>
                    
                  </div>                        
                </div>
              </div>
              <div class="row" id="additional_info">
                <div class="prof_info">
                  <div class="col-sm-12">
                    <button class="btn vd_btn vd_bg-green"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                  </div> 
                </div>
              </div> 
            </div>
          </div>                    
        </div>

      </div>
      
    </form>
  </div>
    
</div>
  
@endsection