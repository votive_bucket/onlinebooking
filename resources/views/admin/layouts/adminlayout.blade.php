<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{url('/')}}/favicon.ico" type="image/x-icon">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/admin/main.css') }}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ url('resources/assets/css/bootstrap-multiselect.css') }}">
    <title>Bud Station Admin - @yield('title')</title>
  </head>
  <body class="sidebar-mini fixed">
    <div class="wrapper">
      <!-- Navbar-->
      <header class="main-header hidden-print"><a class="logo" href="{{url('/')}}">Bud Station</a>
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
          <!-- Navbar Right Menu-->
          <div class="navbar-custom-menu">
            <ul class="top-nav">
              <!-- User Menu-->
              <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                <ul class="dropdown-menu settings-menu"><!-- 
                  <li><a href="#"><i class="fa fa-cog fa-lg"></i> Settings</a></li> -->
                  <li><a href="{{url('admin/viewprofile')}}"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                  <li><a href="{{url('admin/resetpassword')}}"><i class="fa fa-user fa-lg"></i> Change Password</a></li>
                  <li><a href="{{url('signout')}}"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Side-Nav-->
      <aside class="main-sidebar hidden-print">
        <section class="sidebar">
          
          <!-- Sidebar Menu-->
          <ul class="sidebar-menu">

            <li class="treeview {{(Request::is('admin/viewprofile')) || (Request::is('admin/editprofile')) || (Request::is('admin/dashboard'))?'active':''}}"><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i><span>My Dashboard</span></a></li>

            <!-- <li class="treeview {{(Request::is('admin/menu/list')) || (Request::is('admin/submenu/list'))?'active':''}}">
              <a href="#">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <span>Menu</span>
                <i class="fa fa-angle-right"></i>
              </a>
              <ul class="treeview-menu menu-open" style="{{(Request::is('admin/menu/list')) || (Request::is('admin/submenu/list'))?'display: block':'display: none'}}">                
                <li class="{{Request::is('admin/menu/list')?'active':''}}"><a href="{{ url('admin/menu/list') }}"><i class="fa fa-circle-o"></i> Menu List</a></li>
                <li class="{{Request::is('admin/submenu/list')?'active':''}}"><a href="{{ url('admin/submenu/list') }}"><i class="fa fa-circle-o"></i> Submenu List</a></li>
              </ul>
            </li>  -->         
            
            <li class="treeview {{(Request::is('admin/allcombo')) || (Request::is('admin/viewcombo/*'))|| (Request::is('admin/editcombo/*'))|| (Request::is('admin/addcombo'))?'active':''}}"><a href="#"><i class="fa fa-cubes" aria-hidden="true"></i><span>Combo</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu menu-open" style="{{(Request::is('admin/allcombo'))|| (Request::is('admin/viewcombo/*'))|| (Request::is('admin/editcombo/*'))|| (Request::is('admin/addcombo'))?'display: block':'display: none'}}">
                <li class="{{Request::is('admin/allcombo')|| (Request::is('admin/viewcombo/*'))|| (Request::is('admin/editcombo/*'))|| (Request::is('admin/addcombo'))?'active':''}}"><a href="{{ url('admin/allcombo') }}"><i class="fa fa-circle-o"></i> All Combo</a></li>                
              </ul>
            </li>
            <li class="treeview {{(Request::is('admin/alloffer')) || (Request::is('admin/viewoffer/*'))|| (Request::is('admin/editoffer/*'))|| (Request::is('admin/addoffer'))?'active':''}}"><a href="#"><i class="fa fa-suitcase" aria-hidden="true"></i><span>Special Gifts</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu menu-open" style="{{(Request::is('admin/alloffer'))|| (Request::is('admin/viewoffer/*'))|| (Request::is('admin/editoffer/*'))|| (Request::is('admin/addoffer'))?'display: block':'display: none'}}">
                <li class="{{Request::is('admin/alloffer')|| (Request::is('admin/viewoffer/*'))|| (Request::is('admin/editoffer/*'))|| (Request::is('admin/addoffer'))?'active':''}}"><a href="{{ url('admin/alloffer') }}"><i class="fa fa-circle-o"></i> All Special Gifts</a></li>                
              </ul>
            </li>
            <li class="treeview {{(Request::is('admin/venue/*')) ?'active':''}}"><a href="#"><i class="fa fa-suitcase" aria-hidden="true"></i><span>Venues</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu menu-open" style="{{((Request::is('admin/venue/*')) || (Request::is('admin/menu/*')))?'display: block':'display: none'}}">
                <li class="{{Request::is('admin/venue/list')   ?'active':''}}"><a href="{{ url('admin/venue/list') }}"><i class="fa fa-circle-o"></i> All Venues</a></li>
                <li class="{{Request::is('admin/menu/list')?'active':''}}"><a href="{{ url('admin/menu/list') }}"><i class="fa fa-circle-o"></i> All Menus</a></li>
                <!-- <li class="{{Request::is('admin/submenu/list')?'active':''}}"><a href="{{ url('admin/submenu/list') }}"><i class="fa fa-circle-o"></i> All Submenus</a></li>

                <li class="{{Request::is('admin/item/list')?'active':''}}"><a href="{{ url('admin/item/list') }}"><i class="fa fa-circle-o"></i> All Items</a></li> -->


              </ul>
            </li>

            <li class="treeview {{(Request::is('admin/order/*')) ?'active':''}}"><a href="#"><i class="fa fa-suitcase" aria-hidden="true"></i><span>Order</span><i class="fa fa-angle-right"></i></a>
              <ul class="treeview-menu menu-open" style="{{(Request::is('admin/order/*'))?'display: block':'display: none'}}">
                <li class="{{Request::is('admin/order/list')   ?'active':''}}"><a href="{{ url('admin/order/list') }}"><i class="fa fa-circle-o"></i> All Orders</a></li>                
              </ul>
            </li>             
           <!--  <li class="treeview {{(Request::is('admin/venue/list')) || (Request::is('admin/facilities/list'))?'active':''}}">
              <a href="#">
                <i class="fa fa-bars" aria-hidden="true"></i>
                <span>Venue</span>
                <i class="fa fa-angle-right"></i>
              </a>
              <ul class="treeview-menu menu-open" style="{{(Request::is('admin/venue/list')) || (Request::is('admin/facilities/list'))?'display: block':'display: none'}}">                
                <li class="{{Request::is('admin/venue/list')?'active':''}}"><a href="{{ url('admin/venue/list') }}"><i class="fa fa-circle-o"></i> Venue List</a></li>
                
              </ul>
            </li> -->

          </ul>
        </section>
      </aside>
      @yield('content')      
    </div>
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <h3>Are you sure you want to delete this.</h3>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="ok" data-dismiss="modal">Ok</button>
            <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Javascripts-->
    <script src="{{ url('resources/assets/js/admin/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ url('resources/assets/js/admin/bootstrap.min.js') }}"></script>
    <script src="{{ url('resources/assets/js/admin/main.js') }}"></script>
    <script src="{{ url('resources/assets/js/admin/plugin/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('resources/assets/js/admin/plugin/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ url('resources/assets/js/bootstrap-multiselect.min.js') }}"></script>
    <script src="{{ url('resources/assets/js/jquery.validate.min.js') }}"></script>
    
    <script type="text/javascript">$('#sampleTable').DataTable();</script>


    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCEDVd3ns05bhTmlTSlS_zopAJxkbkp5hw&sensor=false&libraries=places"></script> 
    <script type="text/javascript">
      var baseurl = "{{url('/')}}";
    </script>

    <script type="text/javascript">
    $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });

    $(document).ready(function() {
        $('#multi-select-demo').multiselect();
    });
 
     $("#additme").validate({
        rules:{
          item_name: {
            required:true
          },
           
        },
        submitHandler: function(form){
          form.submit();
        }
      });
      $("#addmenuform").validate({
        ignore:[],
        rules:{
          menu_name: {
            required:true
          },
          venue: {
            required:true
          },
          combo: {
            required:true
          },
          Price: {
            required:true
          },
          submenus: {
            required: function() 
                        {
                         CKEDITOR.instances.submenus.updateElement();
                        },
            minlength:10 
          },
        },
        submitHandler: function(form){
          form.submit();
        }
      });

      $("#addsubmenuform").validate({
        rules:{
          sub_menu_name: {
            required:true
          },
          "items[]": {
            required:true
          },
        },
        submitHandler: function(form){
          form.submit();
        }
      });

      $("#comboForm").validate({
        ignore:[],
        rules:{
          combo_name: {
            required:true
          },
          combo_price: {
            required:true,
            number: true
          },
          combo_content: {
            required: function() 
                        {
                         CKEDITOR.instances.combo_content.updateElement();
                        },
                        minlength:10 
          },
          upload_img: {
            required:true
          },
        },
        submitHandler: function(form){
          form.submit();
        }
      });

      $("#offerForm").validate({
        ignore:[],
        rules:{
          offer_name: {
            required:true
          },
          offer_price: {
            required:true,
            number: true
          },
          upload_img: {
            required:true
          }
        },
        submitHandler: function(form){
          form.submit();
        }
      });

      $('#changePasswordForm').validate({ // initialize the plugin
          rules: {
              old_password: {
                  required: true
              },
              new_password: {
                  required: true,
                  minlength : 6,
              },
              confirm_password:{
                required: true,
                minlength : 6,
                equalTo : "#new_password"
              }
          },
          submitHandler: function(form) {
            form.submit();
          }
        });
     $("#addvenueform").validate({
            rules:{
              venue_name: {
                required:true
              },
             "menu_ids[]": {
                required:true
              },
             city:{
                required:true
              },
              district: {
                required:true
              },
             // venuelogo: {
             //    required:true
             //  },              
            },
            submitHandler: function(form){
              form.submit();
            }
     });
  $(".city").on('change',function(){
         var city  = $(this).val();  
        $.ajax({
          url: "{{ url('/') }}"+'/admin/getdistict',
          type: "POST",
          data:{'city':city},
          success: function(data){ 
            $(".district").html(data);
          }       
            
        });
      })
      $(".deleteimg").on("click",function(){
        var delid = $(this).attr('data-id');
         var btn = $(this);
        $.ajax({
          url: "{{ url('/') }}"+'/admin/venue/deletevenueimage',
          type: "POST",
          data:{'delid':delid},
          success: function(data){ 
            if(data==1){
              btn.parent("div").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow", function() { $(this).remove(); });
            }
          }       
            
        });
      })

    function ProductPreviewImages() { 
      var $preview = $('.fortemp').empty();
      if (this.files) $.each(this.files, readAndPreview); 
      function readAndPreview(i, file) { 
      if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
       return alert(file.name +" is not an image");
      } // else... 
      var reader = new FileReader(); 
      $(reader).on("load", function() {
       $preview.append($("<img/>", {src:this.result, height:100,  width:100}));
      }); 
      reader.readAsDataURL(file); 
      } 
    } 
    $('#venueimages').on("change", ProductPreviewImages);    
        function readURLTest(input) {

          if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
             var imagehtml = '<div class="text-center vd_info-parent"><img alt="example image" src="'+e.target.result+'" class="img-test"></div>';
              $('#img_preview').html('');
              $('#img_preview').append(imagehtml);
              $('#upload_img').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
          }
        }

        $("#combo_image").change(function() {
          readURLTest(this);
        });

        function readURL(input) {

          if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
             var imagehtml = '<div class="text-center vd_info-parent"><img alt="example image" src="'+e.target.result+'" class="img-test"></div>';
              $('#img_selected_preview').html('');
              $('#img_selected_preview').append(imagehtml);
              $('#upload_selected_img').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
          }
        }

        $("#combo_selected_image").change(function() {
          readURL(this);
        });

        $('#addUserForm').validate({ // initialize the plugin
          ignore:[],
          rules: {
              p_img: {
                  required: true
              },
              first_name: {
                  required: true
              },
              last_name: {
                  required: true
              },
              email: {
                  required: true,
                  email: true,
              },
              user_contact: {
                  required: true,
                  number: true,
              },
              user_name: {
                  required: true
              },
          },
          submitHandler: function(form) {
            form.submit();
          }
        });

        function readURLuser(input) {

          if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
             var imagehtml = '<div class="text-center vd_info-parent"><img alt="example image" src="'+e.target.result+'" class="img-responsive"><input type="hidden" id="p_img" name="p_img" value="'+e.target.result+'" class="img-responsive"></div>';
              $('#user_profiles').html('');
              $('#user_profiles').append(imagehtml);
              $('#upload_portfolio').val(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
          }
        }

        $("#profileImg").change(function() {
          readURLuser(this);
        }); 
    </script>

    <script type="text/javascript">
      $(".alert").delay(4000).slideUp(200, function() {
              });

      //To update status single row of lists
      function updateStatus(id, attr){
        $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
        });
        $.ajax({
          url: "{{ url('/') }}"+'/admin/'+attr,
          type: "POST",
          data:{'id':id},
          success: function(data){        
            var dec = JSON.parse(data);
            jQuery("html, body").animate({ scrollTop: 0 }, "slow");
            $("div.success").addClass('alert alert-success alert-dismissible');
            $("div.success").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);
            setTimeout(function(){
              location.reload(); 
            }, 2000);
          },error: function(){
            $("div.success").addClass('alert alert-danger alert-dismissible');
            $("div.success").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);
          }
        });
      }

      //To delete single row of lists
      function deleteStatus(id, attr){
        $('#myModal').modal('show');
        $('#ok').click(function(){
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            url: "{{ url('/') }}"+'/'+attr, 
            type: "POST",
            data:{'id':id},
            success: function(data){       
              var dec = JSON.parse(data);
              jQuery("html, body").animate({ scrollTop: 0 }, "slow");
              $("div.success").addClass('alert alert-success alert-dismissible');
              $("div.success").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);
              setTimeout(function(){
                location.reload(); 
              }, 2000);              
            },error: function(){
              $("div.success").addClass('alert alert-danger alert-dismissible');
              $("div.success").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+dec.msg);
            }
          });
        });    
      }
     
    </script>  
    <script type="text/javascript">
      $('.number').keypress(function (event) {
      var keycode = event.which;
      if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
          event.preventDefault();
      }
    });
    </script>     


    </body>
</html>