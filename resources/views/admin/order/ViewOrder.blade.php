@extends('admin.layouts.adminlayout')
@section('title', 'View Order')

@section('content')
<div class="content-wrapper">
  <div class="row">    
    <div class="col-sm-12">
      <div class="ad-user">
        <h4><i class="icon-user mgr-10 profile-icon"></i> Order Information</h4>
      </div>  
      <?php //echo "<pre>"; print_r($orders);die;?>
      <div class="tab-content">
        <div id="profile-tab" class="tab-pane active">
          <div class="pd-20">            
            <div class="row">
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Order No.:</label>
                  <div class="col-xs-7 controls">#{{$orders->order_no}}</div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Orders Date:</label>
                  <div class="col-xs-7 controls">{{date('d/m/Y',strtotime($orders->created_at))}}</div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Number of participants:</label>
                  <div class="col-xs-7 controls">{{$orders->no_of_guest}}</div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Combo:</label>
                  <div class="col-xs-7 controls">{{$orders->combo_name}}</div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Priority in order:</label>
                  <div class="col-xs-7 controls">
                   <b>Venue1:</b> {{$orders->venue1_name}}<br>
                    <b>Venue2:</b> {{$orders->venue2_name}}<br>
                    <b>Venue3:</b> {{$orders->venue3_name}}
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Total:</label>
                  <div class="col-xs-7 controls">
                    <b>Total1:</b>  {{str_replace(",",".",number_format($orders->total1))}} VNĐ<br>
                    <b>Total2:</b>  {{str_replace(",",".",number_format($orders->total2))}} VNĐ<br>
                    <b>Total3:</b>  {{str_replace(",",".",number_format($orders->total3))}} VNĐ
                  </div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Menu1:</label>
                  <div class="col-xs-7 controls">
                    {!!$orders->menu1!!}<br>
                    {!!$orders->submenu1!!} 
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Menu2:</label>
                  <div class="col-xs-7 controls">
                    {!!$orders->menu2!!} <br>
                    {!!$orders->submenu2!!}
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Menu3:</label>
                  <div class="col-xs-7 controls">
                    {!!$orders->menu3!!} <br>
                    {!!$orders->submenu3!!}
                  </div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Date:</label>
                  <div class="col-xs-7 controls">
                    {{date('d/m/Y',strtotime($orders->party_date))}}
                  </div>
                </div>
              </div>  
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Time:</label>
                  <div class="col-xs-7 controls">
                    {!!$orders->party_time!!} 
                  </div>
                </div>
              </div>  
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Name:</label>
                  <div class="col-xs-7 controls">
                    {{$orders->name}}
                  </div>
                </div>
              </div>  
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Email:</label>
                  <div class="col-xs-7 controls">
                    {{$orders->email}}
                  </div>
                </div>
              </div>  
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Phone No.:</label>
                  <div class="col-xs-7 controls">
                    {{$orders->phone_no}}
                  </div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Refferral No.:</label>
                  <div class="col-xs-7 controls">
                    {{$orders->refferred_no}}
                  </div>
                </div>
              </div> 
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Note:</label>
                  <div class="col-xs-7 controls">
                    {{$orders->note}}
                  </div>
                </div>
              </div> 


                                                                                                                          
            </div> 
            <button class="goback"><a href="{{ url('admin/order/list') }}">Go Back</a></button>
            <!-- row --> 
          </div>
          
            <!-- pd-20 --> 
        </div>
      </div>
    </div>
  </div>
  <!-- end of row  -->
</div>
@endsection