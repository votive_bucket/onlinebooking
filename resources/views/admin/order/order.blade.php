@extends('admin.layouts.adminlayout')
@section('title', 'Order')
@section('content')
<div class="content-wrapper">
        <div class="page-title">
          <div>
            <h1><i class="fa fa-th-list"></i> Order</h1>
            <p></p>
          </div>
          <div>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>Order</li>
              <li class="active"><a href="#"></a></li>
            </ul>
          </div>          
        </div>
        
        @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="success"></div>
        <div class="error"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="box_addCat">
              <div class="heading"><h4>Order List</h4></div> 
               
            </div>
            <div class="card">              
              <div class="card-body">
                <div class="table-responsive no-border">
                  <table class="table table-hover table-bordered" id="">
                    <thead>
                      <tr>
                        <th>S No.</th>
                        <th>Order no</th>
                        <th>Total No of People</th>
                        <th>Venues</th>
                        <th>Total</th>
                        <th>Date</th>
                        <th>User Name</th>
                        <th>Contact No.</th>
                        <th>Email</th>
                        <th>Reffered Contact No.</th>
                        <th>Combo</th> 
                        <th>Note</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; $menustatus='';
                      ?>
                      @foreach($orders as $key =>$value)
                        <?php 
                          if($value['order_status'] == 1){
                            $menustatus = 'Active';
                          } else{
                            $menustatus = 'Deactive';
                          }  
                        ?>                      
                      <tr>
                        <td>{{$i++}}</td>
                        <td>#{{$value['order_no']}}</td>
                        <td>{{$value['no_of_guest']}}</td>
                        <td>{{$value['venue1_name']}},</br>
                          {{$value['venue2_name']}},</br>
                          {{$value['venue3_name']}}</td>
                        <td>{{$value['total1']}}</br>
                        {{$value['total2']}}</br>
                      {{$value['total3']}}</td>
                        <td>{{$value['party_date']}}</td>
                        <td>{{ $value['name'] }}</td>
                        <td>{{ $value['phone_no'] }}</td>
                        <td>{{ $value['email'] }}</td>
                        <td>{{ $value['refferred_no'] }}</td>
                        <td>{{ $value['combo_name'] }}</td>
                        <td>{{ $value['note'] }}</td>
                        <!-- <td>
                           <?php echo date('d M Y',strtotime($value['created_at'])); ?>
                        </td> -->
                        <td>
                          <div class="link-del-view link_one"> 
                          
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="View"><a href="{{ url('admin/order/view', $value['order_id']) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Delete">
                              <a href="javascript:void(0);" onclick="return deleteStatus({{$value['order_id']}}, 'admin/deleteorder');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </div>
                                                     
                          </div>
                        </td> 
                        </tr>
                        @endforeach 
                    </tbody>
                  </table>
                  {!! $orders->render() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection