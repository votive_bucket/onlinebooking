@extends('admin.layouts.adminlayout')
@section('title', 'Add Submenu')
@section('content')
<?php //echo "<pre>"; print_r($submenu->category_id); die;?>
<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>@if(!empty($submenu)) EDIT @else ADD @endif Submenu</h4>
        </div>
     </div>
  </div>

<div class="card">
  <div class="row">
    <form action="{{url('admin/addsubmenu')}}" method="POST" name="addsubmenuform" id="addsubmenuform">
      <div class="col-sm-12">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <div class="row">
                <div class="col-sm-12">
                  @csrf
                  <?php 
                  $edit = false;
                  ?>
                  @if(!empty($submenu))
                  <input type="hidden" class="form-control" id="edit" name="edit" value="edit">
                  <input type="hidden" class="form-control" id="submenuid" name="submenuid" value="{{$submenu->sub_menu_id}}">
                  <?php  $edit = true;?>
                  @endif

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-sm-2" for="sub_menu_name">Submenu Name:</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="sub_menu_name" name="sub_menu_name" value="{{ isset($submenu->sub_menu_name) ? $submenu->sub_menu_name : '' }}">
                          </div>
                        </div>
                      </div>
                    </div> 


                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-sm-2" for="sub_menu_name">Items:</label>
                          <div class="col-sm-10">  
                             
                             <select id="multi-select-demo" multiple="multiple" class="form-control" name="items[]">
                                
                               @if(!$items->isEmpty())
                               <?php 
                                   if($edit){
                                    $arr = explode(',',$submenu->items);
                                     
                                  }
                                  ?>
                                @foreach($items as $ite)

                                  <option value="{{$ite->item_id}}" @if($edit && in_array($ite->item_id,$arr)) selected @endif>{{$ite->item_name}}</option>
                                @endforeach
                               @endif
                             </select>                              
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-sm-2" for="submenu_price">Submenu Price (VND):</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="submenu_price" name="submenu_price" value="{{ isset($submenu->submenu_price) ? $submenu->submenu_price : '' }}">
                          </div>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="row" id="additional_info">
                <div class="prof_info">
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-md-offset-4 col-sm-6">  
                        <button class="btn vd_btn vd_bg-green" type="submit" id="submenusubmit"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                      </div>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
@endsection
 
