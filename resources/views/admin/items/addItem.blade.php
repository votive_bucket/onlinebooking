@extends('admin.layouts.adminlayout')

@section('title', 'Add Item')
@section('content')
<?php //echo "<pre>"; print_r($menu->category_id); die;?>
<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>@if(!empty($item)) EDIT @else ADD @endif Item</h4>
        </div>
     </div>
  </div>

<div class="card">
  <div class="row">
    <form action="{{url('admin/item/item-store')}}" method="POST" name="additme" id="additme">
      <div class="col-sm-12">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <div class="row">
                <div class="col-sm-12">
                  @csrf
                  <?php $edit = false; ?>
                  @if(!empty($item))
                  <?php $edit = true; ?>
                  <input type="hidden" class="form-control" id="edit" name="edit" value="edit">
                  <input type="hidden" class="form-control" id="itemid" name="itemid" value="{{$item->item_id}}">
                  @endif

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Item Name:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="item_name" name="item_name" value="{{ isset($item->item_name) ? $item->item_name : '' }}">
                        </div>
                      </div>
                      </div>
                    </div>
    
        
     
                  </div>
                </div>
              </div>
              <div class="row" id="additional_info">
                <div class="prof_info">
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-md-offset-4 col-sm-6">  
                        <button class="btn vd_btn vd_bg-green" type="submit" id="menusubmit"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                      </div>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
@endsection