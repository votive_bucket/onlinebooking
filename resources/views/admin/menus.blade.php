@extends('admin.layouts.adminlayout')
@section('title', 'Add Menu')


@section('content')
<div class="content-wrapper">
        <div class="page-title">
          <div>
            <h1><i class="fa fa-th-list"></i> Menus</h1>
            <p></p>
          </div>
          <div>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>Menus</li>
              <li class="active"><a href="#"></a></li>
            </ul>
          </div>          
        </div>
        
        @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="success"></div>
        <div class="error"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="box_addCat">
              <div class="heading"><h4>Menus List</h4></div> 
              <button type="button" name="addMenu" id="addUser" class="addbtn"><a href="{{ url('admin/menu/add') }}">Add Menu</a>
              </button>
            </div>
            <div class="card">              
              <div class="card-body">
                <div class="table-responsive no-border">
                  <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                      <tr>
                        <th>S No.</th>
                        <th>Menu Title</th>
                        <th>Combo Title</th>
                        <th>Venue Title</th>
                        <th>Menu Price</th>
                        <th>Menu Price</th>                        
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1; $menustatus='';
                      ?>
                      @foreach($menus as $key =>$value)
                        <?php 
                          if($value['menu_status'] == 1){
                            $menustatus = 'Active';
                          } else{
                            $menustatus = 'Deactive';
                          }  
                        ?>                      
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$value['menu_name']}}</td>
                        <td>{{$value['combo']}}</td>
                        <td>{{$value['venue']}}</td>   
                        <td>{{$value['venue']}}</td>                     
                        <td>{{str_replace(",",".",number_format($value['menu_price']))}} </td>
                        <td>
                          <div class="link-del-view link_one"> 
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Edit"><a href="{{ url('admin/menu/edit', $value['menu_id']) }}"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="View"><a href="{{ url('admin/menu/view', $value['menu_id']) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Delete"><a href="javascript:void(0);" onclick="return deleteStatus({{$value['menu_id']}}, 'admin/menu/delete');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </div>
                            @if($value['menu_status']==1)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Inactive">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$value['menu_id']}}, 'menu/status');">
                                  <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                </a>
                              </div>   
                            @elseif($value['menu_status']==0)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Active">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$value['menu_id']}}, 'menu/status');">
                                  <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                </a>
                              </div>  
                            @endif                            
                          </div>
                        </td>
                        </tr>
                        @endforeach 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection