@extends('admin.layouts.adminlayout')
@section('title', 'Special Gift List')


@section('content')
<div class="content-wrapper">
        <div class="page-title">
          <div>
            <h1><i class="fa fa-th-list"></i> Special Gifts</h1>
            <p></p>
          </div>
          <div>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>Special Gifts</li>
              <li class="active"><a href="#"></a></li>
            </ul>
          </div>          
        </div>
        
        @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="success"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="box_addCat">
              <div class="heading"><h4>Special Gift List</h4></div> 
                
                <button type="button" name="addoffer" id="add" class="addbtn"><a href="{{ url('admin/addoffer') }}">Add </a>
                </button>
              </div>
              <div class="error"></div>
            <div class="card">              
              <div class="card-body">
                <div class="table-responsive no-border">
                  <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Image </th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1;
                     // echo "<pre>"; print_r($alloffer[0]->offer_amount);die;
                      ?>

                      @foreach($alloffer as $key =>$offer)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$offer->offer_name}}</td>
                        <td><img src="{{url('public/uploads/offer')}}/{{$offer->offer_image}}" class="img-test"></td>
                        <td>{{$offer->offer_amount}}</td>                        
                        
                        <td>
                          <div class="link-del-view link_one">
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Edit">
                              <a href="{{ url('admin/editoffer', $offer->offer_id) }}"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="View">
                              <a href="{{ url('admin/viewoffer', $offer->offer_id) }}"  id="viewPort-{{$offer->offer_id}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Delete">
                              <a href="javascript:void(0);" onclick="return deleteStatus({{$offer->offer_id}}, 'deleteoffer');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </div>
                            @if($offer->offer_status==1)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Inactive">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$offer->offer_id}}, 'offerstatus');">
                                  <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                </a>
                              </div> 
                            @elseif($offer->offer_status==0)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Active">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$offer->offer_id}}, 'offerstatus');">
                                  <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                </a>
                              </div>          
                            @endif
                          </div>
                        </td>
                        </tr>
                        @endforeach 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection