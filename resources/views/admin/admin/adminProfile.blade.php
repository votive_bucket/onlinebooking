@extends('admin.layouts.adminlayout')

@section('title', 'Add User')

@section('content')
<?php //echo "<pre>"; print_r($user);die;?>
<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>EDIT PROFILE</h4>
        </div>
     </div>
  </div>
  <div class="row">
    <form action="{{ url('admin/adduser') }}" enctype="multipart/form-data" method="post" name="addUserForm" id="addUserForm">
      <div class="col-sm-3">
        <div class="panel widget light-widget panel-bd-top">
          <div class="panel-heading no-title img-upl"> </div>
          <div class="panel-body pnl">
            <div class="row">
              <div class="upload_profile col-xs-12"> 
                <div class="box_upL">
                <i class="fa fa-upload" aria-hidden="true"></i><input type="file" accept="image/png, image/jpeg" name="profileImg" id="profileImg"></div>
                <!-- <img id="blah" src="#" alt="your image" /> -->
              </div>
            </div>
            <div id="user_profiles">
              @if(isset($user[0]->user_profile_pic))
              <div class="text-center vd_info-parent"> 
                <img alt="example image" src="{{ url('/public/uploads/')}}/{{$user[0]->user_profile_pic }}" class="img-responsive"> 
                <input type="hidden" id="p_img" name="p_img" value="{{$user[0]->user_profile_pic }}" class="img-responsive">

              </div>@else
              <div class="text-center vd_info-parent"> 
                <img alt="example image" src="{{ url('resources/assets/images') }}/uploads/big.jpg" class="img-responsive"> 
              </div>
              @endif

            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-9">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <h4 class="mgbt-xs-15 mgtp-10 font-semibold pad"><i class="icon-user mgr-10 profile-icon"></i> PERSONAL INFORMATION</h4>
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-sm-12">
                    <div class="input-filed">
                      <?php //echo "<pre>"; print_r($user); die;?>
                      <select class="selectpicker1" name="profile" id="profile">
                        <option value=""> Select Profile </option>
                        @foreach($role as $key=>$value)
                        <option value="{{ $value->id }}" 
                          @if(!empty($user))
                          @if($user[0]->user_role_id==$value->id)selected="selected" @endif @endif> Create Profile as {{$value->role_name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('user_fname') ? 'has-error' : ''}}">
                      <label for="firstname">First Name:</label>
                      <input class="form-control" type="text" placeholder="First Name" name="user_fname" id="user_fname" value="@if(isset($user[0]->user_firstname)){{ $user[0]->user_firstname }}@endif">
                      {!! $errors->first('user_fname', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  @if(!empty($user))
                  <input type="hidden" name="edit" id="edit" value="edit">
                  <input type="hidden" name="userid" id="userid" value="{{ $user[0]->id }}">
                  @endif
                  @csrf             
                  <div class="col-sm-6">
                     <div class="form-group {{ $errors->has('user_lname') ? 'has-error' : ''}}">
                        <label for="lastname">Last Name:</label>
                        <input class="form-control" type="text" placeholder="Last Name" name="user_lname" id="user_lname" value="@if(isset($user[0]->user_lastname)){{ $user[0]->user_lastname }} @endif">
                        {!! $errors->first('user_lname', '<p class="help-block">:message</p>') !!}
                     </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                      <label for="email">Email:</label>
                      <input class="form-control" type="email" name="email" placeholder="Email" id="email" value="@if(isset($user[0]->email)){{ $user[0]->email }} @endif">
                      {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                      <label for="mobile">Contact:</label>
                      <input class="form-control" type="text" placeholder="Contact" name="mobile" id="mobile" value="@if(isset($user[0]->user_contact)){{ $user[0]->user_contact }} @endif">
                      {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
                      <label for="dob">Date of Birth:</label>
                      <input class="form-control date" type="text" name="dob" id="dob" placeholder="Date Of Birth" value="@if(isset($user[0]->user_dob)){{ $user[0]->user_dob }} @endif">
                      <div></div>
                      
                      {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                      <label for="password">Gender:</label>


                      


                      <div class="form-control">
                      <!--   <div class="ed_btRadio">
                        <label>
                          <input type="radio" class="radio-inline" name="radios" value="" checked="">
                          <span class="outside"><span class="inside"></span></span>Female
                        </label>
                        <label>
                          <input type="radio"class="radio-inline" name="radios" value="">
                          <span class="outside"><span class="inside"></span></span>Male
                        </label>
                      </div> -->



                        @if(!empty($user))

                          <div class="ed_btRadio">
                            <label>
                              <input type="radio" class="radio-inline" value="female" id="female" placeholder="Gender" checked="" name="gender" @if($user[0]->user_gender == "female" && isset($user[0]->user_gender)) checked @else @endif>
                              <span class="outside"><span class="inside"></span></span>Female
                            </label>
                            <label>
                              <input type="radio" class="radio-inline" id="male" value="male" placeholder="Gender" name="gender" @if($user[0]->user_gender == "male" && isset($user[0]->user_gender)) @else @endif>
                              <span class="outside"><span class="inside"></span></span>Male
                            </label>
                          </div>
                        
                        @else
                        <div class="ed_btRadio">                                            
                            <label>
                              <input type="radio" class="radio-inline" value="female" id="female" placeholder="Gender" name="gender" >
                              <span class="outside"><span class="inside"></span></span>Female
                            </label>
                            <label>
                              <input type="radio" class="radio-inline" id="male" value="male" placeholder="Gender" name="gender" >
                              <span class="outside"><span class="inside"></span></span>Male
                            </label>
                        </div>
                        @endif
                      </div>
                        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  @if(!empty($user))
                  @else
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                      <label for="password">Password:</label>
                      <input class="form-control" type="password" placeholder="Password" id="password" name="password">
                      {!! $errors->first('password', '<p class="help-block">:message</p>') !!}                      
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('confirmpass') ? 'has-error' : ''}}">
                      <label for="retypepassword">Retype Password:</label>
                      <input type="password" id="confirmpass" placeholder="Retype Password" class="form-control" name="confirmpass">
                      {!! $errors->first('confirmpass', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  @endif
                  
                </div>
              </div>
              <hr class="pd-10">
              <div class="row">
                <div class="prof_info">
                  <h4 class="mgbt-xs-15 mgtp-10 font-semibold pad"><i class="icon-user mgr-10 profile-icon"></i>ADDRESSS INFORMATION</h4>
                  <div class="col-sm-12">
                    <!-- <div class="col-sm-6">
                      <div class="form-group">
                        <label for="address">Full Address</label>
                        <input type="text" id="address" name="address" placeholder="Address" class="form-control">
                      </div>
                    </div> -->
                   
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="country">Country:</label>
                        <select name="country" id="country" class="form-control">
                          <option value="">Choose Country</option>
                          @foreach($country as $key => $value)
                            <option value="{{$value->id}}" @if(!empty($user))
                          @if($country_selected[0]->id==$value->id)selected="selected" @endif @endif>{{$value->name}}</option>
                          @endforeach
                                                      
                        </select>                        
                      </div>
                    </div>
                     <div class="col-sm-6">
                       <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
                          <label for="state">State:</label>
                          <select class="form-control" name="state" id="state"> @if(!empty($user))
                           @if(!empty($state))

                           <option value="{{$state[0]->id}}"> {{ $state[0]->name}}</option>@endif
                           @else
                           <option value="">Select State</option>
                           @endif
                          </select>
                          {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
                        </div>
                      </div>
                     <div class="col-sm-6">
                        <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                           <label for="city">City:</label>
                           <select class="form-control" name="city" id="city">
                            @if(!empty($user))
                             @if(!empty($city))
                             <option value="{{$city[0]->id}}"> {{ $city[0]->name}}</option>@endif
                             @else
                             <option value="">Select City</option>
                             @endif
                          </select>
                          {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
                        </div>
                     </div>
                     
                     
                      <div class="col-sm-6">              
                        <div class="form-group {{ $errors->has('street') ? 'has-error' : ''}}">
                          <label for="street">Street:</label>
                          <input id="street" class="form-control" placeholder="Street" type="text" name="street" value="@if(isset($user[0]->user_street)) {{ $user[0]->user_street }} @endif">
                            {!! $errors->first('street', '<p class="help-block">:message</p>') !!}
                        </div>
                      </div> 
                      <div class="col-sm-6">              
                        <div class="form-group {{ $errors->has('postalcode') ? 'has-error' : ''}}">
                          <label for="postalcode">Postal Code:</label>
                          <input id="postalcode" class="form-control" placeholder="Postal Code" type="text" name="postalcode" value="@if(isset($user[0]->user_postcode)){{ $user[0]->user_postcode }} @endif">
                            {!! $errors->first('postalcode', '<p class="help-block">:message</p>') !!}
                        </div>
                      </div>
                     
                  </div>
                </div>
              </div>
              <hr class="pd-10">
              <div class="row" id="additional_info">
                <div class="prof_info">
                  <h4 class="mgbt-xs-15 mgtp-10 font-semibold pad"><i class="icon-user mgr-10 profile-icon"></i>ADDITIONAL INFORMATION</h4>
                  <div class="col-sm-12">
                    <div class="col-sm-6">
                      <div class="form-group mult_sec">
                         <label for="category">Category:</label>
                         @if(isset($artist_additional[0]->category_ids))
                         <?php $cat = $artist_additional[0]->category_ids;
                            $cat_array = explode(',', $cat);?>
                         
                         <select class="form-control" name="category[]" id="category" multiple="multiple">
                            @foreach($category as $key => $value)
                              <option value="{{$value->id}}" @if(in_array($value->id, $cat_array)) selected="selected" @else @endif>{{$value->category_name_en}}</option>
                            @endforeach
                         </select>
                         @else
                         <select class="form-control" name="category[]" id="category" multiple="multiple">
                            @foreach($category as $key => $value)
                              <option value="{{$value->id}}" >{{$value->category_name_en}}</option>
                            @endforeach                            
                         </select>
                         @endif
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group mult_sec">
                        <label for="subcategory">Subcategory:</label>
                        <select class="form-control" name="subcategory[]" id="subcategory" multiple="multiple"> 
                                                 
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="education">Education/Year Of Graduation:</label>
                        <input type="text" id="education" name="education" placeholder="Education/Year Of Graduation" class="form-control" value="@if(isset($artist_additional[0]->education)) {{ $artist_additional[0]->education }} @endif">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                         <label for="workshops">Workshops/Year of experience:</label>
                         <input type="text" id="workshops" placeholder="Workshops/Year of experience" name="workshops" class="form-control" value="@if(isset($artist_additional[0]->workshops)) {{ $artist_additional[0]->workshops }} @endif">
                      </div>
                    </div> 
                    <div class="col-sm-6">
                      <div class="form-group">
                         <label for="budget">Budget: </label>
                           <input type="text" class="form-control" id="budget" name="budget" placeholder="Budget" value="@if(isset($artist_additional[0]->budget)) {{ $artist_additional[0]->budget }} @endif">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                         <label for="email">Facebook Link: </label>
                           <input type="text" class="form-control" id="fblink" name="fblink" placeholder="Facebook Link" value="@if(isset($artist_additional[0]->facebook_link)) {{ $artist_additional[0]->facebook_link }} @endif"> 
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                         <label for="email">Instagram Link: </label>
                           <input type="text" class="form-control" id="inlink" name="inlink" placeholder="Instagram Link" value="@if(isset($artist_additional[0]->insta_link)) {{ $artist_additional[0]->insta_link }} @endif">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                         <label for="email">Linkedin Link: </label>
                           <input type="text" class="form-control" id="lnlink" name="lnlink" placeholder="Linkedin Link" value="@if(isset($artist_additional[0]->linked_link)) {{ $artist_additional[0]->linked_link }} @endif">
                      </div>
                    </div>                   
                    <div class="col-sm-6">
                      <div class="form-group">
                         <label for="email">Represented by: </label>
                           <input type="text" class="form-control" id="agency" name="agency" value="@if(isset($user[0]->agency_name)) {{ $user[0]->agency_name }} @endif" placeholder="Agency Name">
                      </div>
                    </div> 
                    <div class="col-sm-6">
                      <div class="form-group mult_sec">
                        <label for="language">Language Known: </label>
                        <div class=""> 
                          @if(isset($artist_additional[0]->language_ids))
                          <?php 
                            $language = $artist_additional[0]->language_ids;
                            $language_array = explode(',', $language);
                          ?>
                           
                          <select id="multi-select-demo" name="language[]" class="form-control" multiple="multiple"> 
                            @foreach($languages as $key => $value)
                              <option value="{{$value->id}}" @if(in_array($value->id, $language_array)) selected="selected" @else @endif>{{$value->language_name}}</option>
                            @endforeach
                          </select>
                          @else
                          <select id="multi-select-demo" name="language[]" class="form-control" multiple="multiple"> 
                            @foreach($languages as $key => $value)
                               <option value="{{$value->id}}">{{$value->language_name}}</option>
                               @endforeach                         
                           </select>
                           @endif 
                        </div>
                      </div>
                    </div>                      
                     <div class="col-sm-6">
                      <div class="form-group">
                         <label for="email">How experience are you?</label>
                         <div class="form-control">
                            <div class="ed_btRadio">
                              <label>
                                <input type="radio" id="10" name="experience" value="0-10" @if(isset($artist_additional[0]->experience) && $artist_additional[0]->experience == "0-10") checked @else @endif>
                                <span class="outside"><span class="inside"></span></span>0-10
                              </label>
                              <label>
                                <input type="radio" id="100" name="experience" value="10-100" @if(isset($artist_additional[0]->experience) && $artist_additional[0]->experience == "10-100") checked @else @endif>
                                <span class="outside"><span class="inside"></span></span>10-100
                              </label>
                              <label>
                                <input type="radio" id="over" name="experience" value="Over 100" @if(isset($artist_additional[0]->experience) && $artist_additional[0]->experience == "Over 100") checked @else @endif>
                                <span class="outside"><span class="inside"></span></span>Over 100
                              </label>
                            </div>                           
                         </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                         <label for="commision">Agency Commision:</label>
                           <input type="text" class="form-control" id="commision" name="commision" value="@if(isset($artist_additional[0]->commision)) {{ $artist_additional[0]->commision }}@endif" placeholder="Agency Commision">
                      </div>
                    </div> 
                    <div class="col-sm-12">
                    <div class="form-group">
                       <label for="description">Description:</label>
                       <textarea class="txtarea" name="description" id="description">@if(isset($artist_additional[0]->artist_bio)){{ $artist_additional[0]->artist_bio }} @endif</textarea>
                    </div>
                  </div>                 
                  </div>
                </div>
              </div>
              <div class="rrow" id="additional_info">
                <div class="prof_info">
                  <div class="col-sm-12">
                    <button class="btn vd_btn vd_bg-green"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                  </div> 
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

</div>
<!-- <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
  CKEDITOR.replace( 'description' );
</script> -->
@endsection