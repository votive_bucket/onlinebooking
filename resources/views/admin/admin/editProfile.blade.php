@extends('admin.layouts.adminlayout')

@section('title', 'Add User')

@section('content')
<?php //echo "<pre>"; print_r($user->user_street); die;?>
<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>EDIT PROFILE</h4>
        </div>
     </div>
  </div>
  <div class="row">
    <form action="{{ url('admin/updateadmin') }}" enctype="multipart/form-data" method="post" name="addUserForm" id="addUserForm">
      <div class="col-sm-3">
        <div class="panel widget light-widget panel-bd-top">
          <div class="panel-heading no-title img-upl"> </div>
          <div class="panel-body pnl">
            <div class="row">
              <div class="upload_profile col-xs-12"> 
                <div class="box_upL">
                  <i class="fa fa-upload" aria-hidden="true"></i>
                  <input type="file" accept="image/png, image/jpeg" name="profileImg" id="profileImg">
                </div>
              </div>
            </div>
            <div id="user_profiles">
              @if(isset($user->user_profile_pic))
              <div class="text-center vd_info-parent"> 
                <img alt="example image" src="{{ url('/public/uploads/')}}/{{$user->user_profile_pic }}" class="img-responsive"> 
                <input type="hidden" id="p_img" name="p_img" value="{{$user->user_profile_pic }}" class="img-responsive">
              </div>
              @else
              <div class="text-center vd_info-parent"> 
                <img alt="example image" src="{{ url('resources/assets/images') }}/uploads/big.jpg" class="img-responsive"> 
              </div>
              @endif
              
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-9">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <h4 class="mgbt-xs-15 mgtp-10 font-semibold pad"><i class="icon-user mgr-10 profile-icon"></i> PERSONAL INFORMATION</h4>
              <div class="row">
                <div class="col-sm-12">
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
                      <label for="firstname">First Name:</label>
                      <input class="form-control" type="text" placeholder="First Name" name="first_name" id="first_name" value="@if(isset($user->first_name)){{$user->first_name}}@endif">
                      {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  @if(!empty($user))
                  <input type="hidden" name="edit" id="edit" value="edit">
                  <input type="hidden" name="userid" id="userid" value="{{ $user->id }}">
                  @endif
                  @csrf             
                  <div class="col-sm-6">
                     <div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
                        <label for="lastname">Last Name:</label>
                        <input class="form-control" type="text" placeholder="Last Name" name="last_name" id="last_name" value="@if(isset($user->last_name)){{$user->last_name}} @endif">
                        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                     </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('user_name') ? 'has-error' : ''}}">
                      <label for="user_name">User Name:</label>
                      <input class="form-control" type="user_name" name="user_name" placeholder="User Name" id="user_name" value="@if(isset($user->user_name)){{$user->user_name}}@endif">
                      {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                      <label for="email">Email:</label>
                      <input class="form-control" type="email" name="email" placeholder="Email" id="email" value="@if(isset($user->email)){{$user->email}}@endif">
                      {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('user_contact') ? 'has-error' : ''}}">
                      <label for="user_contact">Contact:</label>
                      <input class="form-control" type="text" placeholder="Contact" name="user_contact" id="user_contact" value="@if(isset($user->user_contact)){{$user->user_contact}}@endif">
                      {!! $errors->first('user_contact', '<p class="help-block">:message</p>') !!}
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="rrow" id="additional_info">
                <div class="prof_info">
                  <div class="col-sm-12">
                    <button class="btn vd_btn vd_bg-green"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                  </div> 
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

</div>
<!-- <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
  CKEDITOR.replace( 'description' );
</script> -->
@endsection