@extends('admin.layouts.adminlayout')
@section('title', 'Profile')

@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-sm-12">
      <div class="ad-user">
        <h4><i class="icon-user mgr-10 profile-icon"></i> ADMIN PROFILE</h4>
      </div>
      @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
      @endif
      <!-- <div class="tab-content">
        <div id="profile-tab" class="tab-pane active">
          <div class="pd-20">                          
            <div class="row"> -->
              <div class="col-sm-3">
                <div class="panel widget light-widget panel-bd-top">
                  <div class="panel-heading no-title"></div>
                  <div class="panel-body">
                    <div class="text-center vd_info-parent">
                      @if($user->user_profile_pic)
                      <img alt="example image" src="{{ url('public/uploads/') }}/{{ $user->user_profile_pic }}" class="img-responsive"> 
                      @else  
                      <img alt="example image" src="{{ url('resources/assets/images/uploads/') }}/user.png" class="img-responsive">
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-9">
                <div class="tab-content">
                  <div id="profile-tab" class="tab-pane active">
                    <div class="pd-20">
                      <!-- <div class="vd_info tr"> 
                        <a class="btn vd_btn btn-xs vd_bg-yellow"> <i class="fa fa-pencil append-icon"></i> Edit </a> 
                      </div> -->      
                      <h4 class="mgbt-xs-15 mgtp-10 font-semibold pad"><i class="icon-user mgr-10 profile-icon"></i> PERSONAL INFORMATION</h4>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              
                              <label class="col-xs-5 control-label">First Name:</label>
                              
                              <div class="col-xs-7 controls">
                                @if($user->first_name){{ ucfirst(trans($user->first_name)) }}@endif</div>
                              <!-- col-sm-10 --> 
                            </div>
                          </div> 
                          <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              
                              <label class="col-xs-5 control-label">Last Name:</label>
                              
                              <div class="col-xs-7 controls">
                                @if($user->last_name){{ ucfirst(trans($user->last_name)) }}@endif</div>
                              <!-- col-sm-10 --> 
                            </div>
                          </div> 
                          <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              
                              <label class="col-xs-5 control-label">User Name:</label>
                              
                              <div class="col-xs-7 controls">
                                @if($user->user_name){{ ucfirst(trans($user->user_name)) }}@endif</div>
                              <!-- col-sm-10 --> 
                            </div>
                          </div>              
                          <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-5 control-label">Email:</label>
                              <div class="col-xs-7 controls">@if($user->email){{ ucfirst(trans($user->email)) }}@endif</div>
                              <!-- col-sm-10 --> 
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-5 control-label">Contact:</label>
                              <div class="col-xs-7 controls">@if($user->user_contact){{ $user->user_contact }}@endif</div>
                              <!-- col-sm-10 --> 
                            </div>
                          </div>
                          <!-- <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-5 control-label">Gender:</label>
                              <div class="col-xs-7 controls">@if($user->user_gender){{ $user->user_gender }}@endif</div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-5 control-label">DOB:</label>
                              <div class="col-xs-7 controls">@if($user->user_dob){{ $user->user_dob }}@endif</div>
                            </div>
                          </div> -->
                          <div class="col-sm-6">
                            <div class="row mgbt-xs-0">
                              <label class="col-xs-5 control-label">User Type:</label>
                              <div class="col-xs-7 controls">
                                @if($user->user_role_id==1) Administrator 
                                @endif</div>
                              <!-- col-sm-10 --> 
                            </div>
                          </div>
                        </div>
                      </div>
                      
                      <button type="submit" name="edit" id="edit" class="goback"><a href="{{url('admin/editprofile', $user->id)}}">Edit Profile</a></button>
                    </div>
                      <!-- pd-20 --> 

                  </div>
                </div>
              </div>
            <!-- </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
  <!-- end of row  -->
</div>
@endsection