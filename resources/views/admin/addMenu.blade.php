@extends('admin.layouts.adminlayout')

@section('title', 'Add Menu')
@section('content')
<?php //echo "<pre>"; print_r($menu->category_id); die;?>
<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>@if(!empty($menu)) EDIT @else ADD @endif Menu</h4>
        </div>
     </div>
  </div>

<div class="card">
  <div class="row">
    <form action="{{url('admin/addmenu')}}" method="POST" name="addmenuform" id="addmenuform">
      <div class="col-sm-12">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <div class="row">
                <div class="col-sm-12">
                  @csrf
                  <?php $edit = false; ?>
                  @if(!empty($menu))
                  <?php $edit = true; ?>
                  <input type="hidden" class="form-control" id="edit" name="edit" value="edit">
                  <input type="hidden" class="form-control" id="menuid" name="menuid" value="{{$menu->menu_id}}">
                  @endif

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Menu Name:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="menu_name" name="menu_name" value="{{ isset($menu->menu_name) ? $menu->menu_name : '' }}">
                        </div>
                      </div>
                      </div>
                    </div>
                   <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Select Venue:</label>
                        <div class="col-sm-10">
                           <select name="venue" class="form-control">
                              <option value="">select venue</option>
                              @if(!$venues->isEmpty())
                                @foreach($venues as $v)
                                  <option value="{{$v->venue_id}}" @if($edit && $v->venue_id==$menu->menu_venue) selected @endif>{{$v->venue_name}}</option>
                                @endforeach
                              @endif
                           </select>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Select Combo:</label>
                        <div class="col-sm-10">
                           <select name="combo" class="form-control">
                              <option value="">select combo</option>
                              @if(!$combo->isEmpty())
                                @foreach($combo as $c)
                                  <option value="{{$c->combo_id}}" @if($edit && $c->combo_id==$menu->menu_combo) selected @endif >{{$c->combo_name}}</option>
                                @endforeach
                              @endif
                           </select>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Price:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="Price" name="Price" value='@if($edit){{str_replace(",",".",number_format($menu->menu_price))}}@endif'>
                        </div>
                      </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                        <label class="col-sm-2" for="menu_name">Main Course:</label>
                        <div class="col-md-10">
                          <textarea id="submenus" name="submenus" class="form-control">@if($edit){!!$menu->submenu_ids!!}@endif</textarea>
                        </div>
                      </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="row" id="additional_info">
                <div class="prof_info">
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-md-offset-4 col-sm-6">  
                        <button class="btn vd_btn vd_bg-green" type="submit" id="menusubmit"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                      </div>
                    </div>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>

<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'submenus', {
      language:'es'
    });
</script>

@endsection