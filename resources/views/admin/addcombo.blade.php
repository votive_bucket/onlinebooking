@extends('admin.layouts.adminlayout')
@section('title', 'Add Combo')
@section('content')

<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>@if(!empty($Combo))EDIT @else ADD @endif COMBO</h4>
        </div>
     </div>
  </div>
  <div class="row">
    <form action="{{ url('admin/addcombopost') }}" enctype="multipart/form-data" method="post" name="comboForm" id="comboForm">
      <div class="col-sm-12">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <div class="row">
                <div class="col-sm-12">
                  @if(!empty($combo))                  
                  <input type="hidden" name="comboid" id="comboid" value="{{ $combo->combo_id }}">
                  @endif
                  @csrf 
                  <div class="row">
                    <div class="col-sm-12">
                       <div class="form-group">
                          <div class="row">
                            <label  class="col-md-2" for="combo_name">Combo Name:</label>
                            <div class="col-md-10">
                            <input class="form-control" type="text" placeholder="Combo Name" name="combo_name" id="combo_name" value="{{isset($combo->combo_name) ? $combo->combo_name : ''}}">
                            </div>
                        </div>
                       </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="combo_price">Combo Price:</label>
                          <div class="col-md-10">
                            <input class="form-control number" type="text" placeholder="Combo Price" name="combo_price" id="combo_price" value="{{isset($combo->combo_price) ? $combo->combo_price : ''}}">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="combo_content">Combo Content:</label>
                          <div class="col-md-10">
                            <textarea placeholder="Combo Content" name="combo_content" id="combo_content">{!!isset($combo->combo_content)? $combo->combo_content : ''!!}</textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="combo_image">Combo Image:</label>
                          <div class="col-md-10">
                            <div class="col-md-6 form-control">
                              <input type="file" accept="image/png, image/jpeg" name="combo_image" id="combo_image" >
                            </div>
                            <div class="col-md-6">
                              <div class="row">
                                <div id="img_preview">
                                  @if(isset($combo->combo_image))
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('/public/uploads/combo')}}/{{$combo->combo_image }}" class="img-test">
                                    </div>
                                  @else
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('resources/assets/images') }}/logo_black.png" class="img-test" >
                                    </div>
                                  @endif
                                  <input type="hidden" id="upload_img" name="upload_img" value="{{isset($combo->combo_image) ? $combo->combo_image :'' }}" class="img-test" >
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                    
                    </div> 
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="combo_image">Combo Image (Selected):</label>
                          <div class="col-md-10">
                            <div class="col-md-6 form-control">
                              <input type="file" accept="image/png, image/jpeg" name="combo_selected_image" id="combo_selected_image" >
                            </div>
                            <div class="col-md-6">
                              <div class="row">
                                <div id="img_selected_preview">
                                  @if(isset($combo->combo_selected_image))
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('/public/uploads/combo')}}/{{$combo->combo_selected_image }}" class="img-test">
                                    </div>
                                  @else
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('resources/assets/images') }}/logo_black.png" class="img-test" >
                                    </div>
                                  @endif
                                  <input type="hidden" id="upload_selected_img" name="upload_selected_img" value="{{isset($combo->combo_selected_image) ? $combo->combo_selected_image :'' }}" class="img-test" >
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                    
                    </div>
                </div>
                <div class="row" id="additional_info">
                  <div class="prof_info">
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-md-offset-4 col-sm-6">
                          <button class="btn vd_btn vd_bg-green"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div> 
            </div>
          </div>                    
        </div>
      </div>
    </form>
  </div>
</div>

<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<script>
    CKEDITOR.replace( 'combo_content' );
</script>


@endsection