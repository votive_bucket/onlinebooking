@extends('admin.layouts.adminlayout')
@section('title', 'Add Offer')
@section('content')

<div class="content-wrapper">
  <div class="row">
     <div class="col-sm-12">
        <div class="ad-user">
           <h4>@if(!empty($offer))EDIT @else ADD @endif OFFER</h4>
        </div>
     </div>
  </div>
  <div class="row">
    <form action="{{ url('admin/addofferpost') }}" enctype="multipart/form-data" method="post" name="offerForm" id="offerForm">
      <div class="col-sm-12">
        <div class="tab-content">
          <div id="profile-tab" class="tab-pane active">
            <div class="pd-20">
              <div class="row">
                <div class="col-sm-12">
                  @if(!empty($offer))                  
                  <input type="hidden" name="offerid" id="offerid" value="{{ $offer->offer_id }}">
                  @endif
                  @csrf 
                  <div class="row">
                    <div class="col-sm-12">
                       <div class="form-group">
                          <div class="row">
                            <label  class="col-md-2" for="offer_name">Offer Name:</label>
                            <div class="col-md-10">
                            <input class="form-control" type="text" placeholder="Offer Name" name="offer_name" id="offer_name" value="{{isset($offer->offer_name) ? $offer->offer_name : ''}}">
                            </div>
                        </div>
                       </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="offer_amount">Minimum Price for Offer:</label>
                          <div class="col-md-10">
                            <input class="form-control number" type="text" placeholder="Minimum Price for Offer" name="offer_amount" id="offer_amount" value="{{isset($offer->offer_amount) ? $offer->offer_amount : ''}}">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <div class="row">
                          <label class="col-md-2" for="offer_image">Offer Image:</label>
                          <div class="col-md-10">
                            <div class="col-md-6 form-control">
                              <input type="file" accept="image/png, image/jpeg" name="offer_image" id="offer_image" class="upload_image">
                            </div>
                            <div class="col-md-6">
                              <div class="row">
                                <div id="img_preview">
                                  @if(isset($offer->offer_image))
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('/public/uploads/offer')}}/{{$offer->offer_image }}" class="img-test">
                                    </div>
                                  @else
                                    <div class="text-center vd_info-parent"> 
                                      <img alt="example image" src="{{ url('resources/assets/images') }}/logo_black.png" class="img-test" >
                                    </div>
                                  @endif
                                  <input type="hidden" id="upload_img" name="upload_img" value="{{isset($offer->offer_image) ? $offer->offer_image :'' }}" class="img-test" >
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                    
                    </div> 
                </div>
                <div class="row" id="additional_info">
                  <div class="prof_info">
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-md-offset-4 col-sm-6">
                          <button class="btn vd_btn vd_bg-green"><span class="menu-icon"><i class="fa fa-fw fa-check"></i></span> Submit</button>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div> 
            </div>
          </div>                    
        </div>
      </div>
    </form>
  </div>
</div>



@endsection