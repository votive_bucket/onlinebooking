@extends('admin.layouts.adminlayout')
@section('title', 'View Menu')

@section('content')
<div class="content-wrapper">
  <div class="row">    
    <div class="col-sm-12">
      <div class="ad-user">
        <h4><i class="icon-user mgr-10 profile-icon"></i> Menu Information</h4>
      </div>  
      <div class="tab-content">
        <div id="profile-tab" class="tab-pane active">
          <div class="pd-20">            
            <div class="row">
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Menu Title:</label>
                  <div class="col-xs-7 controls">@if(isset($menu->menu_name)){{ $menu->menu_name }}@endif</div>
                </div>
              </div>
              

             <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Venue:</label>
                  <div class="col-xs-7 controls">@if(isset($menu->venue)){{ $menu->venue }}@endif</div>
                </div>
              </div> 
             <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Combo:</label>
                  <div class="col-xs-7 controls">@if(isset($menu->combo)){{ $menu->combo }}@endif</div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Price:</label>
                  <div class="col-xs-7 controls">@if(isset($menu->menu_price)){{str_replace(",",".",number_format($menu->menu_price))}}@endif</div>
                </div>
              </div>                                       
            
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-5 control-label">Main Course:</label>
                  <div class="col-xs-7 controls">@if(!empty($menu->submenu_ids))
                    
                      <span class="sel_submenu">{!!$menu->submenu_ids? $menu->submenu_ids:''!!}</span>
                  @endif
                  </div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
            </div> 

            <button class="goback"><a href="{{ url('admin/menu/list') }}">Go Back</a></button>
            <!-- row --> 
          </div>

       

          
            <!-- pd-20 --> 
        </div>
      </div>
    </div>
  </div>
  <!-- end of row  -->
</div>
@endsection