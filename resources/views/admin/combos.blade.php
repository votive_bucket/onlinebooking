@extends('admin.layouts.adminlayout')
@section('title', 'Combo List')


@section('content')
<div class="content-wrapper">
        <div class="page-title">
          <div>
            <h1><i class="fa fa-th-list"></i> Combos</h1>
            <p></p>
          </div>
          <div>
            <ul class="breadcrumb side">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li>Combos</li>
              <li class="active"><a href="#"></a></li>
            </ul>
          </div>          
        </div>
        
        @if(Session::has('message'))
          <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
        @endif
        <div class="success"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="box_addCat">
              <div class="heading"><h4>Combo List</h4></div> 
                
                <button type="button" name="addhowitworks" id="add" class="addbtn"><a href="{{ url('admin/addcombo') }}">Add </a>
                </button>
              </div>
              
            <div class="card">              
              <div class="card-body">
                <div class="table-responsive no-border">
                  <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Content </th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i=1;
                      ?>
                      @foreach($allcombo as $key =>$combo)
                      <tr>
                        <td>{{$i++}}</td>
                        <td>{{$combo->combo_name}}</td>
                        <td>{!! substr(strip_tags($combo->combo_content), 0, 150) !!}</td>
                        <td>{{$combo->combo_price}}</td>                        
                        
                        <td>
                          <div class="link-del-view link_one">
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Edit">
                              <a href="{{ url('admin/editcombo', $combo->combo_id) }}"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="View">
                              <a href="{{ url('admin/viewcombo', $combo->combo_id) }}"  id="viewPort-{{$combo->combo_id}}"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            </div>
                            <div class="btntip" data-toggle="tooltip" data-placement="top" title="Delete">
                              <a href="javascript:void(0);" onclick="return deleteStatus({{$combo->combo_id}}, 'deletecombo');"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            </div>
                            @if($combo->combo_status==1)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Inactive">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$combo->combo_id}}, 'combostatus');">
                                  <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                </a>
                              </div> 
                            @elseif($combo->combo_status==0)
                              <div class="btntip" data-toggle="tooltip" data-placement="top" title="Active">
                                <a href="javascript:void(0);" onclick="return updateStatus({{$combo->combo_id}}, 'combostatus');">
                                  <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                </a>
                              </div>          
                            @endif
                          </div>
                        </td>
                        </tr>
                        @endforeach 
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection