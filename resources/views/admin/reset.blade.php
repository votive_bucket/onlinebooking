<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/admin/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('resources/assets/css/font-awesome.css') }}">
   
    <title>Bud Station Admin - Login</title>
    
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <a href="#">Bud Station</a>
      </div>
      <div class="login-box">
        <form role="form" method="POST" action="" name="password_reset" id="password_reset" class="login-form">
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Reset Password</h3>
              <div class="rset-head">
                <div id="myLoadDiv">
                  <img class="loading_image" src="{{ url('resources/assets/images/light_rounded/loader.gif') }}" style="display:none;"/>
                </div>
                <div class="alert alert-danger" style="display: none"></div>
                <div class="alert alert-success" style="display: none">Password Reset Successfully</div>
                <div id="myLoadDiv">
                    <img id="loading_image" src="{{ url('resources/assets/images/light_rounded/loader.gif') }}" style="display:none;"/>
                </div>
              </div>
             
              {{ csrf_field() }}
              <input type="hidden" id="vrfn_code" name="vrfn_code" value="{{ request()->segment(4) }}">
            
              <div class="form-group rsetbg {{ $errors->has('password') ? ' has-error' : '' }}">
                @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                @endif
                <label class="control-label">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password*"/>
              </div>
          
              <div class="form-group rsetbg {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                @if ($errors->has('password_confirmation'))
                <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                @endif
                <label class="control-label">Re-enter Password</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password*"/>
              </div>   

              <div class="form-group btn-container">
                <input type="submit" name="resetpassword" class="btn btn-primary btn-block" value="Reset Rassword" id="resetpassword">
              </div>

        </form>
      </div>
    </section>
  </body>

  
  <script src="{{ url('resources/assets/js/admin/jquery-2.1.4.min.js') }}"></script>
  <script src="{{ url('resources/assets/js/admin/bootstrap.min.js') }}"></script>  
  <script src="{{ url('resources/assets/js/admin/main.js') }}"></script>
  <script src="{{ url('resources/assets/js/jquery.validate.min.js') }}"></script>
  <script type="text/javascript">
    /*Reset Password*/
  $('#password_reset').validate({
    rules: {
      password: {
        required: true,
        minlength: 6,
      },
      password_confirmation: {
        required: true,
        minlength: 6,
        equalTo : "#password"
      },
    },
    submitHandler: function(form) {
      baseurl = "{{url('/admin/login')}}";
      jQuery.ajaxSetup({
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      jQuery.ajax({
          url: "{{url('admin/password/reset')}}",
          method: 'post',
          data: {
              vrfn_code : jQuery('#vrfn_code').val(),
              password : jQuery('#password').val(),
              password_confirmation : jQuery('#password_confirmation').val()
          },
          beforeSend: function() {
              $(".loading_image").show();
          },
          success: function(data){
              if($.isEmptyObject(data.errors)){
                  jQuery('.alert-danger').hide();
                  jQuery('.alert-success').show();
                  setTimeout(function() {
                      window.location.replace(baseurl);
                  }, 2000);
              } else {
                  jQuery('.alert-danger').html('');
                  jQuery.each(data.errors, function(key, value){
                      jQuery('.alert-danger').show();
                      jQuery('.alert-danger').append('<p>'+value+'</p>');
                  });
              }
          },
          complete: function(){
              $('.loading_image').hide();
          }                    
      });
    }  
  });
  </script>
</html>