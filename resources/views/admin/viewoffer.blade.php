@extends('admin.layouts.adminlayout')
@section('title', 'Offer')

@section('content')
<div class="content-wrapper">
  <div class="row">    
    <div class="col-sm-12">
      <div class="ad-user">
        <h4><i class="icon-user mgr-10 profile-icon"></i> OFFER INFORMATION</h4>
      </div> 
      <div class="tab-content">
        <div id="profile-tab" class="tab-pane active">
          <div class="pd-20">              
            <!-- <h4 class="mgbt-xs-15 mgtp-10 font-semibold pad"><i class="icon-user mgr-10 profile-icon"></i> combo INFORMATION</h4> -->
            <div class="row">
                <div class="col-sm-12">
                  <div class="row mgbt-xs-0">
                    <label class="col-xs-5 control-label">Offer Name:</label>
                    <div class="col-xs-7 controls">@if(isset($offer->offer_name)){{ ucfirst(trans($offer->offer_name)) }}@endif</div>
                    <!-- col-sm-10 --> 
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="row mgbt-xs-0">
                    <label class="col-xs-5 control-label">Offer Price:</label>
                    <div class="col-xs-7 controls">@if(isset($offer->offer_amount)){{ ucfirst(trans($offer->offer_amount)) }}@endif</div>
                    <!-- col-sm-10 --> 
                  </div>
                </div>
                       
                
                <div class="col-sm-12">
                  <div class="row mgbt-xs-0">
                    <label class="col-xs-5 control-label">Offer Image:</label>
                    <div class="col-xs-7 controls">
                      <img alt="example image" src="{{ url('/public/uploads/offer')}}/{{$offer->offer_image }}" class="img-test">
                     </div>
                    <!-- col-sm-10 --> 
                  </div>
                </div>
              
            </div> 
            <!-- row --> 
            <button class="goback"><a href="{{ url('/admin/alloffer') }}">Go Back</a></button>
          </div>
            <!-- pd-20 --> 
        </div>
      </div>
    </div>
  </div>
  <!-- end of row  -->
</div>
@endsection