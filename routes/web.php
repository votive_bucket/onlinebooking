<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','PagesController@frontPage');
Route::get('choose/combo','PagesController@comboPage');

Route::get('delete/{id}', 'PagesController@delete');
Route::get('offers','PagesController@offerPage');
if (Request::isMethod('post'))
{
	Route::post('saveorder','PagesController@saveorder');
}else{
	Route::get('saveorder','PagesController@saveorder');
}

Route::get('get/menus','PagesController@getMenus');
Route::get('venue/get-district-list','PagesController@getDistrictList');
Route::get('filter/venues','PagesController@getVenuesList');
Route::get('default/venues','PagesController@getComboVenuesList');
Route::get('venue/details','PagesController@getVenueDetails');

Route::get('signout', 'Admin\Auth\LoginController@logout');
if (Request::isMethod('post'))
{
	Route::post('thankyou', 'PagesController@thankyou');
}else{
	Route::get('thankyou', 'PagesController@thankyou');
}

//Admin Login
Route::get('admin/login', [ 'as' => 'admin/login', 'uses' => 'Admin\Auth\LoginController@login']);

Route::post('dologin', [
   'as' => 'admin.dologin',
   'uses' => 'Admin\Auth\LoginController@authenticate',
]);
Route::get('admin/viewprofile', 'Admin\UserController@viewprofile');
Route::get('admin/editprofile/{id}', 'Admin\UserController@editprofile');
Route::post('admin/updateadmin', 'Admin\UserController@updateadminProfile');

Route::get('admin/dashboard', 'Admin\Auth\LoginController@dashboard');

//Change password

Route::get('admin/resetpassword', 'Admin\Auth\ResetPasswordController@resetpasswordview')->middleware('auth');

Route::post('admin/changepass', 'Admin\Auth\ResetPasswordController@resetPassword');

//Admin- Combo management routes
Route::get('admin/allcombo', 'Admin\ComboController@allcombo');
Route::get('admin/viewcombo/{id}', 'Admin\ComboController@viewcombo');
Route::get('admin/editcombo/{id}', 'Admin\ComboController@editcombo');
Route::post('admin/deletecombo', 'Admin\ComboController@deletecombo');
Route::post('admin/combostatus', 'Admin\ComboController@combostatus');
Route::get('admin/addcombo', 'Admin\ComboController@addcombo');
Route::post('admin/addcombopost', 'Admin\ComboController@addcombopost');

//Send password using mail
Route::post('admin/forgetlink', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail');

Route::get('admin/password/reset/{token}', 'Admin\Auth\ForgotPasswordController@showResetForm');
Route::post('admin/password/reset', 'Admin\Auth\ForgotPasswordController@resetPassword');

Route::get('admin/dashboard', 'Admin\Auth\LoginController@dashboard');

//Admin side Create Menus 
Route::get('admin/menu/add', 'Admin\MenuController@MenuAddView');
Route::get('admin/menu/list', 'Admin\MenuController@MenusList');
Route::post('admin/addmenu', 'Admin\MenuController@AddMenu');
Route::get('admin/menu/view/{id}', 'Admin\MenuController@ViewMenu');
Route::get('admin/menu/edit/{id}', 'Admin\MenuController@EditMenu');
Route::post('admin/menu/delete', 'Admin\MenuController@DeleteMenu');
Route::post('admin/menu/status', 'Admin\MenuController@UpdateMenuStatus');


//Admin- Offer management routes
Route::get('admin/alloffer', 'Admin\OfferController@alloffer');
Route::get('admin/viewoffer/{id}', 'Admin\OfferController@viewoffer');
Route::get('admin/editoffer/{id}', 'Admin\OfferController@editoffer');
Route::post('admin/deleteoffer', 'Admin\OfferController@deleteoffer');
Route::post('admin/offerstatus', 'Admin\OfferController@offerstatus');
Route::get('admin/addoffer', 'Admin\OfferController@addoffer');
Route::post('admin/addofferpost', 'Admin\OfferController@addofferpost');



//Admin side Create Sub Menus 
Route::get('admin/submenu/add', 'Admin\MenuController@SubmenuAddView');
Route::get('admin/submenu/list', 'Admin\MenuController@SubmenusList');
Route::post('admin/addsubmenu', 'Admin\MenuController@AddSubmenu');
Route::get('admin/submenu/edit/{id}', 'Admin\MenuController@EditSubmenu');
Route::post('admin/submenu/delete', 'Admin\MenuController@DeleteSubmenu');
Route::post('admin/submenu/status', 'Admin\MenuController@UpdateSubmenuStatus');


//Admin side Create Venues 
Route::get('admin/venue/add', 'Admin\VenueController@VenueAdd');
Route::post('admin/venue/venuestore', 'Admin\VenueController@VenueStore');
Route::get('admin/venue/view/{id}', 'Admin\VenueController@VenueView');
Route::get('admin/venue/list', 'Admin\VenueController@VenuesList'); 
Route::get('admin/venue/edit/{id}', 'Admin\VenueController@EditVenue');
Route::post('admin/venue/delete', 'Admin\VenueController@DeleteVenue');
Route::post('admin/getdistict', 'Admin\VenueController@getDistict');
Route::post('admin/venue/status', 'Admin\VenueController@UpdateVenueStatus');
Route::post('admin/venue/deletevenueimage', 'Admin\VenueController@deleteVenueImage');

//Order management
Route::get('admin/order/list', 'Admin\OrderController@orderList');
Route::get('admin/order/view/{id}', 'Admin\OrderController@orderView');
Route::post('admin/deleteorder', 'Admin\OrderController@deleteorder');

Route::get('admin/item/list', 'Admin\MenuController@itemList');
Route::get('admin/item/item-add', 'Admin\MenuController@itemAdd');
Route::get('admin/item/item-edit/{id}', 'Admin\MenuController@itemEdit');
Route::post('admin/item/delete', 'Admin\MenuController@itemDelete');
Route::post('admin/item/item-store', 'Admin\MenuController@itemStore');
Route::post('admin/item/status', 'Admin\MenuController@itemStatus'); 