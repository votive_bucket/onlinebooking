<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue_images extends Model
{
    protected $table = 'venue_images';
	protected $primaryKey = 'image_id';
	public $timestamps = true;
}
