<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order_detail';
	protected $primaryKey = 'order_id';
	public $timestamps = true;
}
