<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_menu extends Model
{
    protected $table = 'sub_menu';
	protected $primaryKey = 'sub_menu_id';
	public $timestamps = true;
}
