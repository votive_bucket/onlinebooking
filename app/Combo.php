<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combo extends Model
{
    protected $table = 'combo';
	protected $primaryKey = 'combo_id';
	public $timestamps = true;
	//protected $fillable = ['combo_name', 'combo_image', 'combo_content', 'combo_price', 'combo_status', '_token'];

}
