<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Combo, App\Offer, App\Order, App\Venue_menu;
use Session, DB;
use App\Http\Controllers\Admin\MailSendController;

class PagesController extends Controller
{
    //frontpage
    public function frontPage(Request $request)
    {
      return view('frontend.frontpage');
    }


    public function comboPage(Request $request)
    {
      $data['allcombo'] = Combo::where('combo_status', 1)->limit(3)->orderBy('combo_id', 'asc')->get();
      $data['alloffer'] = Offer::where('offer_status', 1)->limit(3)->get();
      $data['allcity'] = DB::table('city')->select('*')->orderBy('city_name', 'asc')->get();
      $data['defaultcity'] = DB::table('city')->select('*')->where('city_id', 1)->first();
      $data['defaultdistricts'] = DB::table("district")
                    ->where("city_id",$data['defaultcity']->city_id)
                    ->get();
      $data['minofferamount'] = Offer::where('offer_status', 1)->min('offer_amount');
      return view('frontend.combopage')->with($data);
    }

    public function getDistrictList(Request $request)
    {
        /*$districts = DB::table("district")
                    ->where("city_id",$request->city_id)
                    ->get();
        return response()->json($districts);*/
        $forminputs  = $request->all();
        $ids = array();

      $districts = DB::table("district")->select('id', 'district_name')->where("city_id",$forminputs['city_id'])->get();
      //echo "<pre>";print_r($districts);die();

      $venues = DB::table('venue_menu');
      $venues->Leftjoin('venues', 'venues.venue_id', '=', 'venue_menu.menu_venue');
      $venues->select("venue_menu.*","venues.*");
      $venues->Where('venues.venue_status', '=', 1);
      $venues->Where('venue_menu.menu_status', '=', 1);
      $venues->Where('venue_menu.menu_combo', '=', $forminputs['cid']);
      $venues->Where('venues.venue_city', '=', $forminputs['city_id']);
      //$venues->WhereIn('venues.venue_district', $ids);
      $venues->Where('venues.venue_hall_capacity', '>=', (int)$forminputs['capacity']);
      $venues->groupBy('venue_menu.menu_venue');
      $venuesData['venues_list'] = $venues->get();
      
      $Venueview = View('frontend.venuefilters',$venuesData)->render();
      echo json_encode(
          array(
              'status' => 1,
              'venueview' => $Venueview,
              'districts' => $districts
          )
      );
    }

    public function offerPage(Request $request)
    {
      $data['alloffer'] = Offer::where('offer_status', 1)->limit(3)->get();
      return view('frontend.offers')->with($data);

    }

    public function saveorder(Request $request)
    {
      if ($request->isMethod('post')) {
        $explodeVal = explode(',', $request->venus_ids);

        $last_record = DB::table('order_detail')->select('order_id')->orderBy('order_id', 'desc')->first();

        $lastid = '';

        if (!empty($last_record)) {
          $lastid = $last_record->order_id;
        } else {
          $lastid = 0;
        }
        

        $selected_city = $request->hidden_city_id;        
        $offer_id1 = '';
        $offer1 = '';
        $offer_name1 = '';
        $offer_id2 = '';
        $offer2 = '';
        $offer_name2= '';
        $offer_id3 = '';
        $offer3 = '';
        $offer_name3 = '';

        if($request->total1 >= $request->offers_hidden_price_0){
          $offer_id1 = $request->offers_hidden_id_0;
          $offer1 = $request->offers_hidden_price_0;
          $offer_name1 = $request->offers_hidden_name_0;

        }
        if($request->total1 >= $request->offers_hidden_price_1){

          $offer_id1 = $request->offers_hidden_id_1;
          $offer1 = $request->offers_hidden_price_1;
          $offer_name1 = $request->offers_hidden_name_1;

        }
        if($request->total1 >= $request->offers_hidden_price_2){
          
          $offer_id1 = $request->offers_hidden_id_1;
          $offer1 = $request->offers_hidden_price_2;
          $offer_name1 = $request->offers_hidden_name_2;

        }
        if($request->total2 >= $request->offers_hidden_price_0){
          
          $offer_id2 = $request->offers_hidden_id_0;
          $offer2 = $request->offers_hidden_price_0;
          $offer_name2 = $request->offers_hidden_name_0;
        
        }
        if($request->total2 >= $request->offers_hidden_price_1){
          
          $offer_id2 = $request->offers_hidden_id_1;
          $offer2 = $request->offers_hidden_price_1;
          $offer_name2 = $request->offers_hidden_name_1;
        
        }if($request->total2 >= $request->offers_hidden_price_2){
          
          $offer_id2 = $request->offers_hidden_id_2;
          $offer2 = $request->offers_hidden_price_2;
          $offer_name2 = $request->offers_hidden_name_2;
        
        }
        if($request->total3 >= $request->offers_hidden_price_0){

          $offer_id3 = $request->offers_hidden_id_0;
          $offer3 = $request->offers_hidden_price_0;
          $offer_name3 = $request->offers_hidden_name_0;
        
        }if($request->total3 >= $request->offers_hidden_price_1){
        
          $offer_id3 = $request->offers_hidden_id_1;          
          $offer3 = $request->offers_hidden_price_1;
          $offer_name3 = $request->offers_hidden_name_1;
        
        }if($request->total3 >= $request->offers_hidden_price_2){

          $offer_id3 = $request->offers_hidden_id_2;          
          $offer3 = $request->offers_hidden_price_2;
          $offer_name3 = $request->offers_hidden_name_2;
        
        }
        $number = '';

        if ($selected_city==1) {
          $number = 'HCM000'.(++$lastid);
        }
        if ($selected_city==2) {
          $number = 'HN000'.(++$lastid);
        }
        if ($selected_city==3) {
          $number = 'DN000'.(++$lastid);
        }

        $data               = new Order();
        $data->combo_id     = $request->comboid;
        //$data->order_no     = mt_rand('100000','999999');
        $data->order_no     = $number;
        $data->no_of_guest  = $request->people;
        $data->venue1       = $explodeVal[0];
        $data->venue2       = $explodeVal[1];
        $data->venue3       = $explodeVal[2];
        $data->menu1        = $request->menucheck1;
        $data->menu2        = $request->menucheck2;
        $data->menu3        = $request->menucheck3;
        $data->total1       = $request->total1;
        $data->total2       = $request->total2;
        $data->total3       = $request->total3;
        $data->offer_id1    = $offer_id1;
        $data->offer_id2    = $offer_id2;
        $data->offer_id3    = $offer_id3;
        $data->order_status = 0;
        $data->save();

        $venus_ids = '';
        $submenu_ids = '';       
        $data1['venuemenudata'] = [];  
        $menudata  = [];     
          for ($i=0; $i < count($explodeVal); $i++) { 
            if($i==0){
              $menu = $request->menucheck1;
            }
            if($i==1){
              $menu = $request->menucheck2;
            }
            if($i==2){
              $menu = $request->menucheck3;
            }
            $venuedata = DB::table('venues')->where('venue_id', $explodeVal[$i])->where('venue_status', 1)->first();
            $menudata = DB::table('venue_menu')->where('menu_id', $menu)->where('menu_status', 1)->first();             
            /*$submenusData = [];   
            if (!empty($menudata)) {
                  $submenu_ids = explode(',', $menudata->submenu_ids);
                  $submenusData = array();
                  for ($k=0; $k < count($submenu_ids) ; $k++) {
                    
                    $submenusData[] = DB::select("SELECT sub_menu.sub_menu_id, sub_menu.sub_menu_name, GROUP_CONCAT(items.item_name ORDER BY items.item_name) ItemName FROM sub_menu INNER JOIN items ON FIND_IN_SET(items.item_id, sub_menu.items) where sub_menu_id = ".$submenu_ids[$k]." GROUP BY sub_menu.sub_menu_id");
                  }
                  $menudata->submenu = $submenusData;
            }*/
            $data1['venuemenudata'][] = array('menus'=>$menudata, 'venue' => $venuedata);
          }

        $data2 =  array('formdata' => $request->all(), 'dataid' => $data->order_id, 'dataorder' =>$data->order_no, 'venuemenudata' =>$data1['venuemenudata'], 'offer1' => $offer_name1, 'offer2' => $offer_name2, 'offer3' => $offer_name3);
        return view('frontend.orderconfirm')->with($data2);
      }else{
        return redirect('/');
      }     
    }

    public function getVenuesList(Request $request)
    {
      $forminputs  = $request->all();

        $dist_ids = '';

        if (isset($forminputs['dist_ids'])) {
          $dist_ids = explode(',', $forminputs['dist_ids']);
        }

        $venues = DB::table('venue_menu');
        $venues->Leftjoin('venues', 'venues.venue_id', '=', 'venue_menu.menu_venue');
          if (!empty($dist_ids)) {
              $venues->where(function($query) use ($dist_ids) {
                  for ($i=0; $i < count($dist_ids); $i++) { 
                      if($i == 0)
                      {
                          $query->whereRaw("FIND_IN_SET(".$dist_ids[$i].",venues.venue_district)");
                      }
                      else
                      {
                          $query->orWhereRaw("FIND_IN_SET(".$dist_ids[$i].",venues.venue_district)");
                      }
                  
                  }
              });                
          }
        $venues->Where('venues.venue_status', '=', 1);
        $venues->select("venue_menu.*","venues.*");
        $venues->Where('venue_menu.menu_status', '=', 1);
        $venues->Where('venue_menu.menu_combo', '=', $forminputs['cid']);
        $venues->Where('venues.venue_city', '=', $forminputs['cityID']);
        $venues->Where('venues.venue_hall_capacity', '>=', (int)$forminputs['capacity']);
        $venues->groupBy('venue_menu.menu_venue');
        $venuesData['venues_list'] = $venues->get();
        //echo "<pre>";print_r($venuesData['venues_list']);die();

        $Venueview = View('frontend.venuefilters',$venuesData)->render();
        echo json_encode(
            array(
                'status' => 1,
                'venueview' => $Venueview
            )
        );
    }

    public function getComboVenuesList(Request $request)
    {
      $forminputs  = $request->all();
      $ids = array();

      $venues = DB::table('venue_menu');
      $venues->Leftjoin('venues', 'venues.venue_id', '=', 'venue_menu.menu_venue');
      $venues->select("venue_menu.*","venues.*");
      $venues->Where('venues.venue_status', '=', 1);
      $venues->Where('venue_menu.menu_status', '=', 1);
      $venues->Where('venue_menu.menu_combo', '=', $forminputs['cid']);
      $venues->Where('venues.venue_city', '=', 1);
      //$venues->WhereIn('venues.venue_district', $ids);
      $venues->Where('venues.venue_hall_capacity', '>=', (int)$forminputs['capacity']);
      $venues->groupBy('venue_menu.menu_venue');
      $venuesData['venues_list'] = $venues->get();
      
      $Venueview = View('frontend.venuefilters',$venuesData)->render();
      echo json_encode(
          array(
              'status' => 1,
              'venueview' => $Venueview
          )
      );
    }

    public function getVenueDetails(Request $request)
    {
     $forminputs  = $request->all();

     $venuedata = DB::table('venues')->where('venue_id', $forminputs['venueid'])->where('venue_status', 1)->first();
     $menudata = DB::table('venue_menu')->where('menu_venue', $forminputs['venueid'])->where('menu_combo', $forminputs['comboid'])->where('menu_status', 1)->get();
     $venueimages = DB::table('venue_images')->where('venue_id', $forminputs['venueid'])->where('image_status', 1)->get();

     /*$submenusData = [];   
     if (!empty($menudata)) {

        for ($j=0; $j < count($menudata); $j++) { 

          $submenu_ids = explode(',', $menudata[$j]->submenu_ids);
          $submenusData = array();
          for ($k=0; $k < count($submenu_ids) ; $k++) {
            
            $submenusData[] = DB::select("SELECT sub_menu.sub_menu_name, GROUP_CONCAT(items.item_name ORDER BY items.item_name) ItemName FROM sub_menu INNER JOIN items ON FIND_IN_SET(items.item_id, sub_menu.items) where sub_menu_id = ".$submenu_ids[$k]." GROUP BY sub_menu.sub_menu_id");
          }
          $menudata[$j]->submenu = $submenusData;
        }                
     }*/
     $data['venuemenudata'][] = array('menus'=>$menudata, 'venue' => $venuedata, 'venue_images' => $venueimages, 'countPeople' => $forminputs['countPeople']);
     //echo "<pre>";print_r($data['venuemenudata']);die();
     
     $Venueview = View('frontend.venuedetails',$data)->render();
     echo json_encode(
         array(
             'status' => 1,
             'venueview' => $Venueview
         )
     );
   }

    public function getMenus(Request $request)
    {
        $forminputs  = $request->all();
        $venus_ids = '';
        $submenu_ids = '';
        if (isset($forminputs['venus_ids'])) {
            $venus_ids = explode(',', $forminputs['venus_ids']);
        }
        $data['venuemenudata'] = [];  
        $menudata  = [];     
        if (!empty($venus_ids)) {
          for ($i=0; $i < count($venus_ids); $i++) { 

            $venuedata = DB::table('venues')->where('venue_id', $venus_ids[$i])->where('venue_status', 1)->first();
            $menudata = DB::table('venue_menu')->where('menu_venue', $venus_ids[$i])->Where('venue_menu.menu_combo', '=', $forminputs['cid'])->where('menu_status', 1)->get();

            //$submenusData = [];   
           /* if (!empty($menudata)) {

                for ($j=0; $j < count($menudata); $j++) { 

                  $submenu_ids = explode(',', $menudata[$j]->submenu_ids);
                  $submenusData = array();
                  for ($k=0; $k < count($submenu_ids) ; $k++) {
                    
                    $submenusData[] = DB::select("SELECT sub_menu.sub_menu_name, GROUP_CONCAT(items.item_name ORDER BY items.item_name) ItemName FROM sub_menu INNER JOIN items ON FIND_IN_SET(items.item_id, sub_menu.items) where sub_menu_id = ".$submenu_ids[$k]." GROUP BY sub_menu.sub_menu_id");
                  }
                  $menudata[$j]->submenu = $submenusData;
                }                
            }*/
            $data['venuemenudata'][] = array('menus'=>$menudata, 'venue' => $venuedata);
          }
        }
        $menuview = View('frontend.getmenu',$data)->render();
        echo json_encode(
            array(
                'status' => 1,
                'menuview' => $menuview
            )
        );
    }

    public function thankyou(Request $request, MailSendController $mail)
    {       
      if ($request->isMethod('post')) {
        $input = $request->all();
        $data = Order::findOrFail($input['order']);
        $data->party_date = $input['date'];
        $data->party_time = $input['time'];
        $data->name       = $input['name'];
        $data->phone_no   = $input['phone'];
        $data->email      = $input['email'];
        $data->refferred_no = $input['ref_phone'];
        $data->note       = $input['note'];
        $data->discount_date = $input['discount_date'];
        $data->order_status = 1;
        if($data->save()){
            $venuedata1 = DB::table('venues')->where('venue_id', $data->venue1)->where('venue_status', 1)->first();
           $venuedata2 = DB::table('venues')->where('venue_id', $data->venue2)->where('venue_status', 1)->first();
           $venuedata3 = DB::table('venues')->where('venue_id', $data->venue3)->where('venue_status', 1)->first();
           $menudata1 = DB::table('venue_menu')->where('menu_id', $data->menu1)->where('menu_status', 1)->first();  
           $menudata2 = DB::table('venue_menu')->where('menu_id', $data->menu2)->where('menu_status', 1)->first(); 
           $menudata3 = DB::table('venue_menu')->where('menu_id', $data->menu3)->where('menu_status', 1)->first();
           $combo_name = DB::table('combo')->where('combo_id', $data->combo_id)->where('combo_status', 1)->first();
           $offerdata1=$offerdata2=$offerdata3 = '';
           if ($data->offer_id1!=0) {
              $offer_data1 = DB::table('offers')->where('offer_id', $data->offer_id1)->where('offer_status', 1)->first();
              $offerdata1 = $offer_data1->offer_name;  
           }
           if ($data->offer_id2!=0) {
              $offer_data2 = DB::table('offers')->where('offer_id', $data->offer_id2)->where('offer_status', 1)->first();
              $offerdata2 = $offer_data2->offer_name;  
           }
           if ($data->offer_id3!=0) {
              $offer_data3 = DB::table('offers')->where('offer_id', $data->offer_id3)->where('offer_status', 1)->first();
              $offerdata3 = $offer_data3->offer_name;
            } 


            $email_content = DB::Table('email_template')->where('eid', 2)->first();

            $searchArray = array("{user_name}", "{email}", "{order_no}", "{order_date}", "{total_people}", "{first_venue}", "{second_venue}", "{third_venue}", "{img_url}", "{site_url}", "{total1}", "{total2}", "{total3}", "{menu_name_1}", "{menu_name_2}", "{menu_name_3}", "{submenu_name_1}", "{submenu_name_2}", "{submenu_name_3}", "{order_note}", "{combo_name}", "{menu_price_1}", "{menu_price_2}", "{menu_price_3}", "{offer_name_1}", "{offer_name_2}", "{offer_name_3}", "{deposite_date}", "{total_with_percent_1}", "{total_with_percent_2}", "{total_with_percent_3}");

              $total_with_percent_1 = ($data->total1*50)/100;
              $total_with_percent_2 = ($data->total2*50)/100;
              $total_with_percent_3 = ($data->total3*50)/100;

           $replaceArray = array($data->name, $data->email, $data->order_no, $data->party_date, $data->no_of_guest, $venuedata1->venue_name, $venuedata2->venue_name, $venuedata3->venue_name, url('resources/assets/images/'), url('/'), str_replace(",",".",number_format($data->total1)), str_replace(",",".",number_format($data->total2)), str_replace(",",".",number_format($data->total3)), $menudata1->menu_name, $menudata2->menu_name, $menudata3->menu_name, $menudata1->submenu_ids, $menudata2->submenu_ids, $menudata3->submenu_ids, $data->note, $combo_name->combo_name, str_replace(",",".",number_format($menudata1->menu_price)), str_replace(",",".",number_format($menudata2->menu_price)), str_replace(",",".",number_format($menudata3->menu_price)), $offerdata1, $offerdata2, $offerdata3, $data->discount_date, str_replace(",",".",number_format($total_with_percent_1)), str_replace(",",".",number_format($total_with_percent_2)), str_replace(",",".",number_format($total_with_percent_3)));

            $content = str_replace($searchArray, $replaceArray, $email_content->content);
           
            $data = [
                'name'      => $data->name,
                'email'     => $data->email,              
                'subject'   => $email_content->subject,
                'content'   => $content,
            ];
              
            $mail->sendmail($data); 
          return view('frontend.thankyou');
        }
      }else{
        return redirect('/');
      }
    }

    public function delete(Request $request){
      $id = $request->id;
      $data = Order::findOrFail($id);
      $data->order_status = 3;
      if($data->save()){
        echo json_encode(array('msg'=>'Đã xóa đơn đặt hàng thành công', 'status'=>'1'));
      }else{
        echo json_encode(array('msg'=>'Đã xảy ra sự cố', 'status'=>'0'));
      }      
    }
}
