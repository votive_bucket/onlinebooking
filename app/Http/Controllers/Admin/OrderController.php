<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, App\Order,Session, View,File;
class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function orderList(){
        $data['orders'] = Order::selectRaw('order_detail.*,(select combo_name from combo where combo_id=order_detail.combo_id) as combo_name,(select venue_name from venues where venue_id=order_detail.venue1) as venue1_name,(select venue_name from venues where venue_id=order_detail.venue2) as venue2_name,(select venue_name from venues where venue_id=order_detail.venue3) as venue3_name')->orderby('order_id','desc')->where('order_status', 1)->paginate(20);
        /*Order::selectRaw('order_detail.*,(select combo_name from combo where combo_id=order_detail.combo_id) as combo_name')->orderby('order_id','desc')->paginate(20);*/
        return view('admin.order.order')->with($data); 
    }
    public function orderView($id){
    	$data['orders'] = Order::selectRaw('order_detail.*,(select combo_name from combo where combo_id=order_detail.combo_id) as combo_name,(select venue_menu.menu_name from venue_menu where menu_id=order_detail.menu1) as menu1,(select venue_menu.menu_name from venue_menu where menu_id=order_detail.menu2) as menu2,(select venue_menu.menu_name from venue_menu where menu_id=order_detail.menu3) as menu3,(select venue_menu.submenu_ids from venue_menu where menu_id=order_detail.menu1) as submenu1,(select venue_menu.submenu_ids from venue_menu where menu_id=order_detail.menu2) as submenu2,(select venue_menu.submenu_ids from venue_menu where menu_id=order_detail.menu3) as submenu3,(select venue_name from venues where venue_id=order_detail.venue1) as venue1_name,(select venue_name from venues where venue_id=order_detail.venue2) as venue2_name,(select venue_name from venues where venue_id=order_detail.venue3) as venue3_name')->where('order_id',$id)->first();
        return view('admin.order.ViewOrder')->with($data); 
    }

    //Delete Order
    public function deleteorder(Request $request){
        $id = $request->id;
        $data =  Order::findOrFail($id);
        $data->order_status = 3;        
        if($data->save()){            
            echo json_encode(array('status'=>1, 'msg'=>'Order Deleted Successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }
    
} 
  