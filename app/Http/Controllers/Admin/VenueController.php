<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Venues,App\Venue_menu,App\City , App\Venue_images, App\District, Auth, Session, View,File;
class VenueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
   protected $validationRules=[
                'title' => 'required|unique|max:255',
                'body' => 'required'
    ];
    public function VenuesList(){
    	$data = array();
    	$data['venues'] = Venues::selectRaw('(select city_name from city where city_id=venues.venue_city) as city,venues.*')->where('venue_status','!=',3)->get(); 
 
    	return view('admin.venues.venues')->with($data);
    }
 
    public function VenueAdd(){ 
        $data = array();  
    	$data['menus'] = Venue_menu::Where('menu_status', '!=', 3)->orderBy('menu_id', 'desc')->get();
        $data['city'] = City::all();
    	return view('admin.venues.addVenue')->with($data);
    }
    public function VenueStore(Request $request){
        $forminputs = $request->all();
        //echo "<pre>";print_r($forminputs);die;
        if($request->input('edit')=="edit"){ 
            $data = Venues::find($request->input('venue_id'));
        }else{
            $data = new Venues();
        }

        $data->venue_name = $forminputs['venue_name'];
        if(isset($forminputs['menu_ids'])){
            $data->menu_ids = implode(',', $forminputs['menu_ids']);
        }
        $data->venue_city = $forminputs['city'];
        $data->venue_district = $forminputs['district'];
        //$data->venue_ward = $forminputs['venue_ward'];  
        //$data->venue_street = $forminputs['venue_streat'];  
        $data->venue_address = $forminputs['venue_address'];  
        $data->venue_hall_capacity = $forminputs['venue_hall_capacity']; 
        $data->venue_has_projector = (isset($forminputs['venue_has_projector'])) ? 1 : 0;
        $data->venue_has_dj = (isset($forminputs['venue_has_dj'])) ? 1 : 0 ;
        $data->venue_has_stage = (isset($forminputs['venue_has_stage'])) ? 1 : 0;
        $data->venue_has_viproom = (isset($forminputs['venue_has_viproom'])) ? 1 : 0;
        $data->venue_has_livemusic = (isset($forminputs['venue_has_livemusic'])) ? 1 : 0;
        $data->venue_has = $forminputs['venue_has'];
        $data->venue_logo = '';
        $data->venue_status = 1;
        if ($request->hasFile('venuelogo')) { 
            $image = $request->file('venuelogo');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/venue');         
            $imagePath = $destinationPath. '/'.  $name;
            $image->move($destinationPath, $name); 
        }else{
            $name = $request->input('upload_img');
        };
        $data->venue_logo = $name;
        $data->save(); 
        $lastid = $data->venue_id;
        if($request->hasfile('venueimages')) 
         { 
            foreach($request->file('venueimages') as $file) 
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/uploads/venue/images', $name);  
                $files = new Venue_images;
                $files->venue_id = $lastid;
                $files->image_name = $name;
                $files->image_status =1;
                $files->save();
            }

         }
         
        if($request->input('edit')=="edit"){
            Session::flash('message', 'Venue Updated Successfully!');
        }else{
            Session::flash('message', 'Venue Added Successfully!');
        } 
        return redirect('admin/venue/list');         
    } 
    public function getDistict(Request $request){
        $city = $request->city;
        $district = District::where('city_id',$city)->get();
        $html = '<option value=""> select district</option>';
        if(!$district->isEmpty()){
            foreach($district as $di){
                 $html .= '<option value="'.$di->id.'"> '. $di->district_name .'</option>';
            }
        }
        echo $html;
    }

    public function UpdateVenueStatus(Request $request){
        $id = $request->id;
        $menu = Venues::findOrFail($id);
        if($menu->venue_status == 0){
            $menu->venue_status = 1;
        }else{
            $menu->venue_status = 0;
        }
        
        if($menu->save()){
            echo json_encode(array('status'=>1, 'msg'=>'Venue status updated successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }
    public function EditVenue($id , Request $request){ 
        $data = array();  
        $data['menus']  = Venue_menu::Where('menu_status', '!=', 3)->orderBy('menu_id', 'desc')->get();
        $data['images'] = Venue_images::Where('venue_id', $id)->get();
        $data['city'] = City::all();
        $data['Venues'] = Venues::find($id);
        $data['district'] = District::where('city_id',$data['Venues']->venue_city)->get();
        return view('admin.venues.addVenue')->with($data);
    } 

    public function DeleteVenue(Request $request){
        $id = $request->id;
        $Venues = Venues::findOrFail($id);
        $Venues->venue_status = 3;
        if($Venues->save()){            
            echo json_encode(array('status'=>1, 'msg'=>'Menu deleted successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }
    public function deleteVenueImage(Request $request){
        $id = $request->delid; 
        $imgname = Venue_images::find($id);
        $image_path = public_path().'/uploads/venue/images/'.$imgname->image_name;
        if(File::exists($image_path)) {
            File::delete($image_path);
        }
        Venue_images::where('image_id',$id)->delete();
        echo 1;
    }
    public function VenueView($id ,Request $request){
        $data = array();   
        $data['images'] = Venue_images::Where('venue_id', $id)->get(); 
        $data['Venues'] = Venues::selectRaw('(select city_name from city where city_id=venues.venue_city) as city,(select district_name from district where id=venues.venue_district) as district,venues.*')->where('venue_id',$id)->first();
        $data['menus']  = Venue_menu::Where('menu_status', '!=', 3)
                                      ->whereIn('menu_id',explode(',',$data['Venues']->menu_ids))          
                                     ->orderBy('menu_id', 'desc')->get();
                                          
        //$data['district'] = District::where('city_id',$data['Venues']->venue_city)->get();
        return view('admin.venues.ViewVenue')->with($data);
    }
}
  