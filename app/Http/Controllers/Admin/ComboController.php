<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Combo;
use Session;

class ComboController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    //combos listing 
    public function allcombo(){
    	
    	$data['allcombo'] = Combo::where('combo_status', '!=', 3)->orderby('combo_id', 'desc')->get();      
    	return view('admin.combos')->with($data);
    }

    //combos edit 
    public function editcombo(Request $request){
    	$id = $request->id;
    	$data['combo'] = Combo::where('combo_id', $id)->firstOrFail();     
    	return view('admin.addcombo')->with($data);
    }

    //Add combo form 
    public function addcombo(){
    	return view('admin.addcombo');
    }

    //Save combo form data 
    public function addcombopost(Request $request){
      	$input = $request->all();
      	$name="";
        if ($request->hasFile('combo_image')) {
        
            $image = $request->file('combo_image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/combo');         
            $imagePath = $destinationPath. '/'.  $name;
            $image->move($destinationPath, $name);

        }else{
            $name = $request->input('upload_img');
        }
        $selectedname="";
        if ($request->hasFile('combo_selected_image')) {
        
            $image = $request->file('combo_selected_image');
            $selectedname = time().'_selected.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/combo');         
            $imagePath = $destinationPath. '/'.  $selectedname;
            $image->move($destinationPath, $selectedname);

        }else{
            $selectedname = $request->input('upload_selected_img');
        }
            //echo $name;die;
    	if((isset($input['comboid'])) && ($input['comboid'] != '')){
    		$data = Combo::findOrFail($input['comboid']);
       	}else{
    		$data = new Combo();
    	}
    		$data->combo_name   	= $input['combo_name'];
        	$data->combo_content   	= $input['combo_content'];
            $data->combo_price    	= $input['combo_price'];        
            $data->combo_image    	= $name;
            $data->combo_selected_image = $selectedname;

            
    	if($data->save()){
    		Session::flash('message', 'Combo Content added successfully');
    	}
    	return redirect('admin/allcombo');
    }

    //Delete Combo
    public function deletecombo(Request $request){
        $id = $request->id;
        $data =  Combo::findOrFail($id);
        $data->combo_status = 3;
        
        if($data->save()){            
            echo json_encode(array('status'=>1, 'msg'=>'Combo Deleted Successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }

    //Active/Deactive Combo
    public function combostatus(Request $request){
        $id = $request->id;
        $data =  Combo::findOrFail($id);
       
        if($data->combo_status==1){
            $data->combo_status = 0;          
        }else{
            $data->combo_status = 1; 
        }
        if($data->save()){
            echo json_encode(array('status'=>1, 'msg'=>'Combo Status Updated Successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    } 

    //combos View 
    public function viewcombo(Request $request){
    	$id = $request->id;
    	$data['combo'] = Combo::findOrFail($id);  
    	return view('admin.viewcombo')->with($data);
    }
}
