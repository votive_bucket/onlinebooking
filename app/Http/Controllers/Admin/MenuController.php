<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Venue_menu, App\Sub_menu, App\Venues,App\Combo,App\Items,Auth, Session, View;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Menu listing 
    public function MenusList(){    	
    	$menus = Venue_menu::Where('menu_status', '!=', 3)->orderBy('menu_id', 'desc')->get();
    	$data = array();
        $data['menu'] = array();
        foreach($menus as $key => $value){
            $venues = Venues::where('venue_status',1)->where('venue_id',$value->menu_venue)->first(); 
            $combo = Combo::where('combo_status',1)->where('combo_id',$value->menu_combo)->first();
            /*$val = rtrim($value->submenu_ids, ',');            
            $s_id = explode(',', $val);
            $sel_submenu = array();
            for($i=0; $i<count($s_id); $i++){                
                $sel_submenu[] = Sub_menu::select('sub_menu_id','sub_menu_name')->where('sub_menu_id', $s_id[$i])->first();
            }*/
            //$data['menus'][$key] = array('menu_name' => $value->menu_name, 'menu_id' => $value->menu_id, 'menu_status' => $value->menu_status, 'submenu_name' => $sel_submenu);
            $data['menus'][$key] = array('menu_name' => $value->menu_name, 'menu_id' => $value->menu_id, 'menu_status' => $value->menu_status, 'combo' => $combo->combo_name, 'venue' => $venues->venue_name, 'menu_price' => $value->menu_price);

        }
    	return view('admin.menus')->with($data);
    }

    //Submenu listing 
    public function SubmenusList(){    	
    	$data['submenulists'] = Sub_menu::selectRaw('sub_menu.*')->Where('submenu_status', '!=', 3)->orderBy('sub_menu_id', 'desc')->get();
    	return view('admin.submenus')->with($data);
    }
    
    //add menu form 
    public function MenuAddView(){
        $data['combo'] = Combo::where('combo_status',1)->get();
        $data['venues'] = Venues::where('venue_status',1)->get(); 
    	//$data['submenulist'] = Sub_menu::Where('submenu_status', 1)->get();
    	return view('admin.addMenu')->with($data); 
    }

    //add submenu form 
    public function SubmenuAddView(){
        $data['items'] = Items::where('item_status','!=',3)->get();

    	$data['menus'] = Venue_menu::Where('menu_status', '!=', 3)->get();
    	return view('admin.addSubmenu')->with($data);
    }

    //Add menu
    public function AddMenu(Request $request){  
        if(Auth::check()){            
            $forminputs = $request->all();
            if($request->input('edit')=="edit"){ 
                $data = Venue_menu::find($request->input('menuid'));
            }else{
                $data = new Venue_menu();
            }  
            $data->menu_name         = $forminputs['menu_name'];
            $data->menu_combo        = $forminputs['combo'];
            $data->menu_venue        = $forminputs['venue'];
            $data->menu_price        = str_replace('.', '', $forminputs['Price']);
            $data->submenu_ids       = $forminputs['submenus'];           
            $data->save();

            if($request->input('edit')=="edit"){
                Session::flash('message', 'Menu Updated Successfully!');
            }else{
                Session::flash('message', 'Menu Added Successfully!');
            } 
            return redirect('admin/menu/list');
        }else{
            return redirect('admin/login');
        }
    }

    //Add Submenu
    /*public function AddSubmenu(Request $request){        
    	
        if(Auth::check()){            
            $forminputs = $request->all();

            if($request->input('edit')=="edit"){ 
                $data = Sub_menu::find($request->input('submenuid'));
            }else{
                $data = new Sub_menu();
            }  
            $data->sub_menu_name = $forminputs['sub_menu_name'];
            //$data->submenu_price = $forminputs['submenu_price'];
            if(!empty($forminputs['items'])){
                $data->items = implode(',',$forminputs['items']); 
            }
            
            $data->save();
            if($request->input('edit')=="edit"){
                Session::flash('message', 'Submenu Updated Successfully!');
            } else {
                Session::flash('message', 'Submenu Added Successfully!');
            } 
            return redirect('admin/submenu/list');
        }else{
            return redirect('admin/login');
        }
    }*/

    //Edit menu view
    public function EditMenu(Request $request){ 
        $data['combo'] = Combo::where('combo_status','!=',3)->get();
        $data['venues'] = Venues::where('venue_status','!=',3)->get();
    	$data['submenulist'] = Sub_menu::Where('submenu_status', '!=', 3)->get(); 
        if(Auth::check()){
            $id =  $request->id;
            $data['menu'] = Venue_menu::where('menu_id',$id)->first();

            $s_id = explode(',', $data['menu']->submenu_ids);
            $val = array_filter($s_id);
            $data['sel_submenu'] = array();
            foreach($val as $key=>$value){
                $data['sel_submenu'][] = Sub_menu::select('sub_menu_id')->where('sub_menu_id', $value)
                ->first();
            }

            return View::make('admin.addMenu')->with($data);
        }else{
            return redirect('/admin/login');
        }
    }

    //Edit submenu view
    /*public function EditSubmenu(Request $request){  
        if(Auth::check()){
            $id =  $request->id;
            $data['items'] = Items::where('item_status','!=',3)->get();
            $data['submenu'] = Sub_menu::where('sub_menu_id',$id)->first();
            return View::make('admin.addSubmenu')->with($data);
        }else{
            return redirect('/admin/login');
        }
    }*/

    // View menu 
    public function ViewMenu(Request $request){  
        if(Auth::check()){
            $id =  $request->id;
            $data['menu'] = Venue_menu::selectRaw('venue_menu.*,(select combo_name from combo where combo_id=venue_menu.menu_combo) as combo,(select venue_name from venues where venue_id=venue_menu.menu_venue) as venue')->where('menu_id',$id)->first();
            $data['sel_submenu'] = array();
            /*if(!empty($data['menu']->submenu_ids)){
                $val = rtrim($data['menu']->submenu_ids, ',');
                $s_id = explode(',', $val);                
                foreach($s_id as $key=>$value){
                    $data['sel_submenu'][] = Sub_menu::where('sub_menu_id', $value)
                    ->first();
                }
            }*/            
            return View::make('admin.ViewMenu')->with($data);
        }else{
            return redirect('/admin/login');
        }
    }

    //Delete menu
    public function DeleteMenu(Request $request)
    {
        $id = $request->id;
        $menu = Venue_menu::findOrFail($id);
        $menu->menu_status = 3;
        if($menu->save()){            
            echo json_encode(array('status'=>1, 'msg'=>'Menu deleted successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }

    //Delete submenu
    public function DeleteSubmenu(Request $request)
    {
        $id = $request->id;
        $submenu = Sub_menu::findOrFail($id);
        $submenu->submenu_status = 3;
        if($submenu->save()){            
            echo json_encode(array('status'=>1, 'msg'=>'Submenu deleted successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }

    //Active/Deactive Menu
    public function UpdateMenuStatus(Request $request)
    {
        $id = $request->id;
        $menu = Venue_menu::findOrFail($id);
        if($menu->menu_status == 0){
            $menu->menu_status = 1;
        }else{
            $menu->menu_status = 0;
        }
        
        if($menu->save()){
            echo json_encode(array('status'=>1, 'msg'=>'Menu status updated successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }

    //Active/Deactive Submenu
    public function UpdateSubmenuStatus(Request $request)
    {
        $id = $request->id;
        $submenu = Sub_menu::findOrFail($id);
        if($submenu->submenu_status == 0){
            $submenu->submenu_status = 1;
        }else{
            $submenu->submenu_status = 0;
        }
        
        if($submenu->save()){
            echo json_encode(array('status'=>1, 'msg'=>'Submenu status updated successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }

    public function itemList(){
        $data['items'] = Items::where('item_status','!=',3)->orderBy('item_id','desc')->get();
        return view('admin.items.items')->with($data);
    }
    public function itemAdd(){
         return view('admin.items.addItem');
    }
    public function itemStore(Request $request){                  
        $forminputs = $request->all();
        if($request->input('edit')=="edit"){ 
            $data = Items::find($request->input('itemid'));
        }else{
            $data = new Items();
        }  
        $data->item_name = $forminputs['item_name'];
        $data->item_status = 1;
        
        $data->save();
        if($request->input('edit')=="edit"){
            Session::flash('message', 'Item Updated Successfully!');
        } else {
            Session::flash('message', 'Item Added Successfully!');
        } 
        return redirect('admin/item/list');
       
    }
    public function itemEdit($id){
        $data['item'] = Items::find($id);
        return View::make('admin.items.addItem')->with($data);
    }
    public function itemStatus(Request $request)
    {
        $id = $request->id;
        $Items = Items::findOrFail($id);
        if($Items->item_status == 0){
            $Items->item_status = 1;
        }else{
            $Items->item_status = 0;
        }
        
        if($Items->save()){
            echo json_encode(array('status'=>1, 'msg'=>'Item status updated successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }
    public function itemDelete(Request $request){
        $id = $request->id;
        $Items = Items::findOrFail($id);
        $Items->item_status = 3;
        if($Items->save()){            
            echo json_encode(array('status'=>1, 'msg'=>'Items deleted successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }       
    }

}
