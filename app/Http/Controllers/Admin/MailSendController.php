<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;


class MailSendController extends Controller
{
    public function sendmail($data){
    	Mail::send('emails.verifyUser', $data, function($message)  use ($data){
    		$message->from('budstation.HCM@vn.ab-inbev.com', 'Bud Station');
            $message->to($data['email'])
           ->subject($data['subject']);
        });
    }
}
