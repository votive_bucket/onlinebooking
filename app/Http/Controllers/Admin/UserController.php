<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB, Mail, Auth, Hash, View, Session;
use App\Users;
use Illuminate\Support\Facades\Validator;
use App\Mail\AccountInformation;
use App\Http\Controllers\Admin\MailSendController;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

   
     //User view
    public function viewprofile(Request $request)
    {
        if(Auth::check()){
            $id = Auth::user()->id;

            $data['user'] = Users::where('id', $id)->where('user_role_id', 1)->first();            
            
            return View::make("admin.admin.viewAdminProfile")->with($data);
        }else{
            return redirect('admin/login');
        }
        
    }



    //Edit Admin Profile 
    public function editprofile(Request $request)
    {
        if(Auth::check()){
            $id = $request->id;
            $data['user'] = DB::table('users')->select('*')->where('id', $id)->first();
            return View::make("admin.admin.editProfile")->with($data);   
        }else{
            return redirect('/admin/login');
        }     
    }


    //Edit admin Profile 
    public function updateadminProfile(Request $request, MailSendController $mail){
        if(Auth::check()){
                $forminputs  = $request->all();           
                $data = Users::Find($request->input('userid')); 
                $validator = Validator::make($forminputs , [            
                    'email' => 'required|email|unique:users,email,'.$data->id,
                ]);                     
                if ($validator->fails())
                {
                   return redirect()->back()->with(["errors"=>$validator->errors()]);
                }
            $name="";

            if ($request->hasFile('profileImg')) {
            
                $image = $request->file('profileImg');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads');         
                $imagePath = $destinationPath. '/'.  $name;
                $image->move($destinationPath, $name);
            }else{
                $name = $request->input('p_img');
            }

            if($request->input('edit')=="edit"){
                $data = Users::Find($request->input('userid')); 
            }      
            
            $data->user_name        = $forminputs['user_name'];  
            $data->first_name       = $forminputs['first_name'];        
            $data->last_name        = $forminputs['last_name'];        
            $data->email            = $forminputs['email'];
            $data->user_contact     = $forminputs['user_contact'];  
            $data->user_profile_pic = $name;  
            if ($data->save()) {
                Session::flash('message', 'Profile updated successfully.'); 
                Session::flash('alert-class', 'alert-success');
            }
            return redirect('/admin/viewprofile');
        }else{
            return redirect('/admin/login');
        }

    } 
}
