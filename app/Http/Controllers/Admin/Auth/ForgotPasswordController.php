<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Mail,DB,view;
use App\Mail\ResetPasswordAdmin;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Admin\MailSendController;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }


    public function showResetForm(Request $request)
    {
        if(!Auth::check()){
        return view('admin.reset');
        }else{
            return redirect('admin/dashboard');
        }
    }



    public function resetPassword(Request $request)
    {
        $forminputs  = $request->all();
        $validator = Validator::make($forminputs , [
            'password' => 'required|min:6|confirmed',
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $resetPass = User::where('vrfn_code', $forminputs['vrfn_code'])->where('user_role_id', 1)->first();
        if(isset($resetPass) ) {
            if($resetPass->vrfn_code) {
                $resetPass->password = Hash::make($forminputs['password']);
                $resetPass->save();
                $status = "Your password is changed. Now you can login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        }

    }


    //send mail of Reset Password information 
    public function sendResetLinkEmail(Request $request, MailSendController $mail)
    {
        $forminputs  = $request->all();
        $validator = Validator::make($forminputs , [
            'email' => 'required|email',
        ]);       
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()]);
        }
        $data = User::where('email', $forminputs['email'])->where('user_role_id', 1)->first();
        
        if(!empty($data)){
            
            $data->vrfn_code=str_random(20);
            $data->save();  

            $email_content = DB::Table('email_template')->where('eid', 1)->first();

            $searchArray = array("{user_name}", "{email}", "{verify_url}", "{reset_url}", "{img_url}", "{site_url}");

            $replaceArray = array($data->user_name, $forminputs['email'], url('admin/verify', $data->vrfn_code), url('admin/password/reset', $data->vrfn_code), url('resources/assets/images/'), url('/'));

            $content = str_replace($searchArray, $replaceArray, $email_content->content);
            
            $data = [
                'name'      => $data->user_name,
                //'email'     => 'votive.prateek@gmail.com',
                'email'     => $forminputs['email'],                    
                'vrfn_code' => $data->vrfn_code,         
                'subject'   => $email_content->subject,
                'content'   => $content,
            ];
            $mail->sendmail($data); 

            return response()->json(['success'=>'Email has been sent to your email id ']);
        }else{
            return response()->json(['warning'=>"This email id does not exist"]);
        }        
    }


}
