<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Auth, Session, Hash;
use Illuminate\Http\Request;
use App\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function resetpasswordview(Request $request){
        return view('admin.layouts.changepassword');
    }

    public function resetPassword(Request $request){
        $inputVal = $request->all();
        $credentials = array(
            'email'     => Auth::user()->email,
            'password'  => $inputVal['old_password'],
            'user_role_id' => '1'
        );
       
        if (Auth::attempt($credentials)){
            $user = User::where('user_role_id', 1)->where('email', Auth::user()->email)->update(['password' => Hash::make($inputVal['new_password'])]);
             if($user==1){                
                Session::flash('message', 'Password Updated Successfully!');
               
            }else{
                Session::flash('error', 'Something Went wrong!');
                
            }
            return redirect('admin/resetpassword');
             
        }else{
            Session::flash('error', 'Old Password did not match!');
            return redirect('admin/resetpassword');

        }
            
    }
}
