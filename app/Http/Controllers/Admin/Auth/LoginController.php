<?php

namespace App\Http\Controllers\Admin\Auth;


use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth, DB;
use Validator;
use App\Admin;
use Hash, Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }



    public function login(){
       
        return view('admin.login');       
    }

    public function authenticate(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'user_name' => 'required',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails()) {
            return redirect('admin/login')
                        ->withErrors($validator)
                        ->withInput();
        }else{

            $inputVal = $request->all();
            $credentials = array(
                'user_name' => $inputVal['user_name'],
                'password' => $inputVal['password'],
                'user_role_id' => '1'
            );
            if (Auth::attempt($credentials)){
                session::flash('message', 'LoggedIn Successfully' );
                return redirect('/admin/dashboard'); 
            }else{
                session::flash('error', 'UserId or Password is Incorrect' );
                return redirect('/admin/login');
            }            
        }
    } 

    public function logout(){
        Auth::logout();
        return redirect('admin/login');
    }  

    public function dashboard(){
        if (Auth::check()) {

            if (Auth::user()->user_role_id == 1) {
                return view('admin.dashboard');
            } else {
                return redirect('/');
            }

        }else{  
            return redirect('/admin/login');
        }
    }
    public function forget(){
        return redirect()->intended('/admin/login');
    } 
    
}
