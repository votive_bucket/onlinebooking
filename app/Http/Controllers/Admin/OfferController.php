<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;
use Session;

class OfferController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    //offers listing 
    public function alloffer(){
    	
    	$data['alloffer'] = Offer::where('offer_status', '!=', 3)->orderby('offer_id', 'desc')->get();      
    	return view('admin.offers')->with($data);
    }

    //offers edit 
    public function editoffer(Request $request){
    	$id = $request->id;
    	$data['offer'] = Offer::where('offer_id', $id)->firstOrFail();     
    	return view('admin.addoffer')->with($data);
    }

    //Add offer form 
    public function addoffer(){
    	return view('admin.addoffer');
    }

    //Save offer form data 
    public function addofferpost(Request $request){
      	$input = $request->all();
      	$name="";
            if ($request->hasFile('offer_image')) {
            
                $image = $request->file('offer_image');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/offer');         
                $imagePath = $destinationPath. '/'.  $name;
                $image->move($destinationPath, $name);

            }else{
                $name = $request->input('upload_img');
            };
    	if((isset($input['offerid'])) && ($input['offerid'] != '')){
    		$data = Offer::findOrFail($input['offerid']);
       	}else{
    		$data = new Offer();
    	}
    		$data->offer_name   	= $input['offer_name'];        	
            $data->offer_amount    	= $input['offer_amount'];        
            $data->offer_image    	= $name;

    	if($data->save()){
    		Session::flash('message', 'Offer Content added successfully');
    	}
    	return redirect('admin/alloffer');
    }

    //Delete offer
    public function deleteoffer(Request $request){
        $id = $request->id;
        $data =  Offer::findOrFail($id);
        $data->offer_status = 3;
        
        if($data->save()){            
            echo json_encode(array('status'=>1, 'msg'=>'Offer Deleted Successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    }

    //Active/Deactive offer
    public function offerstatus(Request $request){
        $id = $request->id;
        $data =  Offer::findOrFail($id);
       
        if($data->offer_status==1){
            $data->offer_status = 0;          
        }else{
            $data->offer_status = 1; 
        }
        if($data->save()){
            echo json_encode(array('status'=>1, 'msg'=>'Offer Status Updated Successfully'));
        }else{
            echo json_encode(array('status'=>0, 'msg'=>'Something went wrong!'));
        }
    } 

    //offers View 
    public function viewoffer(Request $request){
    	$id = $request->id;
    	$data['offer'] = Offer::findOrFail($id);  
    	return view('admin.viewoffer')->with($data);
    }
}
