<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Venues extends Model
{
    protected $table = 'venues';
	protected $primaryKey = 'venue_id';
	public $timestamps = true;
}
