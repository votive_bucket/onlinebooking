<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venue_menu extends Model
{
    protected $table = 'venue_menu';
	protected $primaryKey = 'menu_id';
	public $timestamps = true;
}
